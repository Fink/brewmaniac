/*
 * Heaters_test.cpp
 *
 *  Created on: Aug 6, 2020
 *      Author: fink
 */

#include <gtest/gtest.h>
#include <Heaters.h>

static std::uint8_t out_pct_primary = 0;
static std::uint8_t out_pct_sparge = 0;

static Heaters::t_outputs outputs;

class Heaters_Tests: public ::testing::Test
{
  protected:
    virtual void SetUp()
    {
      outputs.brew = true;
      outputs.sparge = true;
      out_pct_primary = 0;
      out_pct_sparge = 0;
      heaters.Init();
    }
    virtual void TearDown()
    {
    }
};


/*************************************/

TEST_F(Heaters_Tests, Init)
{
  heaters.Get_Outputs(&outputs);
  EXPECT_EQ(false, outputs.brew);
  EXPECT_EQ(false, outputs.sparge);
}


TEST_F(Heaters_Tests, Primary_0pct_duty)
{
  heaters.Set_Brew_Power(0);

  for (std::uint8_t u = 0; u < 100; u++)
  {
    heaters.Service_100ms();
    heaters.Get_Outputs(&outputs);
    out_pct_primary += (outputs.brew) ? 1 : 0;
    out_pct_sparge += (outputs.sparge) ? 1 : 0;
  }

  EXPECT_EQ(0, out_pct_primary);
  EXPECT_EQ(0, out_pct_sparge);
}


TEST_F(Heaters_Tests, Primary_25pct_duty)
{
  heaters.Set_Brew_Power(25);

  for (std::uint8_t u = 0; u < 100; u++)
  {
    heaters.Service_100ms();
    heaters.Get_Outputs(&outputs);
    out_pct_primary += (outputs.brew) ? 1 : 0;
    out_pct_sparge += (outputs.sparge) ? 1 : 0;
  }

  EXPECT_EQ(25, out_pct_primary);
  EXPECT_EQ(0, out_pct_sparge);
}


TEST_F(Heaters_Tests, Primary_100pct_duty)
{
  heaters.Set_Brew_Power(100);

  for (std::uint8_t u = 0; u < 100; u++)
  {
    heaters.Service_100ms();
    heaters.Get_Outputs(&outputs);
    out_pct_primary += (outputs.brew) ? 1 : 0;
    out_pct_sparge += (outputs.sparge) ? 1 : 0;
  }

  EXPECT_EQ(100, out_pct_primary);
  EXPECT_EQ(0, out_pct_sparge);
}


TEST_F(Heaters_Tests, Sparge_0pct_duty)
{
  heaters.Enable_Sparge_Heater(true);
  heaters.Set_Sparge_Power(0);

  for (std::uint8_t u = 0; u < 100; u++)
  {
    heaters.Service_100ms();
    heaters.Get_Outputs(&outputs);
    out_pct_primary += (outputs.brew) ? 1 : 0;
    out_pct_sparge += (outputs.sparge) ? 1 : 0;
  }

  EXPECT_EQ(0, out_pct_primary);
  EXPECT_EQ(0, out_pct_sparge);
}


TEST_F(Heaters_Tests, Sparge_25pct_duty)
{
  heaters.Enable_Sparge_Heater(true);
  heaters.Set_Sparge_Power(25);

  for (std::uint8_t u = 0; u < 100; u++)
  {
    heaters.Service_100ms();
    heaters.Get_Outputs(&outputs);
    out_pct_primary += (outputs.brew) ? 1 : 0;
    out_pct_sparge += (outputs.sparge) ? 1 : 0;
  }

  EXPECT_EQ(0, out_pct_primary);
  EXPECT_EQ(25, out_pct_sparge);
}


TEST_F(Heaters_Tests, Sparge_25pct_duty_Sparge_Disabled)
{
  heaters.Enable_Sparge_Heater(false);
  heaters.Set_Sparge_Power(25);

  for (std::uint8_t u = 0; u < 100; u++)
  {
    heaters.Service_100ms();
    heaters.Get_Outputs(&outputs);
    out_pct_primary += (outputs.brew) ? 1 : 0;
    out_pct_sparge += (outputs.sparge) ? 1 : 0;
  }

  EXPECT_EQ(0, out_pct_primary);
  EXPECT_EQ(0, out_pct_sparge);
}


TEST_F(Heaters_Tests, Sparge_100pct_duty)
{
  heaters.Enable_Sparge_Heater(true);
  heaters.Set_Sparge_Power(100);

  for (std::uint8_t u = 0; u < 100; u++)
  {
    heaters.Service_100ms();
    heaters.Get_Outputs(&outputs);
    out_pct_primary += (outputs.brew) ? 1 : 0;
    out_pct_sparge += (outputs.sparge) ? 1 : 0;
  }

  EXPECT_EQ(0, out_pct_primary);
  EXPECT_EQ(100, out_pct_sparge);
}

