/**********************************************************************
 BrewManiac
 created by Vito Tai
 Copyright (C) 2015 Vito Tai

 This soft ware is provided as-is. Use at your own risks.
 You are free to modify and distribute this software without removing
 this statement.
 BrewManiac by Vito Tai is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
***********************************************************************/

#ifndef BUZZ_H
#define BUZZ_H

#define BUZZER_TIME_TOLERANCE 5

#include <Arduino.h>

typedef byte SoundId;


// for one shot sound
#define SoundIdTemperatureReached 0
#define SoundIdCountDown 1
#define SoundIdAddHop 2
#define SoundIdWaitUserInteraction 3
#define SoundIdBrewEnd 4

#define SoundIdMashInTempReached SoundIdTemperatureReached
#define SoundIdBoil SoundIdTemperatureReached
#define SoundIdDelayTimeout SoundIdTemperatureReached
#define SoundIdIodineTest  SoundIdTemperatureReached
#define SoundIdAutoTuneFinished SoundIdAddHop
#define SoundIdUserInteractiveNeeded 0
#define SoundIdConfirmUser 0


#define PumpRestSoundId    SoundIdTemperatureReached
#define PumpRestEndSoundId  SoundIdTemperatureReached

#define SoundIdWarnning SoundIdTemperatureReached

extern const byte _soundIdTemperatureReached[] PROGMEM;
extern const byte _soundIdCountDown[] PROGMEM; // 5 seconds HOP_ALTERTING_TIME
extern const byte _soundIdAddHop[] PROGMEM; // 10 seconds
extern const byte _soundIdWaitUserInteraction[] PROGMEM;
extern const byte _soundIdBrewEnd[] PROGMEM;
extern const byte * const _sounds [] PROGMEM;

// a hit & run function
// buzzer thread will handle the pattern & time
extern byte _numberofNtesToPlay;
extern boolean _repeat;
extern boolean _playing;
extern boolean _buzzing;
extern unsigned long _buzzingTime;
extern word _currentPeriod;
extern byte* _ptrCurrentNote;

extern byte* _currentSound;

#pragma GCC diagnostic ignored "-Wstrict-aliasing"

void buzzStartPlay(SoundId id, boolean repeat);
void buzzPlaySound(SoundId id);
void buzzPlaySoundRepeat(SoundId id);
void buzzMute(void);
void buzzOn(void);
void buzzThread(void);

#endif
