/*
 * Distill.h
 *
 *  Created on: Aug 4, 2020
 *      Author: fink
 */

#ifndef SRC_DISTILL_H_
#define SRC_DISTILL_H_

#include "SettingEditor.h"
#include "ui_globals.h"

#include <cstdint>
#include <pgmspace.h>

class DistillRecipe
{
  public:

    typedef enum _DistillingState
    {
      DistillingStateConfirmation,
      DistillingStateBeforeHead,
      DistillingStateHeadConfirmation,
      DistillingStateHead,
      DistillingStateHeart,
      DistillingStateTail,
      DistillingStateEnd,
      DistillingStateManual
    } DistillingState;

    enum
    {
      DistillHeadPwm,
      DistillHeartPwm,
      DistillTailPwm,
    };

    enum
    {
      DistillHeadTemp,
      DistillHeartTemp,
      DistillTailTemp,
      DistillEndTemp,
    };

    DistillRecipe();
    virtual ~DistillRecipe();

    void Init();
    bool Eventhandler(std::uint8_t event, std::uint8_t mask);
    void load();
    void save();
    std::uint8_t pwmOf(std::uint8_t index);
    void setPwmOf(std::uint8_t index, std::uint8_t value);
    std::uint8_t tempOf(std::uint8_t index);
    void setTempOf(std::uint8_t index, std::uint8_t value);

  private:
    std::uint8_t _pwms[3];
    std::uint8_t _temps[4];
    bool _loaded;
};

extern DistillRecipe distillRecipe;


class DistillingController
{
  public:
    DistillingController();

    void setup(void);
    bool eventHandler(std::uint8_t event, std::uint8_t mask);

  private:
    DistillRecipe::DistillingState _state;
    bool _blinkingSettingTemperature;
    std::uint16_t _changeCountDownTime;

    bool Handle_ManualDistilling(std::uint8_t event, std::uint8_t mask);
    bool Handle_BeforeHead(std::uint8_t event, std::uint8_t mask);
    bool Handle_HeadConfirmation(std::uint8_t event, std::uint8_t mask);
    bool Handle_Head(std::uint8_t event, std::uint8_t mask);
    bool Handle_Heart(std::uint8_t event, std::uint8_t mask);
    bool Handle_Tail(std::uint8_t event, std::uint8_t mask);
    bool Handle_End(std::uint8_t event, std::uint8_t mask);
    bool Handle_Confirmation(std::uint8_t event, std::uint8_t mask);

    void Enter_ManualDistilling(void);
    void Enter_AutomaticDistilling(void);
    void automaticDistillFinished(void);
    void changePwmValue(uint8_t pwm);
    bool handleAdjustPwm(e_button_mask mask);
    bool autoDistillerButtonHandler(e_button_mask mask);
    void ChangeCountdownTime(std::int16_t val);
};

extern DistillingController distillingController;

#endif /* SRC_DISTILL_H_ */
