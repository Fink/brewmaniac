/*
 * Distill.cpp
 *
 *  Created on: Aug 4, 2020
 *      Author: fink
 */

#include "Distill.h"
#include "Settings.h"
#include "lcd_interface.h"
#include "Temperature.h"
#include "wi.h"
#include "stages.h"
#include "Pump.h"
#include "buzz.h"
#include "BrewLogger.h"
#include "Menu.h"

int distillGetValue(int index);
void distillSetValue(int index, int value);

DistillRecipe distillRecipe;

DistillRecipe::DistillRecipe()
{
  _loaded = false;
}

DistillRecipe::~DistillRecipe()
{
}

void DistillRecipe::Init()
{
  load();
  settingEditor.setup(distillRecipeItems, &distillGetValue, &distillSetValue);
  settingEditor.displayItem();

}

bool DistillRecipe::Eventhandler(std::uint8_t event, std::uint8_t mask)
{
  if (settingEditor.buttonHandler(static_cast<e_button_mask>(mask)))
  {
    size_t index = (size_t) settingEditor.GetCurrentIndex();
    if (index >= (sizeof(distillRecipeItems) / sizeof(SettingEditor::SettingItem) - 1))
    {
      distillRecipe.save();
      menu.switchApplication(Menu::SETUP_SCREEN);
      return true;
    }

    settingEditor.nextItem();

  }
  return true;
}

void DistillRecipe::load()
{
  if (_loaded)
    return;

  _pwms[DistillHeadPwm]    = settings.Get(Settings::set_DistillHeadPwm);
  _pwms[DistillHeartPwm]   = settings.Get(Settings::set_DistillHeartPwm);
  _pwms[DistillTailPwm]    = settings.Get(Settings::set_DistillTailPwm);

  _temps[DistillHeadTemp]  = settings.Get(Settings::set_DistillHeadTemp);
  _temps[DistillHeartTemp] = settings.Get(Settings::set_DistillHeartTemp);
  _temps[DistillTailTemp]  = settings.Get(Settings::set_DistillTailTemp);
  _temps[DistillEndTemp]   = settings.Get(Settings::set_DistillEndTemp);

  _loaded = true;
}

void DistillRecipe::save()
{
  settings.Set(Settings::set_DistillHeadPwm  , static_cast<float>(_pwms[DistillHeadPwm])  );
  settings.Set(Settings::set_DistillHeartPwm , static_cast<float>(_pwms[DistillHeartPwm]) );
  settings.Set(Settings::set_DistillTailPwm  , static_cast<float>(_pwms[DistillTailPwm] ) );

  settings.Set(Settings::set_DistillHeadTemp , static_cast<float>(_temps[DistillHeadTemp] ) );
  settings.Set(Settings::set_DistillHeartTemp, static_cast<float>(_temps[DistillHeartTemp]) );
  settings.Set(Settings::set_DistillTailTemp , static_cast<float>(_temps[DistillTailTemp] ) );
  settings.Set(Settings::set_DistillEndTemp  , static_cast<float>(_temps[DistillEndTemp]  ) );
}

std::uint8_t DistillRecipe::pwmOf(std::uint8_t index)
{
  return _pwms[index];
}
void DistillRecipe::setPwmOf(std::uint8_t index, std::uint8_t value)
{
  _pwms[index] = value;
}
std::uint8_t DistillRecipe::tempOf(std::uint8_t index)
{
  return _temps[index];
}
void DistillRecipe::setTempOf(std::uint8_t index, std::uint8_t value)
{
  _temps[index] = value;
}

int distillGetValue(int index)
{
  if (index == 0)
    return distillRecipe.tempOf(DistillRecipe::DistillHeadTemp);
  else if (index == 1)
    return distillRecipe.pwmOf(DistillRecipe::DistillHeadPwm);
  else if (index == 2)
    return distillRecipe.tempOf(DistillRecipe::DistillHeartTemp);
  else if (index == 3)
    return distillRecipe.pwmOf(DistillRecipe::DistillHeartPwm);
  else if (index == 4)
    return distillRecipe.tempOf(DistillRecipe::DistillTailTemp);
  else if (index == 5)
    return distillRecipe.pwmOf(DistillRecipe::DistillTailPwm);
  else if (index == 6)
    return distillRecipe.tempOf(DistillRecipe::DistillEndTemp);
  return 0;
}

void distillSetValue(int index, int value)
{
  if (index == 0)
    return distillRecipe.setTempOf(DistillRecipe::DistillHeadTemp, (uint8_t) value);
  else if (index == 1)
    return distillRecipe.setPwmOf(DistillRecipe::DistillHeadPwm, (uint8_t) value);
  else if (index == 2)
    return distillRecipe.setTempOf(DistillRecipe::DistillHeartTemp, (uint8_t) value);
  else if (index == 3)
    return distillRecipe.setPwmOf(DistillRecipe::DistillHeartPwm, (uint8_t) value);
  else if (index == 4)
    return distillRecipe.setTempOf(DistillRecipe::DistillTailTemp, (uint8_t) value);
  else if (index == 5)
    return distillRecipe.setPwmOf(DistillRecipe::DistillTailPwm, (uint8_t) value);
  else if (index == 6)
    return distillRecipe.setTempOf(DistillRecipe::DistillEndTemp, (uint8_t) value);
}

DistillingController distillingController;

DistillingController::DistillingController()
{
}

void DistillingController::setup(void)
{
  _state = DistillRecipe::DistillingStateConfirmation;
  // display confirmation temperature
  //
  uiTitle(STR(Distilling));
  uiPrompt(STR(StartDistilling));
  uiButtonLabel(ButtonLabel(DistillConfirm));
}

bool DistillingController::eventHandler(std::uint8_t event, std::uint8_t mask)
{
  if (_state == DistillRecipe::DistillingStateConfirmation)
    return Handle_Confirmation(event, mask);
  else if (_state == DistillRecipe::DistillingStateBeforeHead)
    return Handle_BeforeHead(event, mask);
  else if (_state == DistillRecipe::DistillingStateHeadConfirmation)
    return Handle_HeadConfirmation(event, mask);
  else if (_state == DistillRecipe::DistillingStateHead)
    return Handle_Head(event, mask);
  else if (_state == DistillRecipe::DistillingStateHeart)
    return Handle_Heart(event, mask);
  else if (_state == DistillRecipe::DistillingStateTail)
    return Handle_Tail(event, mask);
  else if (_state == DistillRecipe::DistillingStateEnd)
    return Handle_End(event, mask);
  else if (_state == DistillRecipe::DistillingStateManual)
    return Handle_ManualDistilling(event, mask);
  return false;
}

void DistillingController::Enter_ManualDistilling(void)
{
  _state = DistillRecipe::DistillingStateManual;
  heatLoadParameters();
  uiClearScreen();
  uiTitle(STR(Manual_Distill));
  uiButtonLabel(ButtonLabel(Up_Down_Heat_Pmp));
  uiTempDisplaySetPosition(TemperatureManualModePosition);
  uiShowPwmLabel();
  uiShowPwmValue(gBoilHeatOutput);
  gIsEnterPwm = true;
  gSettingTemperature = DEFAULT_MANUL_MODE_TEMPERATURE;
  wiReportSettingTemperature();
  uiSetSettingTemperature(gSettingTemperature);
  // display counting time
  uiRunningTimeSetPosition(RunningTimeNormalPosition);
  uiRunningTimeShowInitial(0);
  isCountDownTimeBlinking = false;
  _blinkingSettingTemperature = false;

  setAdjustTemperature(110, 20);

  if (temperature.temperatures[Temperature::TEMP_BREW] >= gSettingTemperature)
  {
    //temp reached
    gIsTemperatureReached = true;
    uiRunningTimeStart();
  }
  else
  {
    gIsTemperatureReached = false;
    // wait to reach temperature setting
  }
  SetEventMask(TemperatureEventMask | ButtonPressedEventMask | TimeoutEventMask);
  setAdjustTemperature(110.0, 20.0);

  wiReportCurrentStage(StageManualMode);
}

bool DistillingController::Handle_ManualDistilling(std::uint8_t event, std::uint8_t mask)
{
  if (event == ButtonPressedEventMask)
  {
    switch (mask)
    {
    case ButtonMaskEnter:
      pump.toggle();
      break;

    case ButtonMaskLongEnter:
      pump.setRestEnabled(!pump.isRestEnabled());
      break;

    case ButtonMaskLongStart:
      _blinkingSettingTemperature = !_blinkingSettingTemperature;
      uiSettingTemperatureBlinking(_blinkingSettingTemperature);
      break;

    case ButtonMaskEnterStart:
      isCountDownTimeBlinking = !isCountDownTimeBlinking;
      if (isCountDownTimeBlinking)
      {
        uiRunningTimeShowInitial(0);
        _changeCountDownTime = 0;
        timers.PauseTimer();
        gIsTemperatureReached = true;  // force to ignore temperature
      }
      else
      {
        // end of input. if zero, make it normal mode
        // else make it count down mode
        isManualModeCountDownMode = (_changeCountDownTime > 0);
        gIsTemperatureReached = false;
      }
      uiRunningTimeBlink(isCountDownTimeBlinking);
      break;

    case ButtonMaskUp:
      if (isCountDownTimeBlinking)
      {
        ChangeCountdownTime(+1);
      }
      else if (_blinkingSettingTemperature)
      {
        adjustSp(+1);
      }
      break;

    case ButtonMaskDown:
      if (isCountDownTimeBlinking)
      {
        ChangeCountdownTime(-1);
      }
      else if (_blinkingSettingTemperature)
      {
        adjustSp(-1);
      }
      break;

    case ButtonMaskContUp:
      if (isCountDownTimeBlinking)
      {
        ChangeCountdownTime(+5);
      }
      else if (_blinkingSettingTemperature)
      {
        adjustSp(+5);
      }
      break;

    case ButtonMaskContDown:
      if (isCountDownTimeBlinking)
      {
        ChangeCountdownTime(-5);
      }
      else if (_blinkingSettingTemperature)
      {
        adjustSp(-5);
      }
      break;

    default:
      handleAdjustPwm(static_cast<e_button_mask>(mask));
      break;
    }

    return true;
  }
  else if (event == TemperatureEventMask)
  {
    // Handle temperature change or other states
    if (!gIsTemperatureReached)
    {
      if (temperature.temperatures[Temperature::TEMP_BREW] >= gSettingTemperature)
      {
        // beep & start counting time

        buzzPlaySound(SoundIdTemperatureReached);

        gIsTemperatureReached = true;

        if (isManualModeCountDownMode)
        {
          uiRunningTimeStartCountDown(_changeCountDownTime * 60);
          timers.SetTimeoutAfter(_changeCountDownTime * 60 * 1000);
        }
        else uiRunningTimeStart();

        brewLogger.event(RemoteEventTemperatureReached);
        wiReportEvent(RemoteEventTemperatureReached);
        return true;
      }
    }
    // Temperate Reached state
  }  // end of temperature handling
  else if (event == TimeoutEventMask)
  {
    buzzPlaySound(SoundIdCountDown);
    isManualModeCountDownMode = false;
    uiRunningTimeStart();
    return true;
  }
  return false;
}

void DistillingController::Enter_AutomaticDistilling(void)
{
  _state = DistillRecipe::DistillingStateBeforeHead;
  // setup distilling mode screen
  distillRecipe.load();

  SetEventMask(TemperatureEventMask /*| ButtonPressedEventMask */);

  //load temperature value
  // setup screen
  uiClearTitle();
  uiDistillingModeTitle();
  uiClearSubTitleRow();
  uiClearPrompt();

  uiDistillingModeStage(DistillStageStart);  // 0 is Start
  uiRunningTimeSetPosition(RunningTimeNormalPosition);
  uiRunningTimeStart();
  // displace temperature
  uiTempDisplaySetPosition(TemperatureAutoModePosition);
  setSettingTemperature((float) distillRecipe.tempOf(DistillRecipe::DistillHeadTemp));

  uiButtonLabel(ButtonLabel(Up_Down_x_Pmp));

  heatLoadParameters();

  heatOn(HeatingModePWM);
  gIsEnterPwm = true;
  uiShowPwmLabel();

  changePwmValue(100);

  brewLogger.startSession(2, TemperatureChartPeriod, false, false);

  brewLogger.stage(StageDistillingPreHeat);
  wiReportCurrentStage(StageDistillingPreHeat);
}

bool DistillingController::Handle_Confirmation(std::uint8_t event, std::uint8_t mask)
{
  switch (mask)
  {
  case ButtonMaskEnter:
    backToMain();
    break;

  case ButtonMaskStart:
    Enter_AutomaticDistilling();
    break;

  case ButtonMaskUp:
    return false;

  case ButtonMaskDown:
    Enter_ManualDistilling();
    break;

  default:
    return false;
  }

  return true;
}

void DistillingController::automaticDistillFinished(void)
{
  heatOff();
  pump.off();
  // buzz and display to end

  uiTempDisplayHide();
  uiRunningTimeStop();

  uiClearScreen();

  uiPrompt(STR(Distill_finished));

  buzzPlaySoundRepeat(SoundIdBrewEnd);
  SetEventMask(TimeoutEventMask);
  timers.SetTimeoutAfter(BREW_END_STAY_DURATION * 1000);

  brewLogger.endSession();

  wiReportEvent(RemoteEventBrewFinished);
}

bool DistillingController::Handle_End(std::uint8_t event, std::uint8_t mask)
{
  (void) mask;
  if (event == TimeoutEventMask)
  {
    buzzMute();
    backToMain();
    return true;
  }
  return false;
}

void DistillingController::changePwmValue(uint8_t pwm)
{
  gBoilHeatOutput = pwm;
  uiShowPwmValue(gBoilHeatOutput);
  wiReportPwm(pwm);
}

bool DistillingController::handleAdjustPwm(e_button_mask mask)
{
  std::int8_t val = 0;
  switch (mask)
  {
  case ButtonMaskUp:
    val = +1;
    break;

  case ButtonMaskDown:
    val = -1;
    break;

  case ButtonMaskContUp:
    val = +5;
    break;

  case ButtonMaskContDown:
    val = -5;
    break;

  default:
     return false;
  }

  adjustPwm(val);

  return true;
}

bool DistillingController::autoDistillerButtonHandler(e_button_mask mask)
{
  switch (mask)
  {
  case ButtonMaskEnter:
    pump.toggle();
    break;

  case ButtonMaskLongEnter:
    pump.setRestEnabled(!pump.isRestEnabled());
    break;

  default:
     return handleAdjustPwm(mask);
  }
  return true;
}

bool DistillingController::Handle_BeforeHead(std::uint8_t event, std::uint8_t mask)
{
  (void) mask;

  if (event == ButtonPressedEventMask)
  {
    return autoDistillerButtonHandler(static_cast<e_button_mask>(mask));
  }
  else if (event == TemperatureEventMask)
  {
    if (temperature.temperatures[Temperature::TEMP_BREW] >= gSettingTemperature)
    {
      // stop heating
      heatProgramOff();
      // prompt for start head
      uiRunningTimeStop();
//        uiRunningTimeHide();
      uiClearPrompt();
      uiPrompt(STR(TurnOnCoolWater));
      uiButtonLabel(ButtonLabel(x_x_x_Ok));
      _state = DistillRecipe::DistillingStateHeadConfirmation;

      buzzPlaySoundRepeat(SoundIdConfirmUser);
      return true;
    }
  }  // TemperatureEventMask
  return false;
}

bool DistillingController::Handle_HeadConfirmation(std::uint8_t event, std::uint8_t mask)
{
  if (event == ButtonPressedEventMask)
  {
    if (mask == ButtonMaskEnter)
    {
      buzzMute();
      _state = DistillRecipe::DistillingStateHead;
      uiClearPrompt();
      uiDistillingModeStage(DistillStageHead);
      uiRunningTimeStart();
      uiButtonLabel(ButtonLabel(Up_Down_x_Pmp));
      uiShowPwmLabel();

      heatOn(HeatingModePWM);
      changePwmValue(distillRecipe.pwmOf(DistillRecipe::DistillHeadPwm));
      setSettingTemperature((float) distillRecipe.tempOf(DistillRecipe::DistillHeartTemp));

      brewLogger.stage(StageDistillingHead);
      wiReportCurrentStage(StageDistillingHead);
      return true;
    }
  }
  return false;
}

bool DistillingController::Handle_Head(std::uint8_t event, std::uint8_t mask)
{
  (void) mask;

  if (event == ButtonPressedEventMask)
  {
    return autoDistillerButtonHandler(static_cast<e_button_mask>(mask));

  }
  else if (event == TemperatureEventMask)
  {
    if (temperature.temperatures[Temperature::TEMP_BREW] >= gSettingTemperature)
    {
      _state = DistillRecipe::DistillingStateHeart;
      uiDistillingModeStage(DistillStageHeart);
      changePwmValue(distillRecipe.pwmOf(DistillRecipe::DistillHeartPwm));
      setSettingTemperature((float) distillRecipe.tempOf(DistillRecipe::DistillTailTemp));

      brewLogger.stage(StageDistillingHeart);
      wiReportCurrentStage(StageDistillingHeart);

      buzzPlaySound(SoundIdTemperatureReached);
      return true;
    }
  }  // TemperatureEventMask
  return false;
}

bool DistillingController::Handle_Heart(std::uint8_t event, std::uint8_t mask)
{
  (void) mask;
  if (event == ButtonPressedEventMask)
  {
    return autoDistillerButtonHandler(static_cast<e_button_mask>(mask));
  }
  else if (event == TemperatureEventMask)
  {
    if (temperature.temperatures[Temperature::TEMP_BREW] >= gSettingTemperature)
    {
      _state = DistillRecipe::DistillingStateTail;
      uiDistillingModeStage(DistillStageTail);
      changePwmValue(distillRecipe.pwmOf(DistillRecipe::DistillTailPwm));
      setSettingTemperature(distillRecipe.tempOf(DistillRecipe::DistillEndTemp));

      brewLogger.stage(StageDistillingTail);
      wiReportCurrentStage(StageDistillingTail);
      buzzPlaySound(SoundIdTemperatureReached);
      return true;
    }
  }  // TemperatureEventMask
  return false;
}

bool DistillingController::Handle_Tail(std::uint8_t event, std::uint8_t mask)
{
  (void) mask;

  if (event == ButtonPressedEventMask)
  {
    return autoDistillerButtonHandler(static_cast<e_button_mask>(mask));
  }
  else if (event == TemperatureEventMask)
  {
    if (temperature.temperatures[Temperature::TEMP_BREW] >= gSettingTemperature)
    {
      _state = DistillRecipe::DistillingStateEnd;
      automaticDistillFinished();
      return true;
    }
  }  // TemperatureEventMask
  return false;
}

void DistillingController::ChangeCountdownTime(std::int16_t val)
{
  _changeCountDownTime += val;
  if (_changeCountDownTime < 0)
  {
    _changeCountDownTime = 0;
  }
  else if (_changeCountDownTime > 600)
  {
    _changeCountDownTime = 600;
  }
  uiRunningTimeShowInitial(_changeCountDownTime * 60);
}
