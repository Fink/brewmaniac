/**********************************************************************
 BrewManiacEx
 created by Vito Tai
 Copyright (C) 2015 Vito Tai

 This soft ware is provided as-is. Use at your own risks.
 You are free to modify and distribute this software without removing
 this statement.
 BrewManiac by Vito Tai is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
***********************************************************************/

#ifndef UI_H
#define UI_H
#define INVALID_TEMP_C -127
#define IS_TEMP_INVALID(t) ((t)<-50.0)

//#include "Pins.h"
#include "resources.h"
#include <Wire.h>

#if LCD_USE_SSD1306 == true

#include "SSD1306.h"
#include "font_cousine_10.h"
SSD1306  display(0x3C,I2C_SDA,I2C_SCL);

#else

#include <LiquidCrystal_I2C.h>

#endif

/**********************************************************************
Constant definition
***********************************************************************/

#define LcdCharDegree ((byte)0)
#define LcdCharSetpoint 1
#define LcdCharPump 2
#define LcdCharRevPump 3
#define LcdCharHeating 4
#define LcdCharRevHeating 5
#define LcdCharRevSpargeHeating 6

#define LcdCharReserved 7



typedef enum _SymbolId{
SymbolCelsius=0,
SymbolSetpoint=1,
SymbolPump=2,
SymbolRevPump=3,
SymbolHeating=4,
SymbolRevHeating=5,
SymbolFahrenheit=6,
SymbolWireless=7,
SymbolWirelessAP=8,
SymbolRevSpargeHeating =9,
SymbolPrimaryHeater=10,
SymbolRevPrimaryHeater=11,
SymbolSecondaryHeater=12,
SymbolRevSecondaryHeater=13
} SymbolId;



#define LCD_COLUMN_NUM 20

#define MashOutStage 7
#define BoilingStage 8
#define CoolingStage 9
#define WhirlpoolStage 10
#define HopStandChillingStage 11
#define HopStandStage 12

#define LeftAligned 0
#define RightAligned 1
#define CenterAligned 2

#define WiStateNotConnected 0
#define WiStateConnected 1
#define WiStateAccessPoint 2

#define PumpStatus_On 1
#define PumpStatus_On_PROGRAM_OFF 2
#define PumpStatus_Off 0

#define HeatingStatus_On 1
#define HeatingStatus_On_PROGRAM_OFF 2
#define HeatingStatus_Off 0

#define HeatingSymbolRow 1
#define HeatingSymbolCol 0

#define PumpSymbolRow 1
#define PumpSymbolCol 19

#define AuxHeatingSymbolRow 2
#define AuxHeatingSymbolCol 0

void uiLcdPrint(byte col,byte row,char* str);

void uiLcdPrint_P(byte col,byte row,const char* str);

void uiLcdClear(byte col,byte row,byte space);

void uiLcdClearAll(void);


void uiLcdLine(byte col,byte row,byte num);

void uiLcdInitialize(void);

void uiLcdAssignSymbol(byte sid,SymbolId symbol);
void uiLcdDrawSymbol(byte col,byte row,byte sid);
void uiLcdDrawSymbolBmp(byte col,byte row,SymbolId symbol);

#include "uiTempTime.h"
/**********************************************************************
variables
***********************************************************************/

extern byte _uibuffer[21];
extern byte ipAddress[4];
extern bool ipSet;

extern char _lcdBuffer[4][21];
void clearLcdBuffer(void);

#if LCD_USE_SSD1306 == true

/**********************************************************************
LCD "driver"
***********************************************************************/

const byte _WirelessSymbol[] PROGMEM ={0x00,0x0E,0x11,0x04,0x12,0x00,0x04,0x00,0x00,0x00,0x00,0x00};
const byte _WirelessAPSymbol[] PROGMEM ={0x00,0x0E,0x11,0x04,0x12,0x00,0x04,0x00,0x00,0x00,0x00,0x00};

const byte _CelsiusSymbol[] PROGMEM = { 0x00, 0x02, 0x05, 0x02, 0x00, 0x3c, 0x26, 0x02, 0x26, 0x3c, 0x00, 0x00};
const byte _FahrenheitSymbol[] PROGMEM = {0x00, 0x02, 0x05, 0x02, 0x00, 0x1c, 0x04, 0x1c, 0x04, 0x04, 0x00, 0x00};
const byte _RevHeatingSymbol[] PROGMEM = { 0x3f, 0x3f, 0x3f, 0x3f, 0x2d, 0x2d, 0x21, 0x2d, 0x2d, 0x2d, 0x3f, 0x3f};
const byte _HeatingSymbol[] PROGMEM = { 0x00, 0x00, 0x00, 0x00, 0x12, 0x12, 0x1e, 0x12, 0x12, 0x12, 0x00, 0x00};
const byte _SetpointSymbol[] PROGMEM = {0x07, 0x01, 0x07, 0x04, 0x07, 0x00, 0x3c, 0x24, 0x24, 0x3c, 0x04, 0x04};
const byte _RevPumpSymbol[] PROGMEM = {0x3f, 0x3f, 0x3f, 0x21, 0x2d, 0x21, 0x3d, 0x3d, 0x3d, 0x3f, 0x3f, 0x3f};
const byte _PumpSymbol[] PROGMEM = {0x00, 0x00, 0x00, 0x1e, 0x12, 0x1e, 0x02, 0x02, 0x02, 0x00, 0x00, 0x00};

const byte _RevSpargeHeatingSymbol[] PROGMEM = { 0x3f, 0x3f, 0x33, 0x2d, 0x3d, 0x3b, 0x37, 0x2f, 0x2d, 0x33, 0x3f, 0x3f};
//const byte SpargeHeatingSymbol[] PROGMEM =    {0x00, 0x00, 0x0c, 0x12, 0x02, 0x04, 0x08, 0x10, 0x12, 0x0c, 0x00, 0x00};



const byte* SymbolMaps[]={
_CelsiusSymbol, //0
_SetpointSymbol, //1
_PumpSymbol, //2
_RevPumpSymbol, //3
_HeatingSymbol, //4
_RevHeatingSymbol, //5
_FahrenheitSymbol, //6
_WirelessSymbol, //7
_WirelessAPSymbol, //8
_RevSpargeHeatingSymbol // 9
};

#define TOP_MARGIN (4)
#define LEFT_MARGIN 4
#define FontWidth 6
#define FontHeight 12
#define LineHeight 14
#define XofCol(col) (LEFT_MARGIN + FontWidth * col)
#define YofRow(row) (TOP_MARGIN + FontHeight * row)

void uiLcdPrint(byte col,byte row,char* str);
void uiLcdPrint_P(byte col,byte row,const char* str);
void uiLcdClear(byte col,byte row,byte space);
void uiLcdClearAll(void);
extern byte _reservedChar2SymbolId[8];

void uiLcdAssignSymbol(byte sid,const SymbolId symbol);
void uiLcdDrawSymbol(byte col,byte row,byte sid);
void uiLcdDrawSymbolBmp(byte col,byte row,SymbolId symbol);
void uiLcdLine(byte col,byte row,byte num);
void uiLcdInitialize(void);
void refreshLcdDisplay(void);

#else //#if LCD_USE_SSD1306 == true
/**********************************************************************
 2004 LCD
***********************************************************************/

/**********************************************************************
"bitmap"s
***********************************************************************/


extern const byte _CelsiusSymbol[8]  PROGMEM;  // [0] degree c sybmol
extern const byte _FahrenheitSymbol[8] PROGMEM;  // [0] degree f symbol
extern const byte _SetpointSymbol[8]  PROGMEM;  // [2] SP Symbol
extern const byte _PumpSymbol[8]  PROGMEM;  // [3] Pump Symbol
extern const byte _RevPumpSymbol[8] PROGMEM;  // [4] Reverse PUMP Symbol
extern const byte _HeatingSymbol[8] PROGMEM;  // [5] HEAT symbol
extern const byte _RevHeatingSymbol[8] PROGMEM;  // [6] reverse HEAT symbol
extern const byte _RevSpargeHeatingSymbol[8] PROGMEM;
extern const byte _WirelessSymbol[8] PROGMEM ;
extern const byte _WirelessAPSymbol[8] PROGMEM;




extern const byte* SymbolMaps[];




#define CreatecCustomChar(buff,idx,bm) uiGetBitmap((byte*)buff,bm); lcd.createChar(idx,(byte*)buff)

void uiGetBitmap(byte *dst,const byte *addr);
extern byte _reservedChar2SymbolId[8];

void uiLcdAssignSymbol(byte sid,const SymbolId symbol);
void uiScanLcdAddress(void);
void uiLcdPrint(byte col,byte row,char* str);
void uiLcdPrint_P(byte col,byte row,const char* str);
void uiLcdClear(byte col,byte row,byte space);
void uiLcdClearAll(void);
void uiLcdLine(byte col,byte row,byte num);
void uiLcdDrawSymbol(byte col,byte row,byte sid);
void uiLcdDrawSymbolBmp(byte col,byte row,SymbolId symbol);
void uiLcdInitialize(void);
void refreshLcdDisplay(void);

#endif //#if LCD_USE_SSD1306 == true

//********************************************************
//* helper functions
//********************************************************

void uiClearRow(byte row);
void uiShowTextAtRow(byte row,char *text,byte alignment,int indent);
void uiShowTextAtRow_P(byte row,const char* text,byte alignment,int indent);
//********************************************************
//* LCD interface
//********************************************************

extern byte _wiStatus;

void uiDisplayWirelessIcon(void);
void uiSetWirelessStatus(byte status);
void uiClearIpAddress(void);
void uiPrintIpAddress(void);
void uiSetIp(byte ip[]);
void uiChangeTemperatureUnit(bool useF);
#define  uiClearTitle() uiClearRow(0)
#define  uiClearSettingRow() uiClearRow(2)
#define  uiClearSubTitleRow() uiClearRow(1)
#define  uiNoMenu() uiClearRow(3)

#define uiClearPrompt()	uiLcdClear(8,2,11);

#define MashExtensionNone 0
#define MashExtensionEnabled 1
#define MashExtensionRunning 2

void uiSetMashExtensionStatus(uint8_t status);
//******************************************************
// General interface
//******************************************************
void uiHeatingStatus(byte status);

extern char const S_STR[] PROGMEM;

void uiAuxHeatingStatus(byte status);

void uiPumpStatus(byte status);
void uiTitle(str_t text);
void uiSubTitle(str_t text);

//****************************************
// For setting
// we have to remember the length of

extern byte _labelLegth;

void uiSettingFieldClear(void);
void uiSettingTitle(str_t text);
void uiSettingTitleAppendNumber(byte number);
// the following code is for show Setting Value
void uiSettingDisplayText(str_t text);
void uiSettingDisplayTextDynamic(char* text);

// for degree symbol

void uiSettingDegreeSymbol(byte value);


void uiSettingDisplayField(float number,byte precision,char unit);

void uiSettingTimeInMinutes(byte minutes);
void uiSettingShowTemperature(float temp,byte precision);
void uiDisplaySettingPercentage(int number);
void uiSettingDisplayNumber(float number,byte precision);
void uiClearScreen(void);


void uiSettingSensorIndex(byte num);
#define HexCode(a)  ((a)<10)? ('0'+(a)):('A'+(a)-10)

void uiSettingSensorAddress(byte address[],float temp);
void wiSendButtonLabel(const byte labelId);

void uiButtonLabel(const byte labelId,const char* text);
void uiPrintSettingTemperature(float settemp);
extern float _currentSettingTemperature;

void uiSetSettingTemperature(float settemp);
void uiHideSettingTemperature(void);
void uiPrintSettingTemperature(float settemp);

void uiClearPwmDisplay(void);
void uiShowPwmLabel(void);
void uiShowPwmValue(byte pwm);
void  uiPrompt(str_t prompt);
void uiAutoModeTitle(void);
void uiAutoModeMashTitle(byte idx,byte num);
void uiAutoModeStage(byte idx);
void uiAutoModeShowGeneralHopNumber(const char* hop,byte number);
void uiAutoModeShowHopNumber(byte number);
void uiAutoModeShowPostBoilHopNumber(byte number);
void uiPreparePasueScreen(str_t message);
void uiAutoModeFinishScreen(void);

extern bool _uiSettingTemperatureBlinking;
extern bool _uiSettingTemperatureHidden;
extern unsigned long _settingTempBlinkTime;
#define SettingTempteratureBlinkHideCycle 350
#define SettingTempteratureBlinkShowCycle 650
void uiSettingTemperatureBlinking(bool blink);

void uiInitialize(void);
void uiLoop(void);
void uiDistillingModeTitle(void);
#define DistillStageStart 0
#define DistillStageHead 1
#define DistillStageHeart 2
#define DistillStageTail 3

void uiDistillingModeStage(byte idx);

#endif
