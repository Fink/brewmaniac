/**********************************************************************
 BrewManiac
 created by Vito Tai
 Copyright (C) 2015 Vito Tai

 This soft ware is provided as-is. Use at your own risks.
 You are free to modify and distribute this software without removing
 this statement.
 BrewManiac by Vito Tai is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
 ***********************************************************************/
#include <Arduino.h>
#include <PID_v1.h>
#include <PID_AutoTune_v0.h>
#include "automation.h"

#include "Buttons.h"
#include "Timers.h"
#include "Temperature.h"
#include "Pump.h"
#include "stages.h"
#include "Distill.h"
#include "SettingEditor.h"
#include "ui_globals.h"
#include "AutoMode.h"
#include "ManualMode.h"
#include "Heaters.h"
#include "Sensors.h"
#include "buzz.h"
#include "lcd_interface.h"
#include "Settings.h"
#include "wi.h"
#include "BrewLogger.h"
#include "Pins.h"
#include "Menu.h"

// *************************
//*  Configuration
// *************************

// *************************
//*  global variables
// *************************

float gSettingTemperature;
float gBoilStageTemperature;
float gPidStart;

std::uint8_t gBoilHeatOutput;
std::uint8_t _currentEventMask;
std::uint8_t _currentEvent;

// the pump/heat on/off is requested by user
//  real pump/heat on/off depends on the temperature
//   and parameter setting

bool gIsHeatOn;

bool isCountDownTimeBlinking;
bool isManualModeCountDownMode;

bool gIsEnterPwm;
bool gIsTemperatureReached;
bool gEnableSpargeWaterHeatingControl;

void SetEventMask(std::uint8_t mask)
{
  _currentEventMask |= mask;
}

void SetEvent(std::uint8_t mask)
{
  _currentEvent |= mask;
}

void ClearEvent()
{
  _currentEvent = 0x00;
}

bool IsCurrentEvent(std::uint8_t mask)
{
  return (_currentEvent & (_currentEventMask & mask));
}

// *************************
//*  heating related function
// *************************

boolean _physicalHeatingOn;
double pidBrewInput =
{ 0.0 };
double pidBrewSetpoint =
{ 0.0 };
double pidBrewOutput =
{ 0.0 };

PID brewPID(&pidBrewInput, &pidBrewOutput, &pidBrewSetpoint, 100.0, 40.0, 0.0, DIRECT);

double pidSpargeOutput =
{ 0.0 };
double pidSpargeInput =
{ 0.0 };
double pidSpargeSetpoint =
{ 0.0 };

PID spargePID(&pidSpargeInput, &pidSpargeOutput, &pidSpargeSetpoint, 100.0, 40.0, 0.0, DIRECT);

PID_ATune autoTune(&pidBrewInput, &pidBrewOutput);

bool _isRunningAutoTune = false;

void endAutoTune(void)
{
  _isRunningAutoTune = false;
  brewPID.SetMode(AUTOMATIC);
}

void cancelAutoTune(void)
{
  autoTune.Cancel();
  endAutoTune();
}

void startAutoTune(void)
{
  pidBrewOutput = PidAutoTuneStartValue;
  autoTune.SetNoiseBand(PidAutoTuneNoise);
  autoTune.SetOutputStep(PidAutoTuneStep);
  autoTune.SetLookbackSec((int) PidAutoTuneLookBack);
  autoTune.SetControlType(1);
  _isRunningAutoTune = true;
}

void saveTunning(void)
{
  // update constant
  double kp = autoTune.GetKp();
  double ki = autoTune.GetKi();
  double kd = autoTune.GetKd();
  brewPID.SetTunings(kp, ki, kd);

  settings.Set(Settings::set_Kp, static_cast<float>(kp));
  settings.Set(Settings::set_Ki, static_cast<float>(ki));
  settings.Set(Settings::set_Kd, static_cast<float>(kd));
  settings.Service();
}

void heatPhysicalOn(void)
{
  if (!_physicalHeatingOn)
  {
    _physicalHeatingOn = true;

    pins.Set_Output(Pins::Out_BrewHeater, Pins::PIN_ON);
    uiHeatingStatus(HeatingStatus_On);
    wiReportHeater(HeatingStatus_On);
  }
}

void heatPhysicalOff(void)
{
  if (_physicalHeatingOn)
  {
    pins.Set_Output(Pins::Out_BrewHeater, Pins::PIN_OFF);
    _physicalHeatingOn = false;
  }
  if (gIsHeatOn)
  {
    uiHeatingStatus(HeatingStatus_On_PROGRAM_OFF);
    wiReportHeater(HeatingStatus_On_PROGRAM_OFF);
  }
  else
  {
    uiHeatingStatus(HeatingStatus_Off);
    wiReportHeater(HeatingStatus_Off);
  }
}

void heaterSpargeOn(void)
{
  DBG_PRINTF("Sparge Heater On\n");
  uiAuxHeatingStatus(HeatingStatus_On);
  wiReportAuxHeater(HeatingStatus_On);
}

typedef enum _PowerState
{
  PowerStateIdle,
  PowerStateHeating,
  PowerStateSpargeHeating,
  PowerStateHeatPending,
  PowerStateSpargePending
} PowerState;

PowerState _gPowerState;
PowerState _gReportedPowerState;

unsigned long _gTimeEnterIdle;

void spargeHeaterControl(void)
{
  if (gEnableSpargeWaterHeatingControl == false)
  {
    pidSpargeOutput = 0.0;
  }
  else
  {
    pidSpargeInput = (double) temperature.temperatures[Temperature::TEMP_SPARGE];
    pidSpargeSetpoint = (double) settings.Get(Settings::set_SpargeWaterTemperature);

    spargePID.Compute();
  }
}

void startHeatingSpargeWater(void)
{
  spargePID.SetMode(AUTOMATIC);
  heaters.Enable_Sparge_Heater(true);
}

void stopHeatingSpargeWater(void)
{
  spargePID.SetMode(MANUAL);
  pidSpargeOutput = 0.0;
  heaters.Enable_Sparge_Heater(false);
}

void heatInitialize(void)
{
  brewPID.SetMode(AUTOMATIC);
  brewPID.SetOutputLimits(0.0, 100.0);
  brewPID.SetSampleTime(1000);
  spargePID.SetMode(AUTOMATIC);
  spargePID.SetOutputLimits(0.0, 100.0);
  spargePID.SetSampleTime(1000);

  _physicalHeatingOn = false;
  gIsHeatOn = false;

  _gPowerState = PowerStateIdle;
  _gReportedPowerState = PowerStateIdle;
  _gTimeEnterIdle = millis();

}
// the should be call before REAL action instead of system startup
void heatLoadParameters(void)
{
  brewPID.SetTunings((double) settings.Get(Settings::set_Kp), (double) (settings.Get(Settings::set_Ki)),
                     (double) settings.Get(Settings::set_Kd));

  gBoilStageTemperature = (float) settings.Get(Settings::set_BoilTemp);
  gBoilHeatOutput = settings.Get(Settings::set_BoilHeat);

  gPidStart = (float) settings.Get(Settings::set_PID_Start);

  if (gPidStart < 1.0)
    gPidStart = 1.0;
  else if (gPidStart > 3.5)
    gPidStart = 3.5;

#if 0 // SerialDebug == true

	Serial.print("gBoilStageTemperature=");
	Serial.println(gBoilStageTemperature);

#endif

}

void heatOff(void)
{
  gIsHeatOn = false;
  _gPowerState = PowerStateIdle;
}

boolean _isPIDMode;

void heatOn(bool pidmode)
{
  _isPIDMode = pidmode;
  gIsHeatOn = true;

  // should run through heating algorithm first
  // so that the correct symbol can be shown
  _gPowerState = PowerStateIdle;
}

void heatProgramOff(void)
{
  _gPowerState = PowerStateIdle;
}

float round025(float num)
{
  int intPart = (int) num;
  return (float) intPart + ((int) ((num - (float) intPart) * 100.0) / 25) * 0.25;
}

void heaterControl(void)
{
  if (!gIsHeatOn)
  {
    pidBrewOutput = 0.0;
    return;
  }

  // heat is ON by requested following.
  if (IS_TEMP_INVALID(temperature.temperatures[Temperature::TEMP_BREW]))
  {
    pidBrewOutput = 0.0;
    return;
  }

  pidBrewInput = temperature.temperatures[Temperature::TEMP_BREW];
  pidBrewSetpoint = gSettingTemperature;

  if (_isRunningAutoTune)
  {
    if (autoTune.Runtime())
    {
      // complete
      saveTunning();
      endAutoTune();
      buzzPlaySound(SoundIdAddHop);
    }
  }
  else
  {
    brewPID.Compute();
  }

  if (gIsEnterPwm)
  {
    pidBrewOutput = static_cast<float>(gBoilHeatOutput);
  }
}

void heatThread(void)
{

  heaterControl();
  spargeHeaterControl();
  if (_gReportedPowerState != _gPowerState)
  {
    if (_gPowerState != PowerStateHeatPending && _gPowerState != PowerStateSpargePending)
    {
      wiUpdateHeaterStatus();
      _gReportedPowerState = _gPowerState;
    }
  }
}

void setSettingTemperature(float temp)
{
  gSettingTemperature = temp;
  uiSetSettingTemperature(gSettingTemperature);
  brewLogger.setPoint(gSettingTemperature);
  wiReportSettingTemperature();
}

float _maxAdjustTemp;
float _minAdjustTemp;

bool togglePwmInput(void)
{
  //turn on/off PWM
  if ( ( temperature.temperatures[Temperature::TEMP_BREW] >= gSettingTemperature ) &&
       ( temperature.temperatures[Temperature::TEMP_BREW] >= gBoilStageTemperature ) )
  {
    if (!gIsEnterPwm)
    {
      uiShowPwmLabel();
      uiShowPwmValue(gBoilHeatOutput);
      gIsEnterPwm = true;

      wiTogglePwm();
      return true;
    }
  }
  else
  {
    if (gIsEnterPwm)
    {
      uiClearPwmDisplay();
      gIsEnterPwm = false;

      wiTogglePwm();
      return true;
    }
  }
  return false;
}

void setAdjustTemperature(float max, float min)
{
  _maxAdjustTemp = max;
  _minAdjustTemp = min;
}

void adjustSp(float adjust)
{
  float temp = gSettingTemperature + adjust;

  if (temp > _maxAdjustTemp)
    temp = _maxAdjustTemp;
  if (temp < _minAdjustTemp)
    temp = _minAdjustTemp;

  setSettingTemperature(temp);
}
void adjustPwm(int adjust)
{
  // gBoilHeatOutput is byte, which is, uhhh,, unsigned

  // excludes the case adjust <0 and gBoilheatOutput ==0
  if (adjust > 0 || gBoilHeatOutput != 0)
    gBoilHeatOutput += adjust;

  if (gBoilHeatOutput > 100)
    gBoilHeatOutput = 100;

  uiShowPwmValue(gBoilHeatOutput);
  DBG_PRINTF("adjustPwm\n");
}

boolean processAdjustButtons(e_button_mask mask)
{
  switch (mask)
  {
  case ButtonMaskUp:
    if (gIsEnterPwm)
      adjustPwm(+1);
    else adjustSp(+0.25);
    break;

  case ButtonMaskDown:
    if (gIsEnterPwm)
      adjustPwm(-1);
    else adjustSp(-0.25);
    break;

  case ButtonMaskContUp:
    if (gIsEnterPwm)
      adjustPwm(+2);
    else adjustSp(+0.75);
    break;

  case ButtonMaskContDown:
    if (gIsEnterPwm)
      adjustPwm(-2);
    else adjustSp(-0.75);
    break;

  default:
    return false;
  }

  return true;
}
void heatLoadParameters(void);

void loadBrewParameters(void)
{
  heatLoadParameters();
  pump.loadParameters();
}

void togglePumpRest(void)
{
  //
  if (pump.isRest())
  {
    // into rest
    //no special manual uiButtonLabel(ButtonLabel(_Pump_Rest_));
    // stop heat
    heatProgramOff();
    buzzPlaySound(PumpRestSoundId);

    wiReportEvent(RemoteEventPumpRest);
  }
  else
  {

    heatOn();
    buzzPlaySound(PumpRestEndSoundId);

    wiReportEvent(RemoteEventPumpRestEnd);
  }
}
// ***************************************************************************
//*  Manual Mode Screen
//*
// ***************************************************************************

void manualModeSetup(void)
{
  manualMode.Init();
}

bool manualModeEventHandler(std::uint8_t event, std::uint8_t mask)
{
  return manualMode.EventHandler(event, mask);
}

// *************************
//*  Auto Mode Screen
// *************************

void autoModeSetup(void)
{
  autoMode.Init();
}

bool autoModeEventHandler(byte event, std::uint8_t mask)
{
  return autoMode.EventHandler(event, mask);
}

// *************************
//*  Main Screen
// *************************

void mainSetup(void)
{
  uiClearScreen();

  uiTitle(STR(welcome));
  uiButtonLabel(ButtonLabel(Manual_Auto_Setup));
  uiTempDisplaySetPosition(TemperatureMainScreenPosition);

  uiPrintIpAddress();
  wiReportCurrentStage(StageIdleScreen);
}

// main screen
//  -down button-> manual
//  -start button->auto
//  -ener button ->setup

// *************************
//*  Screen switch
// *************************

void backToMain(void)
{
  brewLogger.abortSession();
  // turn pump & heat off
  heatOff();
  stopHeatingSpargeWater();
  pump.off();
  buzzMute();
  uiRunningTimeBlink(false);  // stop blink if any. additional time print will be done
  // however, it will be clear later, before enter "Main"

  if (_isRunningAutoTune)
    cancelAutoTune();
  //
  settings.Service();
  menu.switchApplication(Menu::MAIN_SCREEN);
}
// *************************
//*  Main procedure
// *************************
void uiPrintInitialScreen(void)
{
  uiClearScreen();
  uiTitle(STR(welcome));

  uiShowTextAtRow_P(1, STR(Initialization), CenterAligned, 1);
}

void startBrewManiac()
{
  menu.switchApplication(Menu::MAIN_SCREEN);
}

void brewmaniac_setup()
{

  settings.Init();
  automation.load();

  Wire.begin(pins.Get_Mapped_Data_Pin(Pins::Data_SDA), pins.Get_Mapped_Data_Pin(Pins::Data_SCL));

  pins.Init();

  timers.Init();
  buttons.Init();
  sensors.Init();
  temperature.Init(static_cast<bool>(settings.Get(Settings::set_Fake_Heat)));
  heaters.Init();
  menu.Init();

  uiInitialize();

  heatInitialize();

  uiPrintInitialScreen();

  wiInitialize();
}

//*********************************************************************
//*********************************************************************

void brewmaniac_loop()
{
  bool updateLcd;
  static Heaters::t_outputs outputs =
  { false, false };

  // Check for temperature event
  static float old_brew_temp = 0;
  static float old_sparge_temp = 0;

  if (old_brew_temp != temperature.temperatures[Temperature::TEMP_BREW])
  {
    old_brew_temp = temperature.temperatures[Temperature::TEMP_BREW];
    SetEvent(TemperatureEventMask);
  }
  if (old_sparge_temp != temperature.temperatures[Temperature::TEMP_SPARGE])
  {
    old_sparge_temp = temperature.temperatures[Temperature::TEMP_SPARGE];
    SetEvent(TemperatureEventMask);
  }

  // Check for button event
  e_button_mask button_mask = buttons.ReadButtons();
  if (button_mask != ButtonMaskNone)
  {
    SetEvent(ButtonPressedEventMask);
  }

  // Check for timer event
  Timers::e_timer_type timer_type = timers.Any_Timeout_Occured();
  if (timer_type != Timers::TIMER_NONE)
  {
    SetEvent(TimeoutEventMask);
  }



  // Handle Temperature Event
  if (IsCurrentEvent(TemperatureEventMask))
  {
    wiLcdBufferBegin();
    updateLcd = (menu.RunCurrentEventHandler(TemperatureEventMask, ButtonMaskNone));
    wiLcdBufferEnd(updateLcd);
  }


  // Handle Timer Event
  if (IsCurrentEvent(TimeoutEventMask))
  {
    if ( timer_type & Timers::TIMER_RECURRING_100ms)
    {
      heaters.Set_Brew_Power(pidBrewOutput);
      heaters.Set_Sparge_Power(pidSpargeOutput);
      heaters.Service_100ms();
      heaters.Get_Outputs(&outputs);
      pins.Set_Output(Pins::Out_BrewHeater, outputs.brew);
      pins.Set_Output(Pins::Out_SpargeHeater, outputs.sparge);
    }

    if ( timer_type & Timers::TIMER_RECURRING_1s)
    {
      sensors.Service();
      temperature.Service();
      brewLogger.temperatures(temperature.temperatures);
      wiReportPwm(static_cast<std::uint8_t>(pidBrewOutput));
      DBG_PRINTF("out_brew: %f/%d, out_sparge: %f/%d\r\n", pidBrewOutput, outputs.brew, pidSpargeOutput, outputs.sparge);
    }

    if ( ( timer_type & Timers::TIMER_NORMAL ) || ( timer_type & Timers::TIMER_AUX ) )
    {
      wiLcdBufferBegin();
      updateLcd = (menu.RunCurrentEventHandler(TimeoutEventMask, timer_type));
      wiLcdBufferEnd(updateLcd);
    }
  }


  // Handle Button Event
  if (IsCurrentEvent(ButtonPressedEventMask))
  {
    if ( button_mask == ButtonMaskUpDown )
    {
      wiLcdBufferBegin();
      backToMain();
      wiLcdBufferEnd(true);
    }
    if ( button_mask == ButtonMaskUpDownStart )
    {
      refreshLcdDisplay();
    }
    else
    {
      wiLcdBufferBegin();
      menu.RunCurrentEventHandler(ButtonPressedEventMask, button_mask);
      wiLcdBufferEnd(true);
    }
  }

  if (pump.restEvent())
  {
    if (IsCurrentEvent(PumpRestEventMask))
    {
      wiLcdBufferBegin();
      menu.RunCurrentEventHandler(PumpRestEventMask, 0);
      wiLcdBufferEnd(true);
    }
  }

  uiLoop();

  heatThread();
  pump.run();
  buzzThread();

  wiThread();
  ClearEvent();
}
