/*
 * Temperature.cpp
 *
 *  Created on: Jul 29, 2020
 *      Author: fink
 */

#include "Temperature.h"
#include "config.h"
#include "lcd_interface.h"
#include "Sensors.h"
#include "debug.h"

Temperature temperature;

#define C2F(d)  ((d) * 1.8 + 32)

Temperature::Temperature() :
    SensorForBrew
    { 0 }, SensorForSparge
    { 0 }, temperatures
    { 0.0, 0.0 }, _filtered_temp
    { 10.0, 10.0 }, _fake_heat
    { 0.0 }
{
}

Temperature::~Temperature()
{
}

void Temperature::Init(bool fake_heat)
{
  _fake_heat = fake_heat;
}

void Temperature::Service(void)
{
  if (_fake_heat)
  {
    extern double pidBrewOutput;
    extern double pidSpargeOutput;
    temperatures[TEMP_BREW] += (0.0083 * pidBrewOutput) - 0.5;
    temperatures[TEMP_SPARGE] += (0.0083 * pidSpargeOutput) - 0.5;
    if (temperatures[TEMP_BREW] < -1.0)
      temperatures[TEMP_BREW] = 0.0;
    if (temperatures[TEMP_SPARGE] < -1.0)
      temperatures[TEMP_SPARGE] = 0.0;
//    DBG_PRINTF("temperatures[Temperature::TEMP_BREW] %f, temperatures[Temperature::TEMP_SPARGE] %f\r\n", temperatures[TEMP_BREW], temperatures[TEMP_SPARGE]);
  }
  else
  {
    for (std::uint8_t i = 0; i < TEMP_COUNT; i++)
    {
      temperatures[i] = AddFilterValue(i, sensors.result[i]);
    }
  }
}

float Temperature::AddFilterValue(std::uint8_t idx, float value)
{
  _filtered_temp[idx] = _filtered_temp[idx] + LowPassFilterParameter * (value - _filtered_temp[idx]);
  return _filtered_temp[idx];
}

