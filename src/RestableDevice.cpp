/*
 * RestableDevice.cpp
 *
 *  Created on: Aug 3, 2020
 *      Author: fink
 */

#include "RestableDevice.h"
#include "Timers.h"
#include "lcd_interface.h"
#include "Temperature.h"
#include "debug.h"


RestableDevice::RestableDevice()
{
  _physicalOn = false;
  _isDeviceOn = false;
  _isRestStateChanged = false;
  _restEnabled = false;
}

RestableDevice::~RestableDevice()
{
}

void RestableDevice::turnPhysicalOn(void)
{
  if (_physicalOn)
    return;
  deviceOn();
  _physicalOn = true;
  _lastSwitchOnTime = timers.current_time_in_ms;
}

void RestableDevice::turnPhysicalOff(void)
{
  deviceOff(_isDeviceOn);
  _physicalOn = false;
}

void RestableDevice::setCycle(std::uint32_t cycle, std::uint32_t rest)
{
  _restTime = rest;
  _cycleTime = cycle;
}

void RestableDevice::resetRest(void)
{
  _restEnabled = false;
  _isRestStateChanged = false;
}

void RestableDevice::setStopTemp(float temp)
{
  _stopTemp = temp;
}

void RestableDevice::off(void)
{
  if (!_isDeviceOn)
    return;
  _isDeviceOn = false;
  turnPhysicalOff();
}

void RestableDevice::on(void)
{
  if (_isDeviceOn)
    return;
  _isDeviceOn = true;

  turnPhysicalOn();
}

void RestableDevice::toggle(void)
{
  if (_isDeviceOn)
    off();
  else on();
}

bool RestableDevice::isRestEnabled(void)
{
  return _restEnabled;
}
bool RestableDevice::isOn(void)
{
  return _isDeviceOn;
}

bool RestableDevice::isRest(void)
{
  return !_physicalOn;
}
bool RestableDevice::isPhysicalOn(void)
{
  return _physicalOn;
}

void RestableDevice::setRestEnabled(bool enable)
{
  _restEnabled = enable;

  if (_restEnabled && _physicalOn)  // restart counting time
    _lastSwitchOnTime = timers.current_time_in_ms;

  if (!_restEnabled && _isDeviceOn)
  {
    if (!_physicalOn)
    {
      turnPhysicalOn();
    }
  }
}

bool RestableDevice::restEvent(void)
{
  if (_isRestStateChanged)
  {
    _isRestStateChanged = false;
    return true;
  }
  return false;
}

void RestableDevice::run(void)
{
  if (!_isDeviceOn)
    return;


  // overheat temperature protection
  if (!IS_TEMP_INVALID(temperature.temperatures[Temperature::TEMP_BREW]) && (temperature.temperatures[Temperature::TEMP_BREW] >= _stopTemp))
  {
    if (_physicalOn)
    {
      turnPhysicalOff();

      // don't affect heating status if it stopTemp hit
      //if(!IS_TEMP_INVALID(gCurrentTemperature)) _isRestStateChanged = true;
    }
    return;
  }
  else  // of if(temperature.temperatures[Temperature::TEMP_BREW] >= _pumpStopTemp)
  {
    // if under pump stop temperature

    // device is "ON" (or it returns at beginning of this function)
    if (!_restEnabled)
    {
      // not in REST state.
      if (!_physicalOn)
        turnPhysicalOn();
      return;
    }
    if ((timers.current_time_in_ms - _lastSwitchOnTime) < (unsigned long) _cycleTime)
    {
      if (!_physicalOn)
      {
        DBG_PRINTF("-End Pump Rest-");
        turnPhysicalOn();
        _isRestStateChanged = true;
      }
    }
    else
    {
      if (_restTime > 0)
      {
        // pump rest state, heat will be off!
        if (_physicalOn)
        {
          DBG_PRINTF("-Pump Rest-");
          turnPhysicalOff();
          _isRestStateChanged = true;
        }
      }
      if ((timers.current_time_in_ms - _lastSwitchOnTime) >= (_cycleTime + _restTime))
      {
        _lastSwitchOnTime = timers.current_time_in_ms;
        // pump will be truned-on on next "run()" call.
        //Serial.println("on time start");
      }
    }
  }  // end of else //if if(temperature.temperatures[Temperature::TEMP_BREW] >= _pumpStopTemp)
}
;
