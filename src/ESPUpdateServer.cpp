#include <Arduino.h>
#include "config.h"
#include <WiFi.h>
#include <ESP32WebServer.h>
#include <ESPmDNS.h>
#include <Update.h>
#include "debug.h"

static char *_username;
static char *_password;
static char* _host;
static ESP32WebServer server(UPDATE_SERVER_PORT);


/*
 * Server Index Page
 */

const char *serverIndex = "<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>"
                          "<form method='POST' action='#' enctype='multipart/form-data' id='upload_form'>"
                          "<input type='file' name='update'>"
                          "<input type='submit' value='Update'>"
                          "</form>"
                          "<div id='prg'>progress: 0%</div>"
                          "<script>"
                          "$('form').submit(function(e){"
                          "e.preventDefault();"
                          "var form = $('#upload_form')[0];"
                          "var data = new FormData(form);"
                          " $.ajax({"
                          "url: '/update',"
                          "type: 'POST',"
                          "data: data,"
                          "contentType: false,"
                          "processData:false,"
                          "xhr: function() {"
                          "var xhr = new window.XMLHttpRequest();"
                          "xhr.upload.addEventListener('progress', function(evt) {"
                          "if (evt.lengthComputable) {"
                          "var per = evt.loaded / evt.total;"
                          "$('#prg').html('progress: ' + Math.round(per*100) + '%');"
                          "}"
                          "}, false);"
                          "return xhr;"
                          "},"
                          "success:function(d, s) {"
                          "console.log('success!')"
                          "},"
                          "error: function (a, b, c) {"
                          "}"
                          "});"
                          "});"
                          "</script>";

void ESPUpdateServer_setup(const char *host, const char *user, const char *pass)
{
  _host = (char*)host;
  _username = (char*) user;
  _password = (char*) pass;

  DBG_PRINTF("Update using host:%s, username:%s, password:%s\r\n", _host, _username, _password);

  if (!MDNS.begin(host))
  {
    DBG_PRINTF("Error setting up MDNS responder!\r\n");
    while (1)
    {
      delay(1000);
    }
  }
  DBG_PRINTF("mDNS responder started\r\n");

  server.on("/", HTTP_GET, []()
  {
    server.sendHeader("Connection", "close");
    server.send(200, "text/html", serverIndex);
  });

  server.on("/update", HTTP_POST, []()
  {
    server.sendHeader("Connection", "close");
    server.send(200, "text/plain", (Update.hasError()) ? "FAIL" : "OK");
    ESP.restart();
  },
            []()
            {
              HTTPUpload& upload = server.upload();
              if (upload.status == UPLOAD_FILE_START)
              {
                DBG_PRINTF("Update: %s\r\n", upload.filename.c_str());
                if (!Update.begin(UPDATE_SIZE_UNKNOWN))
                {
              Update.printError(Serial);
            }
          }
          else if (upload.status == UPLOAD_FILE_WRITE)
          {
            if (Update.write(upload.buf, upload.currentSize) != upload.currentSize)
            {
              Update.printError(Serial);
            }
          }
          else if (upload.status == UPLOAD_FILE_END)
          {
            if (Update.end(true))
            {
              DBG_PRINTF("Update Success: %u\nRebooting...\r\n", upload.totalSize);
            }
            else
            {
              Update.printError(Serial);
            }
          }
        });
  server.begin();
}

void ESPUpdateServer_loop(void)
{
  server.handleClient();
}
