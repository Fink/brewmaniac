/*
 * Menu.h
 *
 *  Created on: Aug 18, 2020
 *      Author: fink
 */

#ifndef SRC_MENU_H_
#define SRC_MENU_H_

#include "SettingEditor.h"
#include "Settings.h"
#include "resources.h"
#include "Temperature.h"
#include "ui_globals.h"
#include <cstdint>

typedef void (*SetupFunc)(void);
typedef bool (*EventHandlerFunc)(std::uint8_t event, std::uint8_t mask);


typedef struct _CScreen
{
    SetupFunc setup;
    EventHandlerFunc eventHandler;
} CScreen;


// Free functions since they are referenced by pointers
void backToMain(void);
void setEventMask(std::uint8_t);

void mainSetup(void);
bool mainEventHandler(std::uint8_t, std::uint8_t mask);

void menuSetup(void);
bool menuEventHandler(std::uint8_t, std::uint8_t mask);

void settingPidSetup(void);
bool settingPidEventHandler(std::uint8_t, std::uint8_t mask);

void settingUnitSetup(void);
bool settingUnitEventHandler(std::uint8_t, std::uint8_t mask);

void settingAutoSetup(void);
bool settingAutoEventHandler(std::uint8_t, std::uint8_t mask);

void manualModeSetup(void);
bool manualModeEventHandler(std::uint8_t, std::uint8_t mask);

void autoModeSetup(void);
bool autoModeEventHandler(std::uint8_t, std::uint8_t mask);

void miscSettingSetup(void);
bool miscSettingEventHandler(std::uint8_t, std::uint8_t mask);

void autoTuneMenuSetup(void);
bool autoTuneMenuEventHandler(std::uint8_t, std::uint8_t mask);

void sensorMenuSetup(void);
bool sensorMenuEventHandler(std::uint8_t, std::uint8_t mask);

void spargeMenuSetup(void);
bool spargeMenuEventHandler(std::uint8_t, std::uint8_t mask);

void distillRecipeSetup(void);
bool distillRecipeEventHandler(std::uint8_t, std::uint8_t mask);

void distillingSetup(void);
bool distillingEventHandler(std::uint8_t, std::uint8_t mask);

void changeAutomationTime(byte stage, byte time);
void changeAutomationTemperature(byte stage, float value);
void finishAutomationEdit(void);


void Display_Offset100(int data);
void Display_Multiply250(int data);
void Display_Multiply50(int data);
void Display_SimpleInteger(int data);
void Display_TempShift50Divide10(int data);
void Display_TempDivide10(int data);
void Display_Percentage(int data);
void Display_Resolution(int value);
void Display_OnOff(int value);
void Display_DegreeSymbol(int value);
void Display_InsideOutside(int value);
void Display_YesNo(int value);
void Display_Time(int value);
void Display_TimeOff(int value);
void Display_SimpleTemperature(int value);
void Display_HotColdOff(int value);
void Display_StageTemperature(int value);
void Display_IntegerPlusOne(int data);

static constexpr const char* level1Menu[ ] =
{
    STR(PID_PWM),
    STR(Unit_Parameters),
    STR(Set_Automation),
    STR(Misc_Setting),
    STR(Sensor_Setting),
    STR(Distill_Recipe)
};

static constexpr CScreen allScreens[ ] =
{
{ &mainSetup, &mainEventHandler, },
{ &menuSetup, &menuEventHandler, },
{ &settingPidSetup, &settingPidEventHandler, },
{ &settingUnitSetup, &settingUnitEventHandler, },
{ &settingAutoSetup, &settingAutoEventHandler, },
{ &manualModeSetup, &manualModeEventHandler, },
{ &autoModeSetup, &autoModeEventHandler, },
{ &miscSettingSetup, &miscSettingEventHandler, },
{ &sensorMenuSetup, &sensorMenuEventHandler, },
{ &distillRecipeSetup, &distillRecipeEventHandler, },
{ &distillingSetup, &distillingEventHandler, }
};

static constexpr SettingEditor::SettingItem pidSettingItems[ ] =
{
{ &Display_SimpleInteger, Settings::set_Kp},
{ &Display_SimpleInteger, Settings::set_Ki},
{ &Display_SimpleInteger, Settings::set_Kd},
{ &Display_Percentage, Settings::set_BoilHeat},
{ &Display_TempDivide10, Settings::set_PID_Start},
};

static constexpr SettingEditor::SettingItem unitSettingItems[ ] =
{
{ &Display_DegreeSymbol, Settings::set_TempUnit },
{ &Display_SimpleTemperature, Settings::set_BoilTemp },  // celius
{ &Display_Time, Settings::set_PumpCycle},
{ &Display_Time, Settings::set_PumpRest},
{ &Display_OnOff, Settings::set_PumpPreMash},
{ &Display_OnOff, Settings::set_PumpOnMash},
{ &Display_OnOff, Settings::set_PumpOnMashOut},
{ &Display_OnOff, Settings::set_PumpOnBoil},
{ &Display_SimpleTemperature, Settings::set_TempPumpRest },  // celius
{ &Display_OnOff, Settings::set_PID_DoughIn},
{ &Display_OnOff, Settings::set_PidPipe },
{ &Display_YesNo, Settings::set_SkipAddMalt },
{ &Display_YesNo, Settings::set_SkipRemoveMalt},
{ &Display_YesNo, Settings::set_SkipIodineTest},
{ &Display_TimeOff, Settings::set_SkipIodineTest },
{ &Display_HotColdOff, Settings::set_Whirlpool }
};

static constexpr SettingEditor::SettingItem miscSettingItems[ ] =
{
{ &Display_YesNo, Settings::set_Init },
{ &Display_YesNo, Settings::set_Fake_Heat },
{ &Display_YesNo, Settings::set_NoDelayStart   },
{ &Display_YesNo, Settings::set_ButtonFeedback },
{ &Display_SimpleInteger, Settings::set_PumpPrimeCount},
{ &Display_Multiply250, Settings::set_PumpPrimeOnTime },
{ &Display_Multiply250, Settings::set_PumpPrimeOffTime },
{ &Display_YesNo, Settings::set_SpargeWaterEnable },
{ &Display_YesNo, Settings::set_SpargeWaterTempeControl},
{ &Display_IntegerPlusOne, Settings::set_SpargeWaterSensorIndex },
{ &Display_SimpleTemperature, Settings::set_SpargeWaterTemperature }
};

static const SettingEditor::SettingItem distillRecipeItems[ 07 ] PROGMEM =
{
{ &Display_SimpleTemperature, static_cast<Settings::db_t>(0) },
{ &Display_Percentage,        static_cast<Settings::db_t>(0) },
{ &Display_SimpleTemperature, static_cast<Settings::db_t>(0) },
{ &Display_Percentage,        static_cast<Settings::db_t>(0) },
{ &Display_SimpleTemperature, static_cast<Settings::db_t>(0) },
{ &Display_Percentage,        static_cast<Settings::db_t>(0) },
{ &Display_SimpleTemperature, static_cast<Settings::db_t>(0) }
};

class Menu
{
  public:
    typedef enum _ScreenIdType
    {
      MAIN_SCREEN,
      SETUP_SCREEN,
      PID_SETTING_SCREEN,
      UNIT_SETTING_SCREEN,
      AUTO_SETTING_SCREEN,
      MANUAL_MODE_SCREEN,
      AUTO_MODE_SCREEN,
      MISC_SETTING_SCREEN,
      SENSOR_SCREEN,
      DISTILL_SETTING_SCREEN,
      DISTILLING_MODE_SCREEN,
      END_OF_SCREEN
    } ScreenIdType;

    const ScreenIdType level1Screens[ 6 ] =
    {
        PID_SETTING_SCREEN,
        UNIT_SETTING_SCREEN,
        AUTO_SETTING_SCREEN,
        MISC_SETTING_SCREEN,
        SENSOR_SCREEN,
        DISTILL_SETTING_SCREEN
    };

    Menu();
    virtual ~Menu();

    void Init();
    void switchApplication(std::uint8_t screenId);
    bool RunCurrentEventHandler(std::uint8_t event, std::uint8_t mask);
    int ToTempForEditing(float t);
    float TempFromEditing(int t);

    std::uint8_t pidSettingAux;
    std::uint8_t postBoilHopIndex;
    std::uint8_t editingStage;
    std::uint8_t editingStageAux;
    std::uint8_t currentLevelOne = 0;
    std::uint8_t sensorSelection;
    bool sensorSelected[Temperature::TEMP_COUNT];
    int maxHopTime;  // to make sure hop time is in order

    static const std::uint8_t SpargeHeaterEnableIndex = 6;
    static const std::uint8_t SpargeHeaterSettingNumber = 5;
    static const std::uint8_t SpargeTemperatureControlIndex = 6;
    static const std::uint8_t SpargeSensorIndex = 7;
    static const std::uint8_t SpargeTemperatureIndex = 8;
    static const std::uint8_t max_stage_time = 140;
    static const std::uint8_t min_stage_time = 1;


  private:
    const CScreen *_currentScreen;
};

extern Menu menu;

#endif /* SRC_MENU_H_ */
