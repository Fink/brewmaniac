/*
 * Settings.cpp
 *
 *  Created on: Aug 21, 2020
 *      Author: fink
 */

#include "Settings.h"
#include "FsEeprom.h"
#include "debug.h"
#include <cstring>

Settings settings;

#define _DB_NAME_LENGTH 14

typedef struct
{
    char name[_DB_NAME_LENGTH];
    char webpar[_DB_NAME_LENGTH];
    std::int32_t min;
    std::int32_t max;
    std::int32_t def;
    std::int16_t scale;
}const dbItem_t;

#define ITEM(Index, Name, WebPar, Min, Max, Default, Scale) {Name, WebPar, Min, Max, Default, Scale},
const dbItem_t DBItem[Settings::DB_COUNT] =
{
DB_ITEMS };
#undef ITEM

Settings::Settings()
{
}

Settings::~Settings()
{
}

void Settings::Init()
{
  std::uint16_t dbIndex;
  _ee_size = (DB_COUNT * sizeof(std::int32_t));
  _ee_address = FsEEPROM.begin(_ee_size);

  std::memcpy(_db_ram, _ee_address, _ee_size);

  if (_db_ram[set_Init] == 0)
  {
    DBG_PRINTF("Loaded EEPROM data\r\n");
  }
  else
  {
    DBG_PRINTF("re-initialized db from EEPROM data\r\n");

    for (dbIndex = 0; dbIndex < DB_COUNT; dbIndex++)
    {
      _db_ram[dbIndex] = DBItem[dbIndex].def;
    }
  }

  for (dbIndex = 0; dbIndex < DB_COUNT; dbIndex++)
  {
    DBG_PRINTF("%s -> %d\r\n", DBItem[dbIndex].name, _db_ram[dbIndex]);
  }
}

void Settings::Service()
{
  if (_dirty)
  {
    _dirty = false;
    std::memcpy(_ee_address, _db_ram, _ee_size);
    FsEEPROM.commit();
    DBG_PRINTF("Saved EEPROM data\r\n");
  }
}

void Settings::GetText(db_t Index, char *dbString)
{
  std::strncpy(dbString, DBItem[Index].name, _DB_NAME_LENGTH);
}

void Settings::GetWebPar(db_t Index, char *dbString)
{
  if (DBItem[Index].webpar != NULL)
  {
    std::strncpy(dbString, DBItem[Index].webpar, _DB_NAME_LENGTH);
  }
}

float Settings::Get(db_t idx)
{
  if (idx < DB_COUNT)
  {
    return (static_cast<float>(_db_ram[idx] / static_cast<float>(DBItem[idx].scale)));
  }
  return -666.0;
}

std::uint32_t Settings::Get_u(db_t idx)
{
  if (idx < DB_COUNT)
  {
    return static_cast<std::uint32_t>(_db_ram[idx] / DBItem[idx].scale);
  }
  return 0xFFFFFFFF;
}

float Settings::GetMin(db_t idx)
{
  if (idx < DB_COUNT)
  {
    return static_cast<float>(DBItem[idx].min / static_cast<float>(DBItem[idx].scale));
  }
  return -666.0;
}

float Settings::GetMax(db_t idx)
{
  if (idx < DB_COUNT)
  {
    return static_cast<float>(DBItem[idx].max / static_cast<float>(DBItem[idx].scale));
  }
  return -666.0;
}

bool Settings::Set(db_t idx, float f)
{
  if (idx < DB_COUNT)
  {
    _db_ram[idx] = static_cast<std::int32_t>(f * static_cast<float>(DBItem[idx].scale));
    _dirty = true;
    return true;
  }
  return false;
}

bool Settings::Set(db_t idx, std::uint32_t u)
{
  if (idx < DB_COUNT)
  {
    _db_ram[idx] = u;
    _dirty = true;
    return true;
  }
  return false;
}

#undef _DB_NAME_LENGTH
