/*
 * UI.h
 *
 *  Created on: Jul 28, 2020
 *      Author: fink
 */

#ifndef SRC_UI_H_
#define SRC_UI_H_

class UI
{
public:
	UI();
	virtual ~UI();

	void Init();
};

#endif /* SRC_UI_H_ */
