/*
 * mystrlib.cpp
 *
 *  Created on: Jul 29, 2020
 *      Author: fink
 */

#include "mystrlib.h"

std::uint8_t sprintIntDigit(char *buff, int number,int base)
{

  std::uint8_t indx=0;
    while (base > 1)
    {
        int digit= number /base;
        buff[indx++]=('0' + digit);
        number = number - digit  * base;
      base = base /10;

    }
    buff[indx++]=('0' + number);
    return indx;
}

std::uint8_t sprintInt(char *buff,int number)
{
  std::uint8_t sign=0;
  if(number <0)
  {
    *buff='-';
    buff = buff +1;
    number = 0- number;
    sign=1;
  }
  int base=1;

    if(number >= 10)
    {
      int cb=number;
      while(cb > 9)
      {
         cb = cb / 10;
        base = base *10;
      }
  }

  return sign+sprintIntDigit(buff,number,base);
}


std::uint8_t sprintFloat(char *buff,float value,std::uint8_t precision)
{
  std::uint8_t len=0;

    if(value <0)
    {
        buff[0]='-';
  buff = buff;
  value = 0.0- value;
  len=1;
    }

    int base=1;
    float r=0.5;

    for(std::uint8_t p=0; p < precision; p++)
    {
        base = base * 10;
        r= r * 0.1;
    }
    float number=value+ r;


    if (number >= 1.0)
    {
        int integer=(int)number;

        len +=sprintInt(buff+len,integer);
        number -= (float)integer;


    }
    else
    {
          buff[len]='0';
          len += 1;
    }


    if(precision == 0) return len;

       buff[len++]='.';

    number = number * base;
    len+=sprintIntDigit(buff+len,(int)number,base/10);

    return len;
}


