/**********************************************************************
 BrewManiac
 created by Vito Tai
 Copyright (C) 2015 Vito Tai

 This soft ware is provided as-is. Use at your own risks.
 You are free to modify and distribute this software without removing
 this statement.
 BrewManiac by Vito Tai is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
***********************************************************************/

#ifndef MY_STRING_LIB
#define MY_STRING_LIB

#include <cstdint>

std::uint8_t sprintIntDigit(char *buff, int number,int base);
std::uint8_t sprintInt(char *buff,int number);
std::uint8_t sprintFloat(char *buff,float value,std::uint8_t precision);

#endif
