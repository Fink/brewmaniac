/*
 * debug.h
 *
 *  Created on: Aug 6, 2020
 *      Author: fink
 */

#ifndef SRC_DEBUG_H_
#define SRC_DEBUG_H_

#include "config.h"

#if SerialDebug == true
#define DebugOut(a) DebugPort.print(a)
#define DBG_PRINTF(...) DebugPort.printf(__VA_ARGS__)
#else
#define DebugOut(a)
#define DBG_PRINTF(...)
#endif



#endif /* SRC_DEBUG_H_ */
