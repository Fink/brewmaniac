/*
 * AutoMode.cpp
 *
 *  Created on: Aug 5, 2020
 *      Author: fink
 */

#include "AutoMode.h"
#include "BrewLogger.h"
#include "lcd_interface.h"
#include "Settings.h"
#include "Pump.h"
#include "wi.h"
#include "stages.h"
#include "buzz.h"
#include "Temperature.h"
#include "automation.h"
#include "Menu.h"

AutoMode autoMode;

AutoMode::AutoMode():
        _state(AS_Finished),
        _stateBeforePause(AS_Finished),
        _delayTime(0),
        _remainingBoilTime(0),
        _whirlpoolTime(0),
        _savedTime(0),
        _mashingStep(0),
        _numberMashingStep(0),
        _numHopToBeAdded(0),
        _hopStandSessionHopIndex(0),
        _hopStandSession(0),
        _primePumpCount(0),
        _delayRequested (false),
        _stageConfirm(false),
        _coolingTempReached(false),
        _mashingStageExtendEnable(false),
        _mashingStageExtending(false),
        _askingSkipMashingStage(false),
        _recoveryTimer(false),
        _whirlpoolInput(false),
        _pumpRunning(false),
        _savedHeating(false),
        _savedPump(false),
        _isPaused(false),
        _manualPump(false),
        _set_no_delay_start(false),
        _set_pump_prime_on_time(0),
        _set_pump_prime_off_time(0),
        _set_pump_prime_count(0),
        _set_sparge_enabled(false),
        _set_pid_during_doughin(false),
        _set_skip_malt(false),
        _set_pump_premash(false),
        _set_skip_iodine_test(false),
        _set_skip_remove_malt(false),
        _set_pump_on_mash(false),
        _set_pump_on_mashout(false),
        _set_iodinetime(0),
        _set_pid_pipe(false),
        _set_boil_temp(0),
        _set_pump_during_boil(false),
        _set_whirlpool(0)
{
}

AutoMode::~AutoMode()
{
}

void AutoMode::Init()
{
  loadBrewParameters();

  _set_no_delay_start        = static_cast<bool>          (settings.Get(Settings::set_NoDelayStart));
  _set_pump_prime_on_time    = static_cast<std::uint32_t> (settings.Get(Settings::set_PumpPrimeOnTime));
  _set_pump_prime_off_time   = static_cast<std::uint32_t> (settings.Get(Settings::set_PumpPrimeOffTime));
  _set_pump_prime_count      = static_cast<std::uint32_t> (settings.Get(Settings::set_PumpPrimeCount));
  _set_sparge_enabled        = static_cast<bool>          (settings.Get(Settings::set_SpargeWaterEnable));
  _set_pid_during_doughin    = static_cast<bool>          (settings.Get(Settings::set_PID_DoughIn));
  _set_skip_malt             = static_cast<bool>          (settings.Get(Settings::set_SkipAddMalt));
  _set_pump_premash          = static_cast<bool>          (settings.Get(Settings::set_PumpPreMash));
  _set_skip_iodine_test      = static_cast<bool>          (settings.Get(Settings::set_SkipIodineTest));
  _set_skip_remove_malt      = static_cast<bool>          (settings.Get(Settings::set_SkipRemoveMalt));
  _set_pump_on_mash          = static_cast<bool>          (settings.Get(Settings::set_PumpOnMash));
  _set_pump_on_mashout       = static_cast<bool>          (settings.Get(Settings::set_PumpOnMashOut));
  _set_iodinetime            = static_cast<std::uint32_t> (settings.Get(Settings::set_IodineTime));
  _set_pid_pipe              = static_cast<bool>          (settings.Get(Settings::set_PidPipe));
  _set_boil_temp             = static_cast<std::uint32_t> (settings.Get(Settings::set_BoilTemp));
  _set_pump_during_boil      = static_cast<bool>          (settings.Get(Settings::set_PumpOnBoil));
  _set_whirlpool             = static_cast<std::uint32_t> (settings.Get(Settings::set_Whirlpool));

  if (brewLogger.checkRecovery())
  {
    _state = AS_AskResume;
    uiSubTitle(STR(Resume_Process));
    uiButtonLabel(ButtonLabel(Continue_Yes_No));
    return;
  }
  else if (!_set_no_delay_start)
  {
    _state = AS_AskDelayStart;
    _delayTime = 0;
    // output Delay State
    uiTitle(STR(AutomaticMode));
    uiSubTitle(STR(Delay_Start));
    uiButtonLabel(ButtonLabel(No_Yes));
  }
  else
  {
    _state = AS_AskWaterAdded;
    uiTitle(STR(AutomaticMode));
    uiSubTitle(STR(Water_Added));
    uiButtonLabel(ButtonLabel(Continue_Yes_No));
  }
}

bool AutoMode::EventHandler(std::uint8_t event, std::uint8_t mask)
{
  switch (_state)
  {
  case AS_AskResume:
    return Handle_Ask_Resume(event, mask);
  case AS_AskDelayStart:
    return Handle_Ask_DelayStart(event, mask);
  case AS_AskWaterAdded:
    return Handle_Ask_WaterAdded(event, mask);
  case AS_AskSpargeWaterAdded:
    return Handle_Ask_SpargeWaterAdded(event, mask);
  case AS_PumpPrime:
    return Handle_PumpPriming(event, mask);
  case AS_DelayTimeInput:
    return Handle_DelayTimerInput(event, mask);
  case AS_DelayTimeConfirm:
    return Handle_DelayTimerConfirm(event, mask);
  case AS_DelayWaiting:
    return Handle_DelayWaiting(event, mask);
  case AS_DoughIn:
    return Handle_DoughIn(event, mask);
  case AS_Pause:
    return Handle_Pause(event, mask);
  case AS_MashInAskContinue:
    return Handle_Ask_MashInContinue(event, mask);
  case AS_AskAddMalt:
    return Handle_Ask_AddMalt(event, mask);
  case AS_Mashing:
    return Handle_Mashing(event, mask);
  case AS_IodineTest:
    return Handle_IodineTest(event, mask);
  case AS_AskMaltRemove:
    return Handle_Ask_MaltRemove(event, mask);
  case AS_Boiling:
    return Handle_Boiling(event, mask);
  case AS_HopStandChilling:
    return Handle_HopStandChilling(event, mask);
  case AS_HopStand:
    return Handle_HopStand(event, mask);
  case AS_Cooling:
    return Handle_Cooling(event, mask);
  case AS_Whirlpool:
    return Handle_Whirlpool(event, mask);
  case AS_Finished:
    return Handle_Finished(event, mask);
  default:
    return false;
  }
}

bool AutoMode::Handle_Ask_Resume(std::uint8_t event, std::uint8_t mask)
{
  switch (mask)
  {
  case ButtonMaskEnter:
    brewLogger.clearRecovery();
    Init();
    return true;

  case ButtonMaskStart:
    ResumeProcess();
    return true;

  default:
    return false;
  }
}

bool AutoMode::Handle_Ask_DelayStart(std::uint8_t event, std::uint8_t mask)
{
  switch (mask)
  {
  case ButtonMaskEnter:
    _delayRequested = true;
    _state = AS_AskWaterAdded;
    break;

  case ButtonMaskStart:
    _delayRequested = false;
    // next state
    _state = AS_AskWaterAdded;
    break;

  default:
    return false;
  }

  if (_state == AS_AskWaterAdded)
  {
    // ask resume, just ignore this for now
    uiSubTitle(STR(Water_Added));
    uiButtonLabel(ButtonLabel(Continue_Yes_No));
    return true;
  }
  return false;
}

bool AutoMode::Handle_Ask_SpargeWaterAdded(std::uint8_t event, std::uint8_t mask)
{
  switch (mask)
  {
  case ButtonMaskEnter:
    gEnableSpargeWaterHeatingControl = true;
    if (_set_pump_prime_on_time)
    {
      Enter_PumpPriming();
    }
    return true;

  case ButtonMaskStart:
    gEnableSpargeWaterHeatingControl = false;
    if (_set_pump_prime_on_time)
    {
      Enter_PumpPriming();
    }
    return true;

  default:
    return false;
  }
}

bool AutoMode::Handle_PumpPriming(std::uint8_t event, std::uint8_t mask)
{
  (void) mask;

  if (event == TimeoutEventMask)
  {
    if (pump.isOn())
    {
      pump.off();
      timers.SetTimeoutAfter(_set_pump_prime_off_time * 1000);
    }
    else
    {
      _primePumpCount++;

//      DBG_PRINTF("_primePumpCount:%d, PS_PumpPrimeCount:%d, event:%d\n",_primePumpCount,readSetting(PS_PumpPrimeCount), event);
      if (_primePumpCount < _set_pump_prime_count)
      {
        pump.on();
        timers.SetTimeoutAfter(_set_pump_prime_on_time * 1000);
      }
      else
      {
        // next stage is setting delay or mash start
        if (_delayRequested)
        {
          Enter_DelayTimeInput();
        }
        else
        {
          Enter_DoughIn();
        }
      }
    }
    return true;
  }
  return false;
}

bool AutoMode::Handle_Ask_WaterAdded(std::uint8_t event, std::uint8_t mask)
{
  switch (mask)
  {
  case ButtonMaskEnter:
    menu.switchApplication(Menu::MAIN_SCREEN);
    return true;

  case ButtonMaskStart:
    // ask sparge water if sparge water is enable.
    if (_set_sparge_enabled)
    {
      _state = AS_AskSpargeWaterAdded;
      uiSubTitle(STR(SpargeWater_Added));
      uiButtonLabel(ButtonLabel(No_Yes));
    }
    else
    {
      gEnableSpargeWaterHeatingControl = false;
      Enter_PumpPriming();
    }
    return true;

  default:
    return false;
  }
}

bool AutoMode::Handle_DelayTimerInput(std::uint8_t event, std::uint8_t mask)
{
  // input delay timer
  if (event != ButtonPressedEventMask)
    return false;

  switch (mask)
  {
  case ButtonMaskEnter:
    _state = AS_DelayTimeConfirm;
    uiButtonLabel(ButtonLabel(Continue_Yes_No));
    break;

  case ButtonMaskStart:
    backToMain();
    break;

  case ButtonMaskUp:
    if ((_delayTime + 1) < _max_delay_time)
    {
      _delayTime++;
      uiRunningTimeShowInitial(_delayTime * 15 * 60);
    }
    break;

  case ButtonMaskDown:
    if (_delayTime > 1)
    {
      _delayTime--;
      uiRunningTimeShowInitial(_delayTime * 15 * 60);
    }
    break;

  default:
    return false;
  }

  return true;
}

bool AutoMode::Handle_DelayTimerConfirm(std::uint8_t event, std::uint8_t mask)
{
  if (event != ButtonPressedEventMask)
    return false;

  switch (mask)
  {
  case ButtonMaskEnter:
    backToMain();
    return true;

  case ButtonMaskStart:
    _state = AS_DelayWaiting;
    uiClearSubTitleRow();
    uiSubTitle(STR(To_be_started_in));
    uiButtonLabel(ButtonLabel(x_x_Quit_Go));

    timers.SetTimeoutAfter(_delayTime * 15 * 60 * 1000);
    uiRunningTimeStartCountDown(_delayTime * 15 * 60);
    SetEventMask(TimeoutEventMask | ButtonPressedEventMask);

    wiReportCurrentStage(StageDelayStart);
    return true;

  default:
    return false;
  }
}

bool AutoMode::Handle_DelayWaiting(std::uint8_t event, std::uint8_t mask)
{
  if (event == ButtonPressedEventMask)
  {
    switch (mask)
    {
    case ButtonMaskEnter:
      uiRunningTimeStop();
      timers.PauseTimer();
      uiClearSettingRow();
      Enter_DoughIn();
      return true;

    case ButtonMaskStart:
      uiRunningTimeStop();
      timers.PauseTimer();
      backToMain();
      return true;

    case ButtonMaskUp:
      break;

    case ButtonMaskDown:
      break;

    default:
      return false;
    }
  }
  else if (event == TimeoutEventMask)
  {
    buzzPlaySound(SoundIdDelayTimeout);
    uiRunningTimeStop();
    uiClearSettingRow();
    Enter_DoughIn();
    return true;
  }
  return false;
}

bool AutoMode::Handle_DoughIn(std::uint8_t event, std::uint8_t mask)
{
  if (event == TemperatureEventMask)
  {
    if (temperature.temperatures[Temperature::TEMP_BREW] >= gSettingTemperature)
    {
      // temp reached. ask continue & malt in
      _state = AS_MashInAskContinue;
      gIsTemperatureReached = true;

      uiPrompt(STR(TemperatureReached));
      //{ADD_MALT_MOD
#if 1
      uiButtonLabel(ButtonLabel(Continue_Yes_No));
#else
      uiButtonLabel(ButtonLabel(Continue_Yes_x));
      #endif
      //}ADD_MALT_MOD

      SetEventMask(ButtonPressedEventMask);

      buzzPlaySoundRepeat(SoundIdWaitUserInteraction);

      brewLogger.event(RemoteEventTemperatureReached);
      wiReportEvent(RemoteEventTemperatureReached);
      return true;
    }
  }     //TemperatureEventMask
  else if (event == ButtonPressedEventMask)
  {
    switch (mask)
    {
    case ButtonMaskEnter:
      pump.toggle();
      break;

    case ButtonMaskStart:
      Pause(0);
      break;

    case ButtonMaskUp:
    case ButtonMaskDown:
    case ButtonMaskContUp:
    case ButtonMaskContDown:
      return processAdjustButtons(static_cast<e_button_mask>(mask));

    default:
      return false;
    }
  }
  return false;
}

bool AutoMode::Handle_Pause(std::uint8_t event, std::uint8_t mask)
{
  if (event == ButtonPressedEventMask)
  {
    switch (mask)
    {
    case ButtonMaskEnter:
      BrewEnd();
      autoMode.Init();
      return true;

    case ButtonMaskStart:
      ExitPause();
      return true;

    case ButtonMaskUp:
      break;

    case ButtonMaskDown:
      break;

    default:
      return false;
    }
  }
  return false;
}

bool AutoMode::Handle_Ask_MashInContinue(std::uint8_t event, std::uint8_t mask)
{
  switch (mask)
  {
  case ButtonMaskEnter:
    // heater & pump might started, so use back to main
    backToMain();
    return true;

  case ButtonMaskStart:
    buzzMute();
    if (_set_pid_during_doughin)
    {
    }
    else
    {
      heatOff();  // turn off heat. during "dough-in"
    }
    // goto next stage, Mashing or ask MaltADD
    if (_set_skip_malt)
    {
      uiClearPrompt();
      // skip Add Malt , enter mashing state
      Enter_Mashing();
    }
    else
    {
      pump.off();
      uiPrompt(STR(Add_Malt));
      uiButtonLabel(ButtonLabel(Continue_Yes_Pmp));

      _state = AS_AskAddMalt;

      wiReportEvent(RemoteEventAddMalt);

    }
    return true;

  default:
    return false;
  }
}

bool AutoMode::Handle_Ask_AddMalt(std::uint8_t event, std::uint8_t mask)
{
  switch (mask)
  {
  case ButtonMaskEnter:
    pump.toggle();
    return true;

  case ButtonMaskStart:
    uiClearPrompt()
    ;
    Enter_Mashing();
    return true;

  default:
    return false;
  }
}

bool AutoMode::Handle_Mashing(std::uint8_t event, std::uint8_t mask)
{
  // handle key event together.
  // the same way reached or not.
  if (event == ButtonPressedEventMask)
  {
    if (_askingSkipMashingStage)
    {
      switch (mask)
      {
      case ButtonMaskEnter:
        uiClearPrompt()
        ;
        uiButtonLabel(ButtonLabel(Up_Down_PmPus_STP));
        // unwind the change
        uiRunningTimeHide(false);
        _askingSkipMashingStage = false;
        break;

      case ButtonMaskStart:
        // undone _askingSkipMashingStage
        uiClearPrompt()
        ;
        // not necessary , autoModeMashingStageFinished()
        // will print eht menu againuiButtonLabel(ButtonLabel(Up_Down_Pause_STP));
        // unwind the change
        uiRunningTimeHide(false);
        _askingSkipMashingStage = false;
        timers.PauseTimer();  // cancel timer, if any
        // go to next stage
        MashingStageFinished();
        break;

      case ButtonMaskUp:
        ToggleMashExtension();
        break;

      default:
        break;
      }

      return true;
    }
    else
    {
      switch (mask)
      {
      case ButtonMaskLongEnter:
        if (gIsTemperatureReached)
          buzzMute();

        uiRunningTimeHide(true);
        _askingSkipMashingStage = true;
        //uiClearPrompt();
        uiPrompt(STR(Skip_Or_Extend));
        uiButtonLabel(ButtonLabel(Extend_Skip_Back));
        break;

      case ButtonMaskEnter:
        if (_mashingStageExtending)
        {
          // go to next step.
          MashingStageFinished();
        }
        break;

      case ButtonMaskLongStart:
        if (gIsTemperatureReached)
        {
          buzzMute();
          Pause(timers.PauseTimer());
        }
        else
        {
          Pause(0);
        }
        break;

      case ButtonMaskStart:
        pump.toggle();
        _manualPump = true;
        break;

      default:
        if (_askingSkipMashingStage)
        {
          return false;
        }
        return processAdjustButtons(static_cast<e_button_mask>(mask));
      }
      return true;
    }
  }
  else if (event == PumpRestEventMask)
  {
    togglePumpRest();
    return true;
  }
  else
  {
    // else of PumpRestEvent & Button,
//    DBG_PRINTF("reach:%d, setting:%d, event:%d\n",gIsTemperatureReached,(int)gSettingTemperature, event);
    if (gIsTemperatureReached)
    {
      if (event == TimeoutEventMask)
      {
        // counting time
        // except button, we care also two timer
        // one for 10  or 5 seconds before time out
        // the other for end of phase timeout
        if (mask == Timers::TIMER_AUX)
        {
          buzzPlaySound(SoundIdCountDown);
        }
        else
        {
          // next stage
          if (_askingSkipMashingStage)
          {
            uiClearPrompt();
            uiRunningTimeHide(false);
            _askingSkipMashingStage = false;
          }

          if (_mashingStageExtendEnable)
          {
            Enter_MashingExtension();
          }
          else
          {
            MashingStageFinished();
          }
        }
        return true;
      }  // end of event == TimeoutEventMask
    }
    else
    {
      // else of if(gIsTemperatureReached)
      if (event == TemperatureEventMask)
      {
        // rising temperature
        if (temperature.temperatures[Temperature::TEMP_BREW] >= gSettingTemperature)
        {
          brewLogger.event(RemoteEventTemperatureReached);

          gIsTemperatureReached = true;
          std::uint32_t seconds = (std::uint32_t) automation.stageTime(_mashingStep) * 60;

          timers.SetTimeoutAfter(seconds * 1000);
          timers.SetAuxTimeoutAfter((seconds - ADVANCE_BEEP_TIME) * 1000);
          SetEventMask(TemperatureEventMask | ButtonPressedEventMask | TimeoutEventMask | PumpRestEventMask);

          uiRunningTimeStartCountDown(seconds);

          buzzPlaySound(SoundIdTemperatureReached);

          pump.setRestEnabled(true);

          wiReportEvent(RemoteEventTemperatureReached);
          return true;
        }
      }
    }   // end of else if(gIsTemperatureReached)
  }   // end of temperature and timeout handling
  return false;
}   //AS_Mashing

bool AutoMode::Handle_IodineTest(std::uint8_t event, std::uint8_t mask)
{
  // timeout or user press ok
  if (event == ButtonPressedEventMask)
  {
    switch (mask)
    {
    case ButtonMaskEnter:
      IodineTestToMashExtension();
      return true;

    case ButtonMaskStart:
      uiClearPrompt();
      // back to next mashing step: Mashout
      IodineTestToMashout();
      return true;

    default:
       return false;
    }
  }
  else if (event == TimeoutEventMask)
  {
    uiClearPrompt();
    //[TODO:] make sure not other timeout event
    IodineTestToMashout();
    return true;
  }
  return false;
}   //AS_IodineTest

bool AutoMode::Handle_Ask_MaltRemove(std::uint8_t event, std::uint8_t mask)
{
  if (event == ButtonPressedEventMask)
  {
    switch (mask)
    {
    case ButtonMaskEnter:
      backToMain();
      return true;

    case ButtonMaskStart:
      buzzMute();
      // yes
      uiClearPrompt();
      Enter_Boiling();
      return true;

    default:
       return false;
    }
  }
  return false;
}

bool AutoMode::Handle_Boiling(std::uint8_t event, std::uint8_t mask)
{
  if (event == ButtonPressedEventMask)
  {
    switch (mask)
    {
    case ButtonMaskEnter:
      pump.toggle();
      break;

    case ButtonMaskStart:
      if (gIsTemperatureReached)
      {
        BoilingPauseHandler();
      }
      break;

    default:
       return processAdjustButtons(static_cast<e_button_mask>(mask));
    }

    return true;
  }
  else if (event == TimeoutEventMask)
  {
    if (mask == Timers::TIMER_AUX)
    {
      // start next timer to end notice of hop adding
      if (_recoveryTimer)
      {
        uiAutoModeStage(BoilingStage);
        StartNextHopTimer();
      }
      else
      {
        // start next timer
        AddHopNotice();
      }
    }
    else
    {
      //boiling timer timeout
      // next stage
      heatOff();  // heat OFF
      // switch to post boil for next starts.
      pump.off();

      brewLogger.event(RemoteEventBoilFinished);
      wiReportEvent(RemoteEventBoilFinished);

      buzzPlaySoundRepeat(SoundIdWaitUserInteraction);

      if (automation.numberOfHopStandSession() == 0)
        CoolingOrWhirlpool();
      else StartHopStand();
    }
    return true;
  }
  else
  {
    bool ret = false;
    if (temperature.temperatures[Temperature::TEMP_BREW] >= gBoilStageTemperature)
    {
      if (gIsTemperatureReached == false)
      {
        brewLogger.event(RemoteEventTemperatureReached);
        gIsTemperatureReached = true;

        //buzz temperature reach first
        // because later "add hop" buzz may interrupt
        // it
        wiReportEvent(RemoteEventTemperatureReached);
        buzzPlaySound(SoundIdBoil);
        // start counting down
        std::uint8_t boilTime = automation.boilTime();
        uiRunningTimeStartCountDown((std::uint32_t) boilTime * 60);
        // start hop & boiling out timer
        StartBoilingTimer();

        uiButtonLabel(ButtonLabel(Up_Down_Pause_Pmp));
        ret = true;
      }
    }
    bool toggled = togglePwmInput();
    return ret || toggled;
  }
  return false;
}  //AS_BoilingautoModeEventHandler

bool AutoMode::Handle_HopStandChilling(std::uint8_t event, std::uint8_t mask)
{
  if (event == ButtonPressedEventMask)
  {
    if (!_stageConfirm)
    {
      if (mask == ButtonMaskStart)
      {
        _stageConfirm = true;
        buzzMute();
        uiButtonLabel(ButtonLabel(Up_Down_Skip_Pmp));
        return true;
      }
      return false;
    }

    switch (mask)
    {
    case ButtonMaskEnter:
      pump.toggle();
      break;

    case ButtonMaskLongStart:
      _state = AS_HopStand;
      brewLogger.stage(StageHopStand);
      Enter_HopStand();
      break;

    default:
       return processAdjustButtons(static_cast<e_button_mask>(mask));
    }

    return true;
  }
  else if (event == TemperatureEventMask)
  {
    if (temperature.temperatures[Temperature::TEMP_BREW] <= gSettingTemperature)
    {
      _state = AS_HopStand;
      brewLogger.stage(StageHopStand);
      Enter_HopStand();
      return true;
    }
  }
  return false;
}  //AS_HopStandChilling

bool AutoMode::Handle_HopStand(std::uint8_t event, std::uint8_t mask)
{
  if (event == ButtonPressedEventMask)
  {
    switch (mask)
    {
    case ButtonMaskEnter:
      pump.toggle();
      break;

    case ButtonMaskLongStart:
      EndHopStandSession();
      break;

    default:
       return processAdjustButtons(static_cast<e_button_mask>(mask));
    }

    return true;
  }
  else if (event == TimeoutEventMask)
  {
    if (mask == Timers::TIMER_AUX)
    {
      AuxTimeout();
    }
    else
    {
      HopStandTimeout();
    }
    return true;
  }
  return false;
}  //AS_HopStand

bool AutoMode::Handle_Cooling(std::uint8_t event, std::uint8_t mask)
{
  if (_stageConfirm)
  {
    if (event == ButtonPressedEventMask)
    {
      switch (mask)
      {
      case ButtonMaskEnter:
        pump.toggle();
        break;

      case ButtonMaskStart:
        CoolingFinish();
        break;

      default:
         return processAdjustButtons(static_cast<e_button_mask>(mask));
      }

      return true;
    }
    else if (event == TemperatureEventMask)
    {
      // if temperature drop to desire temp
      // end this phase
      if (temperature.temperatures[Temperature::TEMP_BREW] <= gSettingTemperature)
      {
        // next stage
        //Manual END. no Auto end
        // autoModeCoolingFinish();
        if (!_coolingTempReached)
        {
          buzzPlaySound(SoundIdTemperatureReached);
          wiReportEvent(RemoteEventTemperatureReached);
          _coolingTempReached = true;
          return true;
        }
      }
    }
  }
  else
  {
    // of if(_stageConfirm), in state of asking Enter Cooling
    // wait confirm
    if (event != ButtonPressedEventMask)
      return false;

    buzzMute();
    switch (mask)
    {
    case ButtonMaskEnter:
      CoolingFinish();
      return true;

    case ButtonMaskStart:
      brewLogger.stage(StageCooling);
      // yes
      _stageConfirm = true;
      Enter_Cooling(0);
      brewLogger.setPoint(gSettingTemperature);
      return true;

    default:
       return false;
    }
  }
  return false;
}

bool AutoMode::Handle_Whirlpool(std::uint8_t event, std::uint8_t mask)
{
  if (_stageConfirm)
  {
    if (_whirlpoolInput)
    {
      // input screen of Whirlpool time
      if (event != ButtonPressedEventMask)
        return false;

      switch (mask)
      {
      case ButtonMaskEnter:
        _whirlpoolInput = false;
        Whirlpool(0);
        break;

      case ButtonMaskStart:
        WhirlpoolFinish();
        break;

      case ButtonMaskUp:
        if ((_whirlpoolTime + 1) <= whirlpool_time_max)
        {
          _whirlpoolTime++;
          uiRunningTimeShowInitial(_whirlpoolTime * 60);
        }
        break;

      case ButtonMaskDown:
        if ((_whirlpoolTime - 1) >= whirlpool_time_min)
        {
          _whirlpoolTime--;
          uiRunningTimeShowInitial(_whirlpoolTime * 60);
        }
        break;

      default:
         return false;
      }

      return true;
    }
    else
    {
      // of _whirlpoolInput
      // Whirlpool stage running
      // counting time & running pump
      if (event == ButtonPressedEventMask)
      {
        switch (mask)
        {
        case ButtonMaskEnter:
          if (_pumpRunning)
          {
            // stop pump ,and stop & reset time
            _pumpRunning = false;
            pump.off();
            uiRunningTimeShowInitial(_whirlpoolTime * 60);
            timers.PauseTimer();

            uiButtonLabel(ButtonLabel(x_x_Time_Pmp));
          }
          else
          {
            _pumpRunning = true;
            pump.on();
            uiRunningTimeStartCountDown(_whirlpoolTime * 60);
            timers.SetTimeoutAfter((std::uint32_t) _whirlpoolTime * 60 * 1000);

            uiButtonLabel(ButtonLabel(x_x_x_Pmp));

          }
          break;

        case ButtonMaskStart:
          if (!_pumpRunning)
          {
            // time, back to time setting
            WhirlpoolInputTime();
          }
          break;

        default:
           return false;
        }

        return true;
      }
      else if (event == TemperatureEventMask)
      {
        //[TODO:] temperature control
      }
      else if (event == TimeoutEventMask)
      {
        WhirlpoolFinish();
        return true;
      }
    }
  }
  else
  {  // if(_stageConfirm)
     // wait confirm
    if (event != ButtonPressedEventMask)
      return false;

    buzzMute();

    switch (mask)
    {
    case ButtonMaskEnter:
      WhirlpoolFinish();
      break;

    case ButtonMaskStart:
      brewLogger.stage(StageWhirlpool);
      // yes
      _stageConfirm = true;
      WhirlpoolInputTime();
      break;

    default:
       return false;
    }

    return true;
  }
  return false;
}

bool AutoMode::Handle_Finished(std::uint8_t event, std::uint8_t mask)
{
  (void) mask;
  if (event == TimeoutEventMask)
  {
    backToMain();
    return true;
  }
  return false;
}

void AutoMode::ResumeProcess(void)
{
  // get stage
  std::uint8_t stage;
  std::uint32_t elapsed;
  brewLogger.resumeSession(&stage, &elapsed);
//  DBG_PRINTF("resume state:%d, elapsed:%d\n", stage, elapsed);

  SetEventMask(TemperatureEventMask | ButtonPressedEventMask | TimeoutEventMask | PumpRestEventMask);

  uiClearScreen();

  uiAutoModeTitle();
  //uiSetSettingTemperature(gSettingTemperature); will be set later in each entering procedure
  uiTempDisplaySetPosition(TemperatureAutoModePosition);
  uiRunningTimeSetPosition(RunningTimeNormalPosition);
  // time may be 0xFF, invalid, => not just enter
  // less then stage time. temperature reached
  //
  loadBrewParameters();

  if (stage == StageBoil)  // boiling
  {
    Enter_Boiling();
    // if 0xfFF, assume not
    if (elapsed != INVALID_RECOVERY_TIME)
    {
      std::uint8_t boilTime = automation.boilTime();
      std::uint8_t time = boilTime - (std::uint8_t) elapsed;

      // findout whihc hop is current

      gIsTemperatureReached = true;
      std::uint32_t sec = (std::uint32_t) time * 60;
      uiRunningTimeStartCountDown(sec);
      timers.SetTimeoutAfter(sec * 1000);

      // start hop & boiling out timer
      std::uint8_t hopnum = automation.numberOfHops();
      _numHopToBeAdded = hopnum;
      if (hopnum > 0)
      {
        std::uint8_t i;
        std::uint8_t nextHopTime = 0;
        for (i = 0; i < hopnum; i++)
        {
          nextHopTime = automation.timeOfHop(i);
          if (nextHopTime > time)
            _numHopToBeAdded--;
          else break;
        }
        if (_numHopToBeAdded > 0)
        {
          std::uint32_t hopTimer = (std::uint32_t) (time - nextHopTime);
          _recoveryTimer = false;
          timers.SetAuxTimeoutAfter((std::uint32_t) hopTimer * 60 * 1000);
        }
      }
    }
  }
  else if (stage == StageCooling)  // cooling
  {
    heatOff();
    if (elapsed != INVALID_RECOVERY_TIME)
      Enter_Cooling((std::uint32_t) elapsed * 60);
    else Enter_Cooling(0);

    _state = AS_Cooling;
    _stageConfirm = (elapsed != 0);
  }
  else if (stage == StageWhirlpool)  // Whirlpool
  {
    heatOff();
    if (elapsed != INVALID_RECOVERY_TIME)
      Whirlpool(elapsed);
    else Whirlpool(0);
    _state = AS_Whirlpool;
  }
  else if (stage == StageHopStandChill)  // HopStandChill
  {
    // need to 'recover' which session it is
    _hopStandSessionHopIndex = 0;
    if (elapsed == INVALID_RECOVERY_TIME)
    {
      _hopStandSession = 0;
    }
    else
    {
      _hopStandSession = HopStandSessionByTime(elapsed);
    }
    _state = AS_HopStandChilling;
    Enter_HopStandChilling();
  }
  else if (stage == StageHopStand)  // StageHopStand
  {
    _hopStandSession = HopStandSessionByTime(elapsed);

    int elapsedPre = 0;

    for (int i = 0; i < _hopStandSession; i++)
    {
      elapsedPre += automation.hopInSession(i, 0);
    }
//    DBG_PRINTF("Resume: elapsed:%d, elaspsedPre:%d\n", elapsed, elapsedPre);
    _state = AS_HopStand;
    Enter_HopStand(elapsed - elapsedPre);
  }
  else if (stage == StageDoughIn)  // Daugh-in
  {
    heatOn();
    Enter_DoughIn();
  }
  else if (stage < StageBoil)
  {
    // everything else is in MASHING state
    // just enter mashing step ...
    heatOn();
    _state = AS_Mashing;
    _askingSkipMashingStage = false;

    Update_numberMashingStep();

    _mashingStep = stage - 1;  // next step will increase the step
    NextMashingStep(true);

    // adjust timer if necessary
    if (elapsed != INVALID_RECOVERY_TIME)
    {
      std::uint8_t stagetime = automation.stageTime(_mashingStep);
      std::uint8_t time = stagetime - elapsed;
      gIsTemperatureReached = true;
      std::uint32_t seconds = (std::uint32_t) time * 60;

      timers.SetTimeoutAfter(seconds * 1000);
      timers.SetAuxTimeoutAfter((seconds - ADVANCE_BEEP_TIME) * 1000);
      uiRunningTimeStartCountDown(seconds);
      pump.setRestEnabled(true);

      //wiReportEvent(RemoteEventTemperatureReached);
    }

  }
}

void AutoMode::Enter_PumpPriming(void)
{
  _state = AS_PumpPrime;
  // request on off timer
  SetEventMask(TimeoutEventMask /*| ButtonPressedEventMask */);

  //[TODO:] pause or stop the action?
  uiNoMenu();
  uiSubTitle(STR(Pump_Prime));
  // start pump & timer
  pump.on();
  _primePumpCount = 0;
  timers.SetTimeoutAfter(_set_pump_prime_on_time);  // 1sec
}

void AutoMode::Enter_DelayTimeInput(void)
{
  _state = AS_DelayTimeInput;
  uiClearSubTitleRow();
  uiSubTitle(STR(Setting_Delay));
  uiButtonLabel(ButtonLabel(Up_Down_Quit_Ok));
  // use display time counting
  uiRunningTimeSetPosition(RunningTimeDelayInputPosition);
  uiRunningTimeShowInitial(15 * 60);
  _delayTime = 1;  // one unit is 15min
}

void AutoMode::Enter_DoughIn(void)
{
  _state = AS_DoughIn;
  // setup temperature event mask request after this.
  SetEventMask(TemperatureEventMask /*| ButtonPressedEventMask */);

  //load temperature value
  float doughinTemp = automation.stageTemperature(0);

  // setup screen
  uiClearTitle();
  uiAutoModeTitle();
  uiClearSubTitleRow();

  uiAutoModeStage(0);  // 0 is Mash-In

  // displace temperature
  uiTempDisplaySetPosition(TemperatureAutoModePosition);

  setSettingTemperature(doughinTemp);

  uiButtonLabel(ButtonLabel(Up_Down_Pause_Pmp));
  // start pump, if request,

  if (_set_pump_premash)
    pump.on();
  else pump.off();

  if (gEnableSpargeWaterHeatingControl)
  {
    startHeatingSpargeWater();
  }

  // start heat
  heatOn();
  setAdjustTemperature(75.0, 25.0);
  gIsEnterPwm = false;

  //

  brewLogger.startSession(Temperature::TEMP_COUNT, TemperatureChartPeriod, false);

  brewLogger.stage(StageDoughIn);

//  brewLogger.temperatures(temperature.temperatures);

  wiReportCurrentStage(StageDoughIn);
}

void AutoMode::Pause(std::uint32_t time)
{
  _stateBeforePause = _state;
  _state = AS_Pause;
  _isPaused = true;
  // stop Heating & pump
  _savedHeating = gIsHeatOn;
  _savedPump = pump.isOn();
  heatOff();
  pump.off();
  // just wait for user button

  uiPreparePasueScreen(STR(In_Pause));
  uiButtonLabel(ButtonLabel(x_x_Exit_End));

  _savedTime = time;

  uiRunningTimeShowInitial(_savedTime / 1000);
  brewLogger.event(RemoteEventPause);
  wiReportEvent(RemoteEventPause);
}

void AutoMode::ExitPause(void)
{
  // restore state
  _state = _stateBeforePause;
  _isPaused = false;
  // restore timer, if any
  if (_savedTime > 0)
  {
    timers.SetTimeoutAfter(_savedTime);
    if (_savedTime > ADVANCE_BEEP_TIME * 1000)
      timers.SetAuxTimeoutAfter(_savedTime - ADVANCE_BEEP_TIME * 1000);
  }
  // restore screen
  uiClearScreen();

  uiAutoModeTitle();

  if (_mashingStep > 0 && _mashingStep < MashStepMashOut /*7*/)
    uiAutoModeMashTitle(_mashingStep, _numberMashingStep);
  else uiAutoModeStage(_mashingStep);

  // temperateure position
  uiTempDisplaySetPosition(TemperatureAutoModePosition);
  // set temperature point
  uiSetSettingTemperature(gSettingTemperature);

  // counting time
  uiRunningTimeSetPosition(RunningTimeNormalPosition);

  if (_savedTime == 0)
  {
    if (_state != AS_DoughIn)
    {
      std::uint8_t time = automation.stageTime(_mashingStep);
      uiRunningTimeShowInitial(time * 60);
    }
  }
  else  //if(_savedTime==0)
  {
    // temperature reached, timer already started
    // this should always NOT be MashIn
    if (_state == AS_Mashing)
    {
      uiRunningTimeShowInitial(_savedTime / 1000);
      uiRunningTimeStartCountDown(_savedTime / 1000);
    }
  }

  // menu is different for mashin & mashing
  if (_state == AS_DoughIn)
    uiButtonLabel(ButtonLabel(Up_Down_Pause_Pmp));
  else uiButtonLabel(ButtonLabel(Up_Down_PmPus_STP));
  // restore heating and pump
  if (_savedHeating)
    heatOn();
  if (_savedPump)
    pump.on();

  brewLogger.event(RemoteEventResume);
  wiReportEvent(RemoteEventResume);
}

void AutoMode::MashingStageFinished(void)
{
  //[TODO:] make sure step 6 , beta 2 is non-skippable.
  if (_mashingStep < MashStepMashOut /*7*/)  // step 7 = mashout
  {
//    DBG_PRINTF("autoModeMashingStageFinished:%d @%d iodine skip%d, extended:%d\n", _mashingStep, _numberMashingStep,
//               readSetting(PS_SkipIodineTest), _mashingStageExtending);
    if ((_mashingStep == _numberMashingStep) && (_set_skip_iodine_test == false) && !_mashingStageExtending)
    {
      // before MashOut(7) and not Skip Iodine Test
//      DBG_PRINTF("autoModeMashingStageFinished autoModeEnterIodineTest\n");
      Enter_IodineTest();
    }
    else
    {
      NextMashingStep(false);
    }
  }
  else
  {
    // change to boiling stage, or malt out waiting state
    if (_set_skip_remove_malt)
      Enter_Boiling();
    else Enter_AskRemoveMalt();
  }
}

void AutoMode::CoolingAsk(const char *msg, std::uint8_t stage)
{
  _stageConfirm = false;

  //dismiss Temperature & running time
  uiTempDisplayHide();
  uiRunningTimeStop();

  uiClearScreen();

  uiSubTitle(msg);

  uiButtonLabel(ButtonLabel(Continue_Yes_No));

  wiReportCurrentStage(stage);
}

void AutoMode::Enter_Cooling(std::uint32_t elapsed)
{
  // state setting and brewlogger is handled outside.
  // this function is used after "confirmation"
  uiClearPrompt();
  uiClearSubTitleRow();

  uiAutoModeTitle();
  uiAutoModeStage(CoolingStage);
  float temp = 20.0;
  _coolingTempReached = false;
  // temperature at automode
  uiTempDisplaySetPosition(TemperatureAutoModePosition);

  setSettingTemperature(temp);

  uiRunningTimeShowInitial(elapsed);
  uiRunningTimeStartFrom(elapsed);

  uiButtonLabel(ButtonLabel(Up_Down_END_Pmp));

  setAdjustTemperature(30, 10);

  gIsEnterPwm = false;

}

void AutoMode::BrewEnd(void)
{
  pump.off();

  uiTempDisplayHide();
  uiRunningTimeStop();

  _state = AS_Finished;

  uiClearScreen();

  uiAutoModeFinishScreen();

  buzzPlaySoundRepeat(SoundIdBrewEnd);
  timers.SetTimeoutAfter(BREW_END_STAY_DURATION * 1000);

  brewLogger.endSession();

  wiReportEvent(RemoteEventBrewFinished);
}

void AutoMode::ToggleMashExtension(void)
{
  _mashingStageExtendEnable = !_mashingStageExtendEnable;
  uiSetMashExtensionStatus(_mashingStageExtendEnable ? MashExtensionEnabled : MashExtensionNone);
}

void AutoMode::ResetMashExtension(void)
{
  _mashingStageExtendEnable = false;
  _mashingStageExtending = false;
  uiSetMashExtensionStatus(MashExtensionNone);
}

void AutoMode::Enter_MashingExtension(void)
{
  uiSetMashExtensionStatus(MashExtensionRunning);
  _mashingStageExtending = true;
  uiRunningTimeStart();
}

void AutoMode::NextMashingStep(bool resume)
{

  // in autoModeEnterMashing, this value is set to zero
  // so it really starts from 1.
  _mashingStep++;

  std::uint8_t time;

  if (_mashingStep > _numberMashingStep)
  {
    // go direct to mashout
    _mashingStep = MashStepMashOut;  //7;
  }
  time = automation.stageTime(_mashingStep);
  if (time == 0)
    time = 1;

  //  if(_mashingStep > 7), mashout time will always more than 1

  if (_mashingStep > 0 && _mashingStep < MashStepMashOut /*7*/)
    uiAutoModeMashTitle(_mashingStep, _numberMashingStep);
  else uiAutoModeStage(_mashingStep);

  uiRunningTimeSetPosition(RunningTimeNormalPosition);
  uiRunningTimeShowInitial(time * 60);

  setAdjustTemperature(80.0, 20.0);

  setSettingTemperature(automation.stageTemperature(_mashingStep));

  uiButtonLabel(ButtonLabel(Up_Down_PmPus_STP));
  gIsTemperatureReached = false;

  pump.setRestEnabled(false);
  heatOn();

  if (!_manualPump)
  {
    if (_mashingStep <= 6)
    {
      // pump is off at the time AddMalt
      if (_set_pump_on_mash)
        pump.on();
      else pump.off();

    }
    else if (_mashingStep == MashStepMashOut /*7*/)
    {
      if (_set_pump_on_mashout)
        pump.on();
      else pump.off();
    }

  }

  if (!resume)
  {
    brewLogger.stage(_mashingStep);
  }

  ResetMashExtension();
  wiReportCurrentStage(_mashingStep);

}

void AutoMode::Update_numberMashingStep(void)
{
  std::uint8_t idx = 1;
  std::uint8_t time;
  while (idx < MashStepMashOut /*7*/&& (time = automation.stageTime(idx)) != 0)
  {
    idx++;
  }
  _numberMashingStep = idx - 1;  // total mash steps.Update_numberMashingStep
}

void AutoMode::Enter_Mashing(void)
{
  _state = AS_Mashing;
  SetEventMask(TemperatureEventMask | ButtonPressedEventMask | PumpRestEventMask);

  _askingSkipMashingStage = false;
  _mashingStep = 0;  // 0 is mash in , real mashing starts from 1, this number will be increased in
  // NextMashingStep() later.

  _manualPump = false;
  heatOn();

  Update_numberMashingStep();
  NextMashingStep(false);
}

void AutoMode::Enter_IodineTest(void)
{
  _state = AS_IodineTest;

  uiPreparePasueScreen(STR(IODINE_TEST));
  uiButtonLabel(ButtonLabel(x_x_Mashout_Extend));

  if (_set_iodinetime)
  {
    // timer, else wait until user input
    // dont' change event, just ignore the temperature event
    uiRunningTimeShowInitial(_set_iodinetime * 60);
    // [IMPORTANT!] cast is needed
    timers.SetTimeoutAfter((std::uint32_t) _set_iodinetime * 60 * 1000);
    buzzPlaySound(SoundIdIodineTest);
    uiRunningTimeStartCountDown(_set_iodinetime * 60);
  }
  else
  {
    uiRunningTimeShowInitial(0);
    buzzPlaySoundRepeat(SoundIdUserInteractiveNeeded);
  }

  wiReportEvent(RemoteEventIodineTest);

}

void AutoMode::IodineTestToMashExtension(void)
{
  // back to last mashing step.
  _state = AS_Mashing;
  buzzMute();
  timers.PauseTimer();
  uiClearScreen();

  uiAutoModeTitle();
  pump.updateUI();

  uiAutoModeMashTitle(_mashingStep, _numberMashingStep);
  uiTempDisplaySetPosition(TemperatureAutoModePosition);
  uiRunningTimeSetPosition(RunningTimeNormalPosition);
  uiRunningTimeShowInitial(0);
  setAdjustTemperature(80.0, 20.0);

  setSettingTemperature(automation.stageTemperature(_mashingStep));

  uiButtonLabel(ButtonLabel(Up_Down_PmPus_STP));
  Enter_MashingExtension();
  _mashingStageExtendEnable = false;
}

void AutoMode::IodineTestToMashout(void)
{
  timers.PauseTimer();
  uiRunningTimeStop();
  buzzMute();
  // restore Screen
  uiClearScreen();

  uiAutoModeTitle();
  pump.updateUI();
  // temperateure position
  uiTempDisplaySetPosition(TemperatureAutoModePosition);
  //phase name, setting point, and counting time will be shown
  // in autoModeNextMashingStep()
  // restore Mashing
  _state = AS_Mashing;
  NextMashingStep(false);

}

void AutoMode::Enter_AskRemoveMalt(void)
{
  _state = AS_AskMaltRemove;
  pump.off();

  uiRunningTimeStop();
  //uiClearPrompt();
  uiPrompt(STR(Remove_Malt));
  uiButtonLabel(ButtonLabel(Continue_Yes_No));
  // skip event mask, just filter it out in handling code

  buzzPlaySoundRepeat(SoundIdWaitUserInteraction);

//  if(!(readSetting(PS_PidPipe) == 1 && readSetting(PS_SensorType) == SensorInside))

  if (!_set_pid_pipe)
    heatProgramOff();  // heat off, programming

  wiReportEvent(RemoteEventRemoveMalt);
}

void AutoMode::Enter_Boiling(void)
{
  if (gEnableSpargeWaterHeatingControl)
  {
    stopHeatingSpargeWater();
  }

  _state = AS_Boiling;
  gIsTemperatureReached = false;
  _isPaused = false;
  gBoilStageTemperature = _set_boil_temp;
  //gSettingTemperature =110;//
  setSettingTemperature(gBoilStageTemperature);

  // display time
  std::uint8_t boilTime = automation.boilTime();

  brewLogger.stage(StageBoil);

  uiRunningTimeShowInitial(boilTime * 60);

  uiAutoModeStage(BoilingStage);
  uiButtonLabel(ButtonLabel(Up_Down_x_Pmp));

  if (_set_pump_during_boil)
    pump.on();
  else pump.off();

  setAdjustTemperature(110.0, 80.0);

  gIsEnterPwm = false;
  heatOn(false);  // NO need of PID, just full power until boiling
  wiReportCurrentStage(StageBoil);
}

void AutoMode::ShowHopAdding(void)
{
  uiAutoModeShowHopNumber(automation.numberOfHops() - _numHopToBeAdded + 1);
}

void AutoMode::AddHopNotice(void)
{

  // the first hop is added at the time boiling starts
  timers.SetAuxTimeoutAfter(hop_altering_time_s * 1000);
  _recoveryTimer = true;

  ShowHopAdding();
  _numHopToBeAdded--;
  buzzPlaySound(SoundIdAddHop);

  wiReportEvent(RemoteEventAddHop);
}

void AutoMode::ReStartBoilingTimer(void)
{
#if SerialDebug == true
  Serial.print("Boil time:");
  Serial.println(_remainingBoilTime);
  Serial.print("_numHopToBeAdded:");
  Serial.println(_numHopToBeAdded);
#endif

  // [IMPORTANT!] cast to (unsigned long) is needed

  timers.SetTimeoutAfter(_remainingBoilTime);

  if (_numHopToBeAdded > 0)
  {
    std::uint8_t idx = automation.numberOfHops() - _numHopToBeAdded;

    unsigned long nextHopTime = (unsigned long) automation.timeOfHop(idx) * 60 * 1000;

    unsigned long nextHopTimeout = _remainingBoilTime - nextHopTime;
    if (nextHopTimeout == 0)
    {
      // alert directly, start timer to restore
      AddHopNotice();
    }
    else
    {
      _recoveryTimer = false;
      timers.SetAuxTimeoutAfter(nextHopTimeout);
    }
  }
}

void AutoMode::StartBoilingTimer(void)
{
  // [IMPORTANT!] cast to (unsigned long) is needed
  // NO hop adding. just start last before

  std::uint8_t boilTime = automation.boilTime();

  _remainingBoilTime = (unsigned long) boilTime * 60 * 1000;

  _numHopToBeAdded = automation.numberOfHops();

  ReStartBoilingTimer();
}

void AutoMode::StartNextHopTimer(void)
{
  // it is done at hop timer expires :_numHopToBeAdded--;
  // this function is called after Screen is restored.(restore timer expires)

  std::uint8_t lastHopIdx = automation.numberOfHops() - _numHopToBeAdded - 1;

  if (_numHopToBeAdded > 0)  // there are next timer
  {
    std::uint8_t lastHopTime = automation.timeOfHop(lastHopIdx);

    std::uint8_t nextHopTime = automation.timeOfHop(lastHopIdx + 1);

    timers.SetAuxTimeoutAfter(((unsigned long) (lastHopTime - nextHopTime) * 60 - hop_altering_time_s) * 1000);
    _recoveryTimer = false;
  }
}

void AutoMode::BoilingPauseHandler(void)
{
  if (_isPaused)
  {
    // resume
    uiButtonLabel(ButtonLabel(Up_Down_Pause_Pmp));
    uiRunningTimeStartCountDown(_remainingBoilTime / 1000);
    ReStartBoilingTimer();

    brewLogger.event(RemoteEventResume);
  }
  else
  {
    // to pause boiling timer only
    uiRunningTimeStop();
    _remainingBoilTime = timers.PauseTimer();
    // in case hop reminder is running. restore the screen
    uiAutoModeStage(BoilingStage);
    uiButtonLabel(ButtonLabel(Up_Down_RUN_Pmp));

    brewLogger.event(RemoteEventPause);
  }
  _isPaused = !_isPaused;
}

void AutoMode::CoolingOrWhirlpool(void)
{
  if (_set_whirlpool == WhirlpoolHot)
  {
    _state = AS_Whirlpool;
    CoolingAsk(STR(WHIRLPOOL), StageWhirlpool);
  }
  else
  {
    _state = AS_Cooling;
    CoolingAsk(STR(START_COOLING), StageCooling);
  }
  //#else
  //_state = AS_Cooling;
  //autoModeCoolingAsk(STR(START_COOLING),StageCooling);
  //#endif
}

void AutoMode::StartHopStand(void)
{
  uiClearSubTitleRow();
  uiClearPrompt();

  _hopStandSession = 0;
  _hopStandSessionHopIndex = 0;
  // in case of knock off, (stating temp == boil temp, or current temperature)
  // skip chilling stage, go direct to hopstand
  float start = automation.sessionStartTemperature(0);

  if (start >= gBoilStageTemperature || start >= temperature.temperatures[Temperature::TEMP_BREW])
  {
    _state = AS_HopStand;
    brewLogger.stage(StageHopStand);
    Enter_HopStand();
  }
  else
  {
    _state = AS_HopStandChilling;
    brewLogger.stage(StageHopStandChill);
    Enter_HopStandChilling();
  }
}

void AutoMode::Enter_HopStandChilling(void)
{
  heatOff();
  //set temperature as "start" temperature.
  float start = automation.sessionStartTemperature(_hopStandSession);
  setSettingTemperature(start);
  float max = (_hopStandSession) ? (automation.sessionKeepTemperature(_hopStandSession - 1) - 1) : gBoilStageTemperature;
  setAdjustTemperature(max, automation.sessionKeepTemperature(_hopStandSession));
  // display chilling
  uiAutoModeTitle();
  uiAutoModeStage(HopStandChillingStage);

  brewLogger.stage(StageHopStandChill);
  // notify for interaction.
  uiButtonLabel(ButtonLabel(x_x_Ok_x));
  buzzPlaySoundRepeat(SoundIdWaitUserInteraction);
  _stageConfirm = false;
  uiRunningTimeShowInitial(0);
  uiRunningTimeStart();

  wiReportCurrentStage(StageHopStandChill);
}

void AutoMode::ShowPostBoilHop(void)
{
  std::uint8_t hopIdx = automation.postBoilHopIndex(_hopStandSession, _hopStandSessionHopIndex);
  uiAutoModeShowPostBoilHopNumber(hopIdx);
  buzzPlaySoundRepeat(SoundIdAddHop);
  wiReportEvent(RemoteEventAddHop);
  // NOTE: the hop time should not be ZERO.
  _recoveryTimer = true;
  timers.SetAuxTimeoutAfter(hop_altering_time_s * 1000);
}

void AutoMode::StartHopStandHopTimer(void)
{
  //
  std::uint32_t remainingTime = timers.GetRemainingTime();
  // remainingTime should not be zero.
  if (!remainingTime)
    return;

  int idx;
  std::uint32_t hopTime = 0;
  for (idx = 0; idx < automation.numberOfHopInSession(_hopStandSession); idx++)
  {
    hopTime = (std::uint32_t) automation.hopInSession(_hopStandSession, idx) * 60000;
    if (remainingTime > hopTime)
      break;
  }
  _hopStandSessionHopIndex = idx;
  if (idx >= automation.numberOfHopInSession(_hopStandSession))
  {
//    DBG_PRINTF("autoModeStartHopStandHopTimer, no more hop\n");
  }
  else
  {
    std::uint32_t hstime = remainingTime - hopTime;
//    DBG_PRINTF("autoModeStartHopStandHopTimer, %d,%d, time:%d\n", _hopStandSession, _hopStandSessionHopIndex, hstime);
    _recoveryTimer = false;
    timers.SetAuxTimeoutAfter(hstime);
  }
}

void AutoMode::Enter_HopStand(std::uint32_t elapsed)
{
  heatOn();
  pump.off();
  //set temperature as "start" temperature.
  float temp = automation.sessionKeepTemperature(_hopStandSession);
  setSettingTemperature(temp);

  float min;
  if (_hopStandSession == (automation.numberOfHopStandSession() - 1))
  {
    // last one
    min = 20;
  }
  else
  {
    min = automation.sessionStartTemperature(_hopStandSession + 1) + 1;
  }
  float max = (_hopStandSession) ? (automation.sessionKeepTemperature(_hopStandSession - 1) - 1) : gBoilStageTemperature;
  setAdjustTemperature(max, min);
  // display Hopstand
  uiButtonLabel(ButtonLabel(Up_Down_Skip_Pmp));

  uiAutoModeTitle();
  //
  if (elapsed > 0)
  {
    uiAutoModeStage(StageHopStand);
    // derive the correct time elapse.
    std::uint32_t hsTotal = (std::uint32_t) automation.hopInSession(_hopStandSession, 0);
    std::uint32_t hsLeft = hsTotal - elapsed;  // time left

    uiRunningTimeShowInitial(hsLeft * 60);
    uiRunningTimeStartCountDown(hsLeft * 60);
    timers.SetTimeoutAfter(hsLeft * 60 * 1000);
    // the main timer should start first.

    StartHopStandHopTimer();

  }
  else
  {
    // display adding hop first hop

    // display time
    std::uint32_t hstimeSecond = (std::uint32_t) automation.hopInSession(_hopStandSession, 0) * 60;
    uiRunningTimeShowInitial(hstimeSecond);
    uiRunningTimeStartCountDown(hstimeSecond);
    // start timer
    timers.SetTimeoutAfter(hstimeSecond * 1000);
    // the main timer should start first.

    // prompt for hop adding
    ShowPostBoilHop();
  }

  wiReportCurrentStage(StageHopStand);
}

void AutoMode::EndHopStandSession(void)
{
//  DBG_PRINTF("autoModeEndHopStandSession, %d,%d\n", _hopStandSession, _hopStandSessionHopIndex);
  timers.PauseTimer();

  _hopStandSession++;
  _hopStandSessionHopIndex = 0;
  if (_hopStandSession >= automation.numberOfHopStandSession())
  {
    // end of hopstand
//    DBG_PRINTF("HopStand Finished.\n");
    heatOff();
    pump.off();
    CoolingOrWhirlpool();
  }
  else
  {
    // next hopstand, enter chilling
    _state = AS_HopStandChilling;
    brewLogger.stage(StageHopStandChill);
    Enter_HopStandChilling();
  }
}

void AutoMode::HopStandTimeout(void)
{
  // end of session
  EndHopStandSession();
}

void AutoMode::AuxTimeout(void)
{
  if (_recoveryTimer)
  {
    uiAutoModeStage(HopStandStage);
    buzzMute();
    StartHopStandHopTimer();
  }
  else
  {  // hop time
     // show hop if any
//        _hopStandSessionHopIndex ++;
//        if(_hopStandSessionHopIndex < automation.numberOfHopInSession(_hopStandSession)){
    ShowPostBoilHop();
//        }
  }
}

void AutoMode::WhirlpoolInputTime(void)
{
  _whirlpoolTime = 3;
  _whirlpoolInput = true;

  uiTempDisplayHide();
  uiClearScreen();

  uiSubTitle(STR(Timeing_Whirlpool));
  uiRunningTimeShowInitial(_whirlpoolTime * 60);
  uiButtonLabel(ButtonLabel(Up_Down_Quit_Ok));
}

void AutoMode::Whirlpool(std::uint32_t elapsed)
{
  uiClearSubTitleRow();
  uiClearPrompt();

  uiAutoModeTitle();
  uiAutoModeStage(WhirlpoolStage);
  if (_set_whirlpool == WhirlpoolCold)
    gSettingTemperature = 30;
  else gSettingTemperature = 85;

  // temperature at automode
  uiTempDisplaySetPosition(TemperatureAutoModePosition);
  uiSetSettingTemperature(gSettingTemperature);
  unsigned long time = (unsigned long) (_whirlpoolTime - elapsed) * 60;
  uiRunningTimeShowInitial(time);
  uiButtonLabel(ButtonLabel(x_x_x_Pmp));

  _pumpRunning = true;
  pump.on();
  timers.SetTimeoutAfter((unsigned long) time * 1000);
  uiRunningTimeStartCountDown(time);

  //wiReportCurrentStage(StageWhirlpool);

}

void AutoMode::WhirlpoolFinish(void)
{
  pump.off();

  // if cool whirlpool, got to whirlpool, or go to end
  if (_set_whirlpool == WhirlpoolHot)
  {
    _state = AS_Cooling;
    CoolingAsk(STR(START_COOLING), StageCooling);
  }
  else
  {
    BrewEnd();
  }

}

void AutoMode::CoolingFinish(void)
{
  if (_set_whirlpool == WhirlpoolCold)
  {
    _state = AS_Whirlpool;
    CoolingAsk(STR(WHIRLPOOL), StageWhirlpool);
  }
  else
  {
    BrewEnd();
  }
}

std::uint8_t AutoMode::HopStandSessionByTime(std::uint32_t elapsed)
{
  std::uint8_t i;
  std::uint32_t time = 0;
  for (i = 0; i < automation.numberOfHopStandSession(); i++)
  {
    time += automation.hopInSession(i, 0);
    if (elapsed < time)
      break;
  }
  return i;
}

void AutoMode::StartWithoutPumpPrimming(void)
{
  if (_delayRequested)
  {
    Enter_DelayTimeInput();
  }
  else
  {
    Enter_DoughIn();
  }
}
