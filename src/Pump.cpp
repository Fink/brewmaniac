/*
 * Pump.cpp
 *
 *  Created on: Aug 3, 2020
 *      Author: fink
 */

#include "Pump.h"
#include "Pins.h"
#include "lcd_interface.h"
#include "wi.h"
#include "Settings.h"

Pump pump;

Pump::Pump()
{
}

Pump::~Pump()
{
}

void Pump::deviceOn(void)
{
  pins.Set_Output(Pins::Out_PumpControl, Pins::PIN_ON);
  uiPumpStatus(PumpStatus_On);
  wiReportPump(PumpStatus_On);
}

void Pump::deviceOff(bool programOff)
{
  pins.Set_Output(Pins::Out_PumpControl, Pins::PIN_OFF);
  if (programOff)
  {
    uiPumpStatus(PumpStatus_On_PROGRAM_OFF);
    wiReportPump(PumpStatus_On_PROGRAM_OFF);
  }
  else
  {
    uiPumpStatus(PumpStatus_Off);
    wiReportPump(PumpStatus_Off);
  }
}
void Pump::loadParameters(void)
{
  setStopTemp(static_cast<float> (settings.Get(Settings::set_TempPumpRest)));
  resetRest();

  setCycle(static_cast<float> (settings.Get(Settings::set_PumpCycle)) * 60 * 1000,
           static_cast<float> (settings.Get(Settings::set_PumpRest)) * 60 * 1000);
}

void Pump::updateUI()
{
  if (isOn())
  {
    uiPumpStatus(PumpStatus_On);
    wiReportPump(PumpStatus_On);
  }
  else
  {
    uiPumpStatus(PumpStatus_Off);
    wiReportPump(PumpStatus_Off);
  }
}
;

