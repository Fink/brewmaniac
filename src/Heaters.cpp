/*
 * Heaters.cpp
 *
 *  Created on: Aug 6, 2020
 *      Author: fink
 */

#include "Heaters.h"

Heaters heaters;

Heaters::Heaters()
{
}

Heaters::~Heaters()
{
}

void Heaters::Init()
{
  _sparge_heater_enabled = false;
  _primary_duty = 0;
  _sparge_duty = 0;
  _requested_brew_power = 0;
  _requested_sparge_power = 0;
  _duty_counter = 0;
  _outputs = { false, false };
};

void Heaters::Service_100ms()
{
  if (_duty_counter < 100)
  {
    _duty_counter++;

    // Primary only
    if (_duty_counter <= _requested_brew_power)
    {
      _outputs.brew = true;
    }
    else
    {
      _outputs.brew = false;
    }

    // Sparge
    if (_sparge_heater_enabled)
    {
      if (_duty_counter <= _requested_sparge_power)
      {
        _outputs.sparge = (_outputs.brew == false) ? true : false;
      }
      else
      {
        _outputs.sparge = false;
      }
    }
    else
    {
      _outputs.sparge = false;
    }
  }
  else
  {
    _duty_counter = 0;
  }
}

void Heaters::Enable_Sparge_Heater(bool enable)
{
  _sparge_heater_enabled = enable;
}

void Heaters::Set_Brew_Power(std::uint8_t pct)
{
  if (pct > 100)
    pct = 100;
  _requested_brew_power = pct;
}

void Heaters::Set_Sparge_Power(std::uint8_t pct)
{
  if (pct > 100)
    pct = 100;
  _requested_sparge_power = pct;
}

void Heaters::Get_Outputs(t_outputs *outputs)
{
  *outputs = _outputs;
}

