/*
 * Menu.cpp
 *
 *  Created on: Aug 18, 2020
 *      Author: fink
 */

#include "Menu.h"
#include "lcd_interface.h"
#include "Distill.h"
#include "Sensors.h"
#include "stages.h"
#include "automation.h"
#include "wi.h"
#include "Display.h"
#include "debug.h"

Menu menu;

Menu::Menu()
{
}

Menu::~Menu()
{
}

void Menu::Init()
{
  switchApplication(MAIN_SCREEN);
}



void Menu::switchApplication(std::uint8_t screenId)
{
  _currentScreen = &allScreens[screenId];
  //turn off temperature update by default, let those who want turn it on

  uiClearSettingRow();

  uiRunningTimeStop();
  uiTempDisplayHide();

  //default event is keyboard
  SetEventMask(ButtonPressedEventMask);

  (*_currentScreen->setup)();
}



bool Menu::RunCurrentEventHandler(std::uint8_t event, std::uint8_t mask)
{
  return (*_currentScreen->eventHandler)(event, mask);
}

int Menu::ToTempForEditing(float t)
{
  return (int) ((t) * 4);
}

float Menu::TempFromEditing(int t)
{
  return (float) (t) / 4.0;
}

// Free functions

void distillingSetup(void)
{
  distillingController.setup();
}

bool distillingEventHandler(byte event, std::uint8_t mask)
{
  return distillingController.eventHandler(event, mask);
}

bool mainEventHandler(byte event, std::uint8_t mask)
{
  switch (mask)
  {
  case ButtonMaskEnter:
    menu.switchApplication(Menu::SETUP_SCREEN);
    break;

  case ButtonMaskDown:
    menu.switchApplication(Menu::MANUAL_MODE_SCREEN);
    break;

  case ButtonMaskStart:
    menu.switchApplication(Menu::AUTO_MODE_SCREEN);
    break;

  case ButtonMaskUp:
    menu.switchApplication(Menu::DISTILLING_MODE_SCREEN);
    break;

  default:
    break;
  }

  return true;
}


void settingPidSetup(void)
{
  settingEditor.setup(pidSettingItems);
  settingEditor.displayItem();
}

bool settingPidEventHandler(std::uint8_t event, std::uint8_t mask)
{
  if (settingEditor.buttonHandler(static_cast<e_button_mask>(mask)))
  {
    if (settingEditor.GetCurrentIndex() == (sizeof(pidSettingItems) / sizeof(SettingEditor::SettingItem) - 2))
    {
      if (0 == sensors.gSensorNumber)
      {
        menu.switchApplication(Menu::SETUP_SCREEN);
        return true;
      }
      menu.pidSettingAux = 0;
      settingEditor.nextItem();
      settingEditor.editItemTitleAppendNumber(menu.pidSettingAux + 1);
      return true;
    }
    else if (settingEditor.GetCurrentIndex() == (sizeof(pidSettingItems) / sizeof(SettingEditor::SettingItem) - 1))
    {
      // last
      menu.pidSettingAux++;
      if (menu.pidSettingAux < Sensors::Sensor_Count)
      {
        settingEditor.displayItem();
        settingEditor.editItemTitleAppendNumber(menu.pidSettingAux + 1);
        return true;
      }

      menu.switchApplication(Menu::SETUP_SCREEN);
      return true;

    }
    settingEditor.nextItem();
  }
  return true;
}

void settingUnitSetup(void)
{
  settingEditor.setup(unitSettingItems);
  settingEditor.displayItem();

}

bool settingUnitEventHandler(std::uint8_t event, std::uint8_t mask)
{
  if (settingEditor.buttonHandler(static_cast<e_button_mask>(mask)))
  {
    int index = settingEditor.GetCurrentIndex();
    if (index >= (int) (sizeof(unitSettingItems) / sizeof(SettingEditor::SettingItem) - 1))
    {
      menu.switchApplication(Menu::SETUP_SCREEN);
      return true;
    }

    else settingEditor.nextItem();
  }
  return true;
}

void miscSettingSetup(void)
{
  settingEditor.setup(miscSettingItems);
  settingEditor.displayItem();
}

bool miscSettingEventHandler(std::uint8_t event, std::uint8_t mask)
{
  if (settingEditor.buttonHandler(static_cast<e_button_mask>(mask)))
  {
    int index = settingEditor.GetCurrentIndex();

    if (index >= (int) (sizeof(miscSettingItems) / sizeof(SettingEditor::SettingItem) - 1))
    {
      menu.switchApplication(Menu::SETUP_SCREEN);
      return true;
    }
    else settingEditor.nextItem();
  }
  return true;
}

void resetSelection(byte sensorNumber)
{
  for (byte i = 0; i < Temperature::TEMP_COUNT; i++)
  {
    menu.sensorSelected[i] = (i < sensorNumber) ? false : true;
  }
}

int nextUp(byte i)
{
  int idx = i;
  idx--;
  for (; idx >= 0; idx--)
  {
    if (menu.sensorSelected[idx] == false)
      return idx;
  }
  return -1;
}

int firstAvailable(void)
{
  int idx = 0;
  for (; idx < Temperature::TEMP_COUNT; idx++)
  {
    if (menu.sensorSelected[idx] == false)
      return idx;
  }
  return -1;
}

int nextDown(byte i)
{
  int idx = i;
  idx++;

  for (; idx < Temperature::TEMP_COUNT; idx++)
  {
    if (menu.sensorSelected[idx] == false)
      return idx;
  }
  return -1;
}

/* helper functions*/

void sensorMenuSelectSensorButtonChange(void)
{
  bool up = (nextUp(menu.sensorSelection) >= 0);
  bool down = (nextDown(menu.sensorSelection) >= 0);

  if (up && down)
    uiButtonLabel(ButtonLabel(Up_Down_Quit_Ok));
  else if (down)
    uiButtonLabel(ButtonLabel(x_Down_Quit_Ok));
  else if (up)
    uiButtonLabel(ButtonLabel(Up_x_Quit_Ok));
  else uiButtonLabel(ButtonLabel(x_x_Quit_Ok));
}

void sensorMenuItem(void)
{
  if (menu.editingStage == 0)
  {
    if (sensors.gSensorNumber == 0)
    {
      uiSettingTitle(STR(No_Sensor_Found));
      uiButtonLabel(ButtonLabel(x_x_x_Ok));
      return;
    }
    //else
    menu.sensorSelection = (byte) firstAvailable();

    uiSettingSensorIndex(menu.editingStageAux);  // print #1
    uiSettingSensorAddress(sensors.sensor_addresses[menu.sensorSelection], sensors.result[menu.sensorSelection]);
    sensorMenuSelectSensorButtonChange();
  }
  else
  {
    if (menu.editingStage == (temperature.SensorForBrew + 1))
    {
      if (menu.editingStageAux == 0)
        uiSettingTitle(STR(Sensor_Idle));
      else uiSettingTitle(STR(Sensor_Idle));

    }
    else if (menu.editingStage == (temperature.SensorForSparge + 1))
    {
      if (menu.editingStageAux == 0)
        uiSettingTitle(STR(Sparge_Sensor));
      else uiSettingTitle(STR(Sparge_Sensor));
    }
    int s = firstAvailable();
    menu.sensorSelection = (s >= 0) ? s : 0;
    uiSettingDisplayNumber((float) menu.sensorSelection + 1, 0);
    sensorMenuSelectSensorButtonChange();
  }
}

void sensorMenuSetup(void)
{
  menu.editingStage = 0;
  menu.editingStageAux = 0;
  sensors.scanSensors(MaximumNumberOfSensors, sensors.sensor_addresses);
  for (byte i = 0; i < MaximumNumberOfSensors; i++)
    sensors.calibration[i] = 0;

  resetSelection(sensors.gSensorNumber);
  sensorMenuItem();
}

bool sensorMenuEventHandler(std::uint8_t event, std::uint8_t mask)
{
  if (menu.editingStage == 0 && sensors.gSensorNumber == 0)
  {
    if (mask == ButtonMaskEnter)
    {
      //uiClearSettingRow();
      menu.switchApplication(Menu::SETUP_SCREEN);
      return true;
    }
    return false;
  }

  if (menu.editingStage == 0)
  {
    switch (mask)
    {
    case ButtonMaskEnter:
      sensors.saveSensor(menu.editingStageAux, sensors.sensor_addresses[menu.sensorSelection]);
      //updateSetting(CalibrationAddressOf(menu.editingStageAux), 50);  // zero
      sensors.calibration[menu.editingStageAux] = 0;
      menu.editingStageAux++;

      menu.sensorSelected[menu.sensorSelection] = true;

#if SerialDebug == true
      Serial.print("menu.editingStageAux:");
      Serial.print(menu.editingStageAux);
      Serial.print(" gSensorNumber=");
      Serial.println(sensors.gSensorNumber);
#endif

      if (menu.editingStageAux >= sensors.gSensorNumber)
      {
        // next item
        menu.editingStage++;
        menu.editingStageAux = 0;
        resetSelection(sensors.gSensorNumber);
      }
      sensorMenuItem();
      break;

    case ButtonMaskUp:
    case ButtonMaskDown:
    {
      int next = (mask == ButtonMaskUp) ? nextUp(menu.sensorSelection) : nextDown(menu.sensorSelection);
      if (next >= 0)
      {
        menu.sensorSelection = (byte) next;
        uiSettingSensorAddress(sensors.sensor_addresses[menu.sensorSelection], sensors.result[menu.sensorSelection]);
        sensorMenuSelectSensorButtonChange();

      }
    }
      break;

    case ButtonMaskStart:
      break;

    default:
      break;
    }
  }
  else  // all after
  {
    switch (mask)
    {
    case ButtonMaskEnter:
      // save
      if (menu.editingStageAux == 0)
      {
        //settingEditor.changeSettingValue(PS_SensorUseAddressOf(menu.editingStage - 1), menu.sensorSelection);
        menu.editingStageAux = 1;
        menu.sensorSelected[menu.sensorSelection] = true;
      }
      else
      {
        //settingEditor.changeSettingValue(PS_AuxSensorAddressOf(menu.editingStage - 1), menu.sensorSelection);
        menu.editingStage++;
        menu.editingStageAux = 0;
        resetSelection(sensors.gSensorNumber);
      }

      if (menu.editingStage > sensors.gSensorNumber)
      {
        //uiClearSettingRow();
        menu.switchApplication(Menu::SETUP_SCREEN);
        //commitSetting();
        // reaload sensor
        sensors.loadSensorSetting();
        return true;
      }
      sensorMenuItem();
      break;

    case ButtonMaskStart:
      menu.switchApplication(Menu::SETUP_SCREEN);
      break;

    case ButtonMaskUp:
    case ButtonMaskDown:
    {
      int next = (mask == ButtonMaskUp) ? nextUp(menu.sensorSelection) : nextDown(menu.sensorSelection);
      if (next >= 0)
      {
        menu.sensorSelection = (byte) next;
        uiSettingDisplayNumber((float) menu.sensorSelection + 1, 0);
        sensorMenuSelectSensorButtonChange();
      }
    }
      break;

    default:
      break;
    }
  }
  return true;
}

void distillRecipeSetup(void)
{
  distillRecipe.Init();
}

bool distillRecipeEventHandler(std::uint8_t event, std::uint8_t mask)
{
  return distillRecipe.Eventhandler(event, mask);
}

void settingAutomationDisplayItem(void)
{
  int value = 0;

  if (menu.editingStage <= MashStepMashOut /*7*/)  // from MashIn,Phytase,Glucanase,Protease,bAmylase,aAmylase1,aAmylase2,MashOut
  {
    if (menu.editingStageAux == 0)
    {
      value = menu.ToTempForEditing(automation.stageTemperature(menu.editingStage));
    }
    else
    {
      value = automation.stageTime(menu.editingStage);
      if (value == 0)
        value = 1;
    }
  }
  // else read value later
  //8. number of hops
  //9. boil time
  //10. time hop number #

  if (menu.editingStageAux == 1   // 1:time editing of stage 1 to 5, step 6 has no End, nor More optoin
      && menu.editingStage > 0 && menu.editingStage < 6)  // except MashIn/MashOut, and in Temperature editing
    uiButtonLabel(ButtonLabel(Up_Down_End_More));
  else uiButtonLabel(ButtonLabel(Up_Down_x_Ok));
  if (menu.editingStage == 0)
  {
    int max = menu.ToTempForEditing(75);
    int min = menu.ToTempForEditing(20);
    // Mash In:temp only
    float __value = menu.TempFromEditing(value);
    settingEditor.editItem(STR(Mash_In), __value, max, min, &Display_StageTemperature);
  }

  else if (menu.editingStage > 0 && menu.editingStage < MashStepMashOut /*7*/)
  {
    int max = menu.ToTempForEditing(76);
    int minTemp;

    if (menu.editingStage == 1)
    {
      minTemp = menu.ToTempForEditing(25);
    }
    else
    {
      minTemp = menu.ToTempForEditing(automation.stageTemperature(menu.editingStage - 1)) + 1;
    }

    if (menu.editingStageAux == 0)
    {
      float __value = menu.TempFromEditing(value);
      settingEditor.editItem(STR(Mash_x), __value, max, minTemp, &Display_StageTemperature);
    }
    else
    {
      settingEditor.editItem(STR(Mash_x), value, menu.max_stage_time, menu.min_stage_time, &Display_Time);
    }
    settingEditor.editItemTitleAppendNumber(menu.editingStage);
  }

  else if (menu.editingStage == MashStepMashOut /*7*/)
  {
    // MashOut
    if (menu.editingStageAux == 0)
    {
      int max = menu.ToTempForEditing(80);
      int min = menu.ToTempForEditing(75);

      float __value = menu.TempFromEditing(value);
      settingEditor.editItem(STR(Mash_out), __value, max, min, &Display_StageTemperature);
    }
    else settingEditor.editItem(STR(Mash_out), value, menu.max_stage_time, menu.min_stage_time, &Display_Time);
  }
  else if (menu.editingStage == 8)
  {
    // 8. number of hops
    value = automation.numberOfHops();
    // boiling, need to input
    settingEditor.editItem(STR(Number_Of_Hops), value, 10, 0, &Display_SimpleInteger);
  }
  else if (menu.editingStage == 9)
  {
    // 9. boil time
    value = automation.boilTime();
    // boiling, need to input
    settingEditor.editItem(STR(Boil), value, menu.max_stage_time, menu.min_stage_time, &Display_Time);
  }
  else if (menu.editingStage == 10)
  {
    //10. hops
    value = automation.timeOfHop(menu.editingStageAux);

    if (value > menu.maxHopTime)
      value = menu.maxHopTime;
    //create a number
    // hop number starts from 1

    settingEditor.editItem(STR(Hops_Number_x), value, menu.maxHopTime, 0, &Display_Time);
    settingEditor.editItemTitleAppendNumber(menu.editingStageAux + 1);  //
  }
  else if (menu.editingStage == 11)  // hop session
  {
    //11. hopstand session number
    value = automation.numberOfHopStandSession();
    //DBG_PRINTF("numberOfHopStandSession:%d\n",value);

    if (value > MAXIMUM_HSSESSION_NUMBER)
      value = 0;
    //DBG_PRINTF("numberOfHopStandSession:%d\n",value);
    //create a number
    // hop number starts from 1
    settingEditor.editItem(STR(HS_Session_Number), value, MAXIMUM_HSSESSION_NUMBER, 0, &Display_SimpleInteger);
  }
  else if (menu.editingStage == 12)  // hop session start temperature
  {
    value = automation.sessionStartTemperature(menu.editingStageAux);

    int min = menu.ToTempForEditing(25);
    int max;
    if (menu.editingStageAux == 0)
    {
      max = menu.ToTempForEditing(100);
    }
    else
    {
      max = menu.ToTempForEditing(automation.sessionKeepTemperature(menu.editingStageAux - 1) - 1);
    }
    if (value > max || value < min)
      value = max;

    float __value = menu.TempFromEditing(value);
    settingEditor.editItem(STR(HS_Start), __value, max, min, &Display_StageTemperature);
    settingEditor.editItemTitleAppendNumber(menu.editingStageAux + 1);
  }
  else if (menu.editingStage == 13)  // hop session keep temperature
  {
    value = automation.sessionKeepTemperature(menu.editingStageAux);

    int min = menu.ToTempForEditing(25);
    int max = menu.ToTempForEditing(automation.sessionStartTemperature(menu.editingStageAux));
    if (value > max || value < min)
      value = max;

    float __value = menu.TempFromEditing(value);
    settingEditor.editItem(STR(HS_Keep), __value, max, min, &Display_StageTemperature);
    settingEditor.editItemTitleAppendNumber(menu.editingStageAux + 1);
  }
  else if (menu.editingStage == 14)  //post hop time
  {
    value = automation.hopInSession(menu.editingStageAux, menu.postBoilHopIndex);

    int max = (menu.postBoilHopIndex == 0) ? 255 : automation.hopInSession(menu.editingStageAux, menu.postBoilHopIndex - 1);

    settingEditor.editItem(STR(PBH_x), value, max, 0, &Display_Time);
    settingEditor.editItemTitleAppendNumber(automation.postBoilHopIndex(menu.editingStageAux, menu.postBoilHopIndex));
    uiButtonLabel(ButtonLabel(Up_Down_End_More));
  }
}

void settingAutoSetup(void)
{
  menu.editingStage = 0;
  menu.editingStageAux = 0;
  settingAutomationDisplayItem();
}

bool settingAutoEventHandler(std::uint8_t event, std::uint8_t mask)
{
  switch (mask)
  {
  case ButtonMaskEnter:
  {
    int value = settingEditor.editItemValue();

    if (menu.editingStage == 0)
    {

      menu.editingStageAux = 0;
    }
    if (menu.editingStage <= MashStepMashOut /*7*/)
    {
      if (menu.editingStageAux == 0)
      {
        changeAutomationTemperature(menu.editingStage, menu.TempFromEditing(value));

        if (menu.editingStage == 0)  // no time needed for Mash In
          menu.editingStage++;
        else menu.editingStageAux = 1;
      }
      else
      {
        changeAutomationTime(menu.editingStage, (byte) value);
        //next stage
        menu.editingStageAux = 0;
        menu.editingStage++;
      }
    }
    else if (menu.editingStage == 8)
    {
      automation.setNumberOfHops((uint8_t) value);
      //number of hops
      menu.editingStage++;
    }
    else if (menu.editingStage == 9)
    {
      automation.setBoilTime((uint8_t) value);
      // set the maxHopTime for the first hop
      menu.maxHopTime = value;
      // boiling time;

      menu.editingStageAux = 0;
      int hopsNum = automation.numberOfHops();
      if (hopsNum)
      {
        menu.editingStage++;

      }
      else
      {
        finishAutomationEdit();

        //uiClearSettingRow();
        menu.switchApplication(Menu::SETUP_SCREEN);
        return true;
      }
    }
    else if (menu.editingStage == 10)
    {
      automation.setTimeOfHop(menu.editingStageAux, (byte) value);
      // update masxHoptime for next hop
      menu.maxHopTime = value - 1;

      int hopsNum = automation.numberOfHops();

      if (menu.editingStageAux == (hopsNum - 1))
      {
        //finish
        //finishAutomationEdit();

        //uiClearSettingRow();
        //menu.switchApplication(Menu::SETUP_SCREEN);
        menu.editingStage++;
      }
      else
      {
        menu.editingStageAux++;

      }
    }
    else if (menu.editingStage == 11)
    {
      automation.setNumberOfHopStandSession(value);
      if (value == 0)
      {
        finishAutomationEdit();
        menu.switchApplication(Menu::SETUP_SCREEN);
        return true;

      }
      else
      {
        menu.editingStageAux = 0;
        menu.editingStage++;
      }
    }
    else if (menu.editingStage == 12)  //start
    {
      automation.setSessionStartTemperature(menu.editingStageAux, menu.TempFromEditing(value));
      menu.editingStage++;
    }
    else if (menu.editingStage == 13)  // keep
    {
      automation.setSessionKeepTemperature(menu.editingStageAux, menu.TempFromEditing(value));
      menu.editingStage++;
      menu.postBoilHopIndex = 0;
    }
    else if (menu.editingStage == 14)  //post hop time
    {
      automation.setHopInSession(menu.editingStageAux, menu.postBoilHopIndex, value);

      menu.postBoilHopIndex++;

      if (automation.postBoilHopIndex(menu.editingStageAux, menu.postBoilHopIndex) > MAXIMUM_POST_BOIL_HOP_NUMBER || (menu.postBoilHopIndex
          >= MAXIMUM_HOP_IN_HSSESSION
                                                                                                                      && (menu.editingStageAux + 1) == automation.numberOfHopStandSession()))
      {
        // force to stop
        automation.setNumberOfHopStandSession(menu.editingStageAux + 1);
        finishAutomationEdit();
        menu.switchApplication(Menu::SETUP_SCREEN);
        return true;
      }
      if (menu.postBoilHopIndex >= MAXIMUM_HOP_IN_HSSESSION)
      {
        // next session
        automation.setNumberOfHopInSession(menu.editingStageAux, menu.postBoilHopIndex + 1);
        menu.editingStageAux++;
        menu.postBoilHopIndex = 0;
        menu.editingStage = 12;
      }
    }

    //next item
    settingAutomationDisplayItem();
  }
    break;

  case ButtonMaskStart:
    // only handle in stage 1 to 5
    if ((menu.editingStage >= 1 && menu.editingStage < 6) && menu.editingStageAux == 1)
    {
      int value = settingEditor.editItemValue();
      changeAutomationTime(menu.editingStage, (byte) value);
      // End Mash step and go to MashOut
      changeAutomationTime(menu.editingStage + 1, (byte) 0);
      menu.editingStage = MashStepMashOut;  //7;
      menu.editingStageAux = 0;
      settingAutomationDisplayItem();
    }
    else if (menu.editingStage == 14)
    {
      int value = settingEditor.editItemValue();
      automation.setHopInSession(menu.editingStageAux, menu.postBoilHopIndex, value);
      automation.setNumberOfHopInSession(menu.editingStageAux, menu.postBoilHopIndex + 1);
      menu.editingStageAux++;
      if (menu.editingStageAux == automation.numberOfHopStandSession())
      {
        // finish
        finishAutomationEdit();
        menu.switchApplication(Menu::SETUP_SCREEN);
        return true;
      }
      else
      {
        // next
        menu.postBoilHopIndex = 0;
        menu.editingStage = 12;
        settingAutomationDisplayItem();
      }
    }
    break;

  case ButtonMaskUp:
    settingEditor.editItemChange(+1);
    break;

  case ButtonMaskDown:
    settingEditor.editItemChange(-1);
    break;

  case ButtonMaskContUp:
    settingEditor.editItemChange(+5);
    break;

  case ButtonMaskContDown:
    settingEditor.editItemChange(-5);
    break;

  default:
    return false;
  }

  return true;
}

void menuDisplayList(byte index)
{
  byte menuNO = (sizeof(level1Menu) / sizeof(char const*)) - 1;

  uiSubTitle(level1Menu[index]);
  if (index == 0)
    uiButtonLabel(ButtonLabel(x_Down_Quit_Ok));
  else if (index == menuNO)
    uiButtonLabel(ButtonLabel(Up_x_Quit_Ok));
  else uiButtonLabel(ButtonLabel(Up_Down_Quit_Ok));
}

void menuSetup(void)
{
  uiTitle(STR(Setup));
//  _currentLevelOne=0; "remember" last menu position
  menuDisplayList(menu.currentLevelOne);

  wiReportCurrentStage(StageSetting);
}

bool menuEventHandler(std::uint8_t event, std::uint8_t mask)
{
  switch (mask)
  {
  case ButtonMaskEnter:
    menu.switchApplication(menu.level1Screens[menu.currentLevelOne]);
    break;

  case ButtonMaskStart:
    backToMain();
    break;

  case ButtonMaskUp:
    if (menu.currentLevelOne > 0)
    {
      menu.currentLevelOne--;
      menuDisplayList(menu.currentLevelOne);

    }
    break;

  case ButtonMaskDown:
  {
    byte menuNO = (sizeof(level1Menu) / sizeof(char const*)) - 1;

    if (menu.currentLevelOne < menuNO)
    {
      menu.currentLevelOne++;
      menuDisplayList(menu.currentLevelOne);

    }
  }
    break;

  default:
    return false;
  }

  return true;
}

void changeAutomationTime(byte stage, byte time)
{
  automation.setStageTime(stage, time);
}

void changeAutomationTemperature(byte stage, float value)
{
  automation.setStageTemperature(stage, value);
}

void finishAutomationEdit(void)
{
  automation.save();
  wiRecipeChange();
}

