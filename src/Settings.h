/*
 * Settings.h
 *
 *  Created on: Aug 21, 2020
 *      Author: fink
 */

#ifndef SRC_SETTINGS_H_
#define SRC_SETTINGS_H_

#include <cstdint>

/*********************************************************************************************************************************/
/*********************************************************************************************************************************/
/*      Index,                                Name ,    WebPar         ,     Min,                Max,           Default    Scale */
/*********************************************************************************************************************************/
#define DB_ITEMS                                                                                            \
  ITEM (set_Init                      , "Initalize DB", ""             ,       0,                  1,                 0,    1) \
  ITEM (set_Kp                        , "Constant Kp ", "s_kp"         ,    -500,                500,               200,  100) \
  ITEM (set_Ki                        , "Constant Ki ", "s_ki"         ,    -500,                500,                50,  100) \
  ITEM (set_Kd                        , "Constant Kd ", "s_kd"         ,    -500,                500,               200,  100) \
  ITEM (set_BoilHeat                  , "Heat in Boil", "s_pwm"        ,       0,                100,                75,    1) \
  ITEM (set_PID_Start                 , "Start PID in", "s_pidstart"   ,       0,                  1,                 1,    1) \
  ITEM (set_PID_DoughIn               , "PID Dough In", "s_piddoughin" ,       0,                  1,                 1,    1) \
  ITEM (set_TempUnit                  , "Temp Unit   ", "s_unit"       ,       0,                  1,                 0,    1) \
  ITEM (set_NoDelayStart              , "No Delay St.", "s_nodelay"    ,       0,                  1,                 0,    1) \
  ITEM (set_BoilTemp                  , "Boil Temp   ", "s_boil"       ,       0,                101,                99,    1) \
  ITEM (set_PumpCycle                 , "Pump Cycles ", "s_pumpcycle"  ,       5,                 15,                 5,    1) \
  ITEM (set_PumpRest                  , "Pump Rest   ", "s_pumprest"   ,       0,                  5,                 0,    1) \
  ITEM (set_PumpPreMash               , "Pump PreMash", "s_pumppremash",       0,                  1,                 0,    1) \
  ITEM (set_PumpOnMash                , "Pump On Mash", "s_pumpmash"   ,       0,                  1,                 1,    1) \
  ITEM (set_PumpOnMashOut             , "Pump Mashout", "s_pumpmashout",       0,                  1,                 1,    1) \
  ITEM (set_PumpOnBoil                , "Pump On Boil", "s_pumpboil"   ,       0,                  1,                 0,    1) \
  ITEM (set_TempPumpRest              , "Temp Pmp Rst", "s_pumpstop"   ,      80,                105,                 0,    1) \
  ITEM (set_PidPipe                   , "Pid Pipe    ", "s_pipe"       ,       0,                  1,                 1,    1) \
  ITEM (set_SkipAddMalt               , "Skip Add Mlt", "s_skipadd"    ,       0,                  1,                 0,    1) \
  ITEM (set_SkipRemoveMalt            , "Skip Rem Mlt", "s_skipremove" ,       0,                  1,                 0,    1) \
  ITEM (set_SkipIodineTest            , "Skip Iodine ", "s_skipiodine" ,       0,                  1,                 0,    1) \
  ITEM (set_IodineTime                , "Iodine Time ", "s_iodine"     ,       0,                 90,                 0,    1) \
  ITEM (set_Whirlpool                 , "Whirlpool   ", "s_whirlpool"  ,       0,                  2,                 0,    1) \
  ITEM (set_SpargeWaterEnable         , "Sparge Enabl", "s_spenable"   ,       0,                  1,                 1,    1) \
  ITEM (set_SpargeWaterTempeControl   , "Sp Temp Ctrl", "s_sptempctrl" ,       0,                  1,                 1,    1) \
  ITEM (set_SpargeWaterSensorIndex    , "Sp Sens idx ", "s_spsensor"   ,       0,                  2,                 2,    1) \
  ITEM (set_SpargeWaterTemperature    , "Sp Temp     ", "s_sptemp"     ,      70,                 80,                78,    1) \
  ITEM (set_ButtonFeedback            , "Beep Enable ", "s_btnbuzz"    ,       0,                  1,                 0,    1) \
  ITEM (set_PumpPrimeCount            , "Prime Count ", "s_pprime"     ,       0,                 10,                 5,    1) \
  ITEM (set_PumpPrimeOnTime           , "Pr On Time  ", "s_ppon"       ,       1,                 40,                 5,    1) \
  ITEM (set_PumpPrimeOffTime          , "Pr Off Time ", "s_ppoff"      ,       1,                 40,                 2,    1) \
  ITEM (set_DistillHeadPwm            , "Head PWM    ", ""             ,       0,                100,                50,    1) \
  ITEM (set_DistillHeadTemp           , "Head Temp   ", ""             ,       0,                100,                70,    1) \
  ITEM (set_DistillHeartPwm           , "Heart PWM   ", ""             ,       0,                100,               100,    1) \
  ITEM (set_DistillHeartTemp          , "Heart Temp  ", ""             ,       0,                100,                55,    1) \
  ITEM (set_DistillTailPwm            , "Tail PWM    ", ""             ,       0,                100,                83,    1) \
  ITEM (set_DistillTailTemp           , "Tail Temp   ", ""             ,       0,                100,               100,    1) \
  ITEM (set_DistillEndTemp            , "End Temp    ", ""             ,       0,                100,               100,    1) \
  ITEM (set_SensorAddress_Brew_MSB    , "Sensor Brew ", ""             ,       0,  (std::int32_t)0xFFFFFFFF,          0,    1) \
  ITEM (set_SensorAddress_Brew_LSB    , "Sensor Brew ", ""             ,       0,  (std::int32_t)0xFFFFFFFF,          0,    1) \
  ITEM (set_SensorAddress_Sparge_MSB  , "Sensor Sparg", ""             ,       0,  (std::int32_t)0xFFFFFFFF,          0,    1) \
  ITEM (set_SensorAddress_Sparge_LSB  , "Sensor Sparg", ""             ,       0,  (std::int32_t)0xFFFFFFFF,          0,    1) \
  ITEM (set_SensorCalibration_Brew    , "Cal Brew    ", ""             ,       0,                300,                 0,  100) \
  ITEM (set_SensorCalibration_Sparge  , "Cal Spage   ", ""             ,       0,                300,                 0,  100) \
  ITEM (set_Fake_Heat                 , "Fake Heater ", ""             ,       0,                  1,                 0,    1) \
/**********************************************************************************************************************************/
/**********************************************************************************************************************************/


class Settings
{
  public:
#define ITEM(Index, Name, WebPar, Min, Max, Default, Scale) Index,
typedef enum
{
  DB_ITEMS
  DB_COUNT
} db_t;
#undef ITEM

    Settings();
    virtual ~Settings();

    void Init();
    void Service();

    void GetText(db_t Index, char * dbString);
    void GetWebPar(db_t Index, char * dbString);
    float Get(db_t idx);
    std::uint32_t Get_u(db_t idx);
    float GetMin(db_t idx);
    float GetMax(db_t idx);
    bool Set(db_t idx, float f);
    bool Set(db_t idx, std::uint32_t u);
    static const std::uint8_t max_name_length = 14;

  private:
    std::int32_t _db_ram[DB_COUNT];
    bool _dirty;
    char * _ee_address;
    std::uint32_t _ee_size;
};

extern Settings settings;

#endif /* SRC_SETTINGS_H_ */
