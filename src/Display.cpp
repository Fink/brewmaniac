/*
 * Display.cpp
 *
 *  Created on: Aug 18, 2020
 *      Author: fink
 */

#include "Display.h"
#include "lcd_interface.h"
#include "ui_globals.h"

void Display_Offset100(int data)
{
  float fvalue = (float) data - 100.0;
  uiSettingDisplayNumber(fvalue, 0);
}

void Display_Multiply250(int data)
{
  float fvalue = (float) data * 250.0;
  uiSettingDisplayNumber(fvalue, 0);
}

void Display_Multiply50(int data)
{
  float fvalue = (float) data * 50.0;
  uiSettingDisplayNumber(fvalue, 0);
}

void Display_SimpleInteger(int data)
{
  uiSettingDisplayNumber((float) data, 0);
}

void Display_TempShift50Divide10(int data)
{
  float fvalue = ((float) data - 50.0) / 10.0;

  uiSettingShowTemperature(fvalue, 1);
}

void Display_TempDivide10(int data)
{
  float fvalue = ((float) data) / 10.0;

  uiSettingShowTemperature(fvalue, 1);
}

void Display_Percentage(int data)
{
  uiDisplaySettingPercentage(data);
}

void Display_Resolution(int value)
{
  uiSettingDisplayNumber((float) value, 0);
}
void Display_OnOff(int value)
{
  if (value == 0)
    uiSettingDisplayText(STR(Off));
  else uiSettingDisplayText(STR(On));
}

void Display_DegreeSymbol(int value)
{
  uiSettingDegreeSymbol(value);
}

void Display_InsideOutside(int value)
{
  if (value == 0)
    uiSettingDisplayText(STR(Inside));
  else uiSettingDisplayText(STR(Outside));
}

void Display_YesNo(int value)
{
  if (value == 0)
    uiSettingDisplayText(STR(No));
  else uiSettingDisplayText(STR(Yes));
}


void Display_Time(int value)
{
  uiSettingTimeInMinutes((byte) value);
}

void Display_TimeOff(int value)
{
  if (value == 0)
  {
    uiSettingDisplayText(STR(Off));
  }
  else
  {
    Display_Time(value);
  }
}

void Display_SimpleTemperature(int value)
{
  uiSettingShowTemperature((float) value, 0);
}

void Display_HotColdOff(int value)
{
  if (value == 0)
    uiSettingDisplayText(STR(Off));
  else if (value == WhirlpoolCold)
    uiSettingDisplayText(STR(Cold));
  else uiSettingDisplayText(STR(Hot));
}

void Display_StageTemperature(int value)
{
  uiSettingShowTemperature((float) value, 2);
}

void Display_IntegerPlusOne(int data)
{
  uiSettingDisplayNumber((float) data + 1, 0);
}

