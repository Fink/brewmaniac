/*
 * Display.h
 *
 *  Created on: Aug 18, 2020
 *      Author: fink
 */

#ifndef DISPLAY_H_
#define DISPLAY_H_

void Display_Offset100(int data);
void Display_Multiply250(int data);
void Display_Multiply50(int data);
void Display_SimpleInteger(int data);
void Display_TempShift50Divide10(int data);
void Display_TempDivide10(int data);
void Display_Percentage(int data);
void Display_Resolution(int value);
void Display_OnOff(int value);
void Display_DegreeSymbol(int value);
void Display_InsideOutside(int value);
void Display_YesNo(int value);
void Display_Time(int value);
void Display_TimeOff(int value);
void Display_SimpleTemperature(int value);
void Display_HotColdOff(int value);
void Display_StageTemperature(int value);
void Display_IntegerPlusOne(int data);

#endif /* DISPLAY_H_ */
