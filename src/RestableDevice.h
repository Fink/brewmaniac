/*
 * RestableDevice.h
 *
 *  Created on: Aug 3, 2020
 *      Author: fink
 */

#ifndef SRC_RESTABLEDEVICE_H_
#define SRC_RESTABLEDEVICE_H_

#include <cstdint>

class RestableDevice
{
    void virtual deviceOn(void)=0;
    void virtual deviceOff(bool)=0;

  public:
    RestableDevice();
    virtual ~RestableDevice();

    void turnPhysicalOn(void);
    void turnPhysicalOff(void);
    void setCycle(std::uint32_t cycle, std::uint32_t rest);
    void resetRest(void);
    void setStopTemp(float temp);
    void off(void);
    void on(void);
    void toggle(void);
    bool isRestEnabled(void);
    bool isOn(void);
    bool isRest(void);
    bool isPhysicalOn(void);

    void setRestEnabled(bool enable);
    bool restEvent(void);
    void run(void);

    bool _isDeviceOn;
    bool _physicalOn;
    std::uint32_t _lastSwitchOnTime;
    float _stopTemp;

    std::uint32_t _restTime;
    std::uint32_t _cycleTime;
    bool _isRestStateChanged;
    bool _restEnabled;

};

#endif /* SRC_RESTABLEDEVICE_H_ */
