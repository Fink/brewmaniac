/*
 * Pins.cpp
 *
 *  Created on: Aug 13, 2020
 *      Author: fink
 */

#include "Pins.h"
#include "esp32-hal-gpio.h"

Pins pins;

constexpr std::uint8_t Pins::map_input[INPUT_PIN_COUNT];
constexpr std::uint8_t Pins::map_data[DATA_PIN_COUNT];
constexpr std::uint8_t Pins::map_output[OUTPUT_PIN_COUNT];

Pins::Pins()
{
}

Pins::~Pins()
{
}

void Pins::Init()
{
  for (std::uint8_t i = 0; i < INPUT_PIN_COUNT; i++)
  {
    pinMode (map_input[i], INPUT_PULLUP);
  }

  // Data pins are initialized by the drivers

  for (std::uint8_t o = 0; o < OUTPUT_PIN_COUNT; o++)
  {
    pinMode (map_output[o], OUTPUT);
    digitalWrite (o, LOW);
  }
}

void Pins::Set_Output(std::uint8_t output, bool on)
{
  if (output < OUTPUT_PIN_COUNT)
  {
    digitalWrite (map_output[output], on);
  }
}

std::uint8_t Pins::Read_Inputs(void)
{
  std::uint8_t result = 0;
  for (std::uint8_t i = 0; i < INPUT_PIN_COUNT; i++)
  {
    result |= digitalRead(map_input[i]) << i;
  }
  return result;
}

std::uint8_t Pins::Get_Mapped_Data_Pin(std::uint8_t pin)
{
  if (pin < DATA_PIN_COUNT)
  {
    return map_data[pin];
  }
  else
  {
    return 99;
  }
}

