#include <Arduino.h>
#include <pgmspace.h>
#include <EEPROM.h>
#include <FS.h>
#include <WiFi.h>
#include <ArduinoJson.h>
#include "BrewManiacWeb.h"
#include "automation.h"
#include "Buttons.h"
#include "Temperature.h"
#include "wi.h"
#include "debug.h"
#include "Settings.h"

String _jsonDisconnected = "{\"code\":-1,\"result\":\"BM disc.\"}";

extern FS &FileSystem;
extern float gSettingTemperature;
extern float gBoilStageTemperature;

extern byte gBoilHeatOutput;
extern bool gIsEnterPwm;
extern bool gIsTemperatureReached;

extern bool uiIsTimerRunning(void);
extern bool uiIsTimerRunningUp(void);
extern int uiGetDisplayTime(void);
extern char _lcdBuffer[4][21];

extern void wiSetDeviceAddress(byte ip[ ], bool apmode);
extern void wiUpdateSetting(int address, byte value);

/* from BM */
void BrewManiacWeb::holdStatusUpdate(void)
{
  _holdingStatusUpdate = true;
}

void BrewManiacWeb::unHoldStatusUpdate(bool update)
{
  _holdingStatusUpdate = false;
  if (update)
  {
    if (_eventHandler)
      _eventHandler(this, BmwEventStatusUpdate);
  }
}

void BrewManiacWeb::statusChange(void)
{
  if (_holdingStatusUpdate)
    return;
  if (_eventHandler)
    _eventHandler(this, BmwEventStatusUpdate);
}

void BrewManiacWeb::setButtonLabel(byte btns)
{
  _buttonLabel = btns;
  // the result is the same as BmwEventStatusUpdate
  if (_holdingStatusUpdate)
    return;
  if (_eventHandler)
    _eventHandler(this, BmwEventButtonLabel);
}
void BrewManiacWeb::setBrewStage(uint8_t stage)
{
  _stage = stage;
  if (_holdingStatusUpdate)
    return;
  if (_eventHandler)
    _eventHandler(this, BmwEventStatusUpdate);
}

void BrewManiacWeb::brewEvent(uint8_t event)
{
  _lastEvent = event;
  if (_eventHandler)
    _eventHandler(this, BmwEventBrewEvent);
}

void BrewManiacWeb::updateSettingTemperature(void)
{
  if (_eventHandler)
    _eventHandler(this, BmwEventSettingTemperatureChanged);
}
void BrewManiacWeb::updatePwm(uint8_t pwm)
{
  _pwm = pwm;
  if (_eventHandler)
    _eventHandler(this, BmwEventPwmChanged);
}

void BrewManiacWeb::settingChanged(int address, byte value)
{
  if (_eventHandler)
    _eventHandler(this, BmwEventSettingChanged);
}

void BrewManiacWeb::automationChanged(void)
{
  if (_eventHandler)
    _eventHandler(this, BmwEventAutomationChanged);
}

/* end of from BM */

void BrewManiacWeb::scanSensors(void)
{
  wiStartSensorScan();
}

void BrewManiacWeb::updateSensorSetting(String &json)
{
}

void BrewManiacWeb::scanSensorDone(void)
{
  if (_eventHandler)
    _eventHandler(this, BmwEventSettingChanged);
}

void BrewManiacWeb::setIp(uint8_t ip[ ], bool apmode)
{
  wiSetDeviceAddress(ip, apmode);
}

BrewManiacWeb::BrewManiacWeb(void)
{
  _reportPeriod = DEFAULT_REPORT_PERIOD;
  _lastReportTime = 0;

  _eventHandler = NULL;
  _holdingStatusUpdate = false;
}
void BrewManiacWeb::onEvent(BmwEventHandler handler)
{
  _eventHandler = handler;
}

float BrewManiacWeb::Temperature(void)
{
  return temperature.temperatures[Temperature::TEMP_BREW];
}

float* BrewManiacWeb::temperatures(void)
{
  return temperature.temperatures;
}
byte BrewManiacWeb::sensorNumber(void)
{
  return sensors.gSensorNumber;
}

bool BrewManiacWeb::isBrewing(void)
{
  return (_stage <= 10   // auto mode
          || _stage == 100   // manual mode
          || _stage == 103);  // pid auto tune
}

byte BrewManiacWeb::brewingStage(void)
{
  return _stage;
}

byte BrewManiacWeb::lastBrewEvent(void)
{
  return _lastEvent;
}

void BrewManiacWeb::getSettings(String &json)
{

  //json = "{\"code\":0,\"result\":\"OK\", \"data\":{";
  json = "{";
  bool comma = false;
  for (int i = 0; i < Settings::DB_COUNT; i++)
  {
    char str[settings.max_name_length] = {0};
    settings.GetWebPar(static_cast<Settings::db_t>(i), str);
    if (str[0] == 0)
    {
      if (!comma)
      {
        comma = true;  // don't append comma before the first object
      }
      else
      {
        json += ",";
      }
      json += "\"" + String(str) + "\":" + String(settings.Get(static_cast<Settings::db_t>(i)));
    }
  }

  char buff[20];
  byte numSensor = sensors.gSensorNumber;

  for (byte i = 0; i < numSensor; i++)
  {
    json += ",\"s_cal_" + String(i + 1) + "\":" + wiReadCalibrationOfSensor(i);
  }

  json += ",\"sensors\":[";

  SensorAddressType *address = sensors.sensor_addresses;
  for (byte i = 0; i < numSensor; i++)
  {
    sensors.printSensorAddress(buff, address[i]);
    if (i > 0)
      json += ",";
    json += "\"0x" + String(buff) + "\"";
  }

  json += "]";
  byte primary[NumberSensorStage], auxiliary[NumberSensorStage];

  for (byte i = 0; i < NumberSensorStage; i++)
  {
    byte p = wiReadPrimarySensor(i);
    byte a = wiReadAuxSensor(i);
    ;
    primary[i] = (p >= sensors.gSensorNumber) ? 0 : p;
    auxiliary[i] = (a >= sensors.gSensorNumber) ? 0 : a;
  }

  json += ",\"primary\":[";
  byte p = 0;
  for (byte i = 0; i < NumberSensorStage; i++)
  {
    if (i > 0)
      buff[p++] = ',';
    buff[p++] = '0' + primary[i];
  }
  buff[p] = 0;
  json += String(buff);
  json += "]";

  json += ",\"auxiliary\":[";
  p = 0;
  for (byte i = 0; i < NumberSensorStage; i++)
  {
    if (i > 0)
      buff[p++] = ',';
    buff[p++] = '0' + auxiliary[i];
  }
  buff[p] = 0;
  json += String(buff);

  json += "]";

  json += "}";
}

void BrewManiacWeb::getAutomation(String &json)
{
//    json = "{\"code\":0,\"result\":\"OK\", \"data\":";
//    json += automation.json();
//	json += "}";
  json = automation.json();
}
#define HEXCode(a) (((a) < 10)? ('0'+(a)):('A'-10+(a)))

void BrewManiacWeb::getCurrentStatus(String &json, bool initial)
{
  DynamicJsonDocument root(1024);

  root["state"] = _stage;
  root["btn"] = _buttonLabel;
  root["pump"] = _pumpStatus;
  root["heat"] = _heaterStatus;

  root["spgw"] = _auxHeaterStatus;

  JsonArray temps = root.createNestedArray("temps");
  for (byte i = 0; i < Temperature::TEMP_COUNT; i++)
  {
    temps.add(temperature.temperatures[i]);
  }
  root["tr"] = gIsTemperatureReached ? 1 : 0;
  root["pwmon"] = gIsEnterPwm ? 1 : 0;

  if (gIsEnterPwm)
  {
    root["pwm"] = _pwm;
  }
  root["paused"] = (!uiIsTimerRunning()) ? 1 : 0;
  root["counting"] = (uiIsTimerRunningUp()) ? 1 : 0;
  root["timer"] = uiGetDisplayTime();
  root["stemp"] = gSettingTemperature;
  root["rssi"] = WiFi.RSSI();
// lcd.
  JsonArray lcds = root.createNestedArray("lcd");
  char lcdcol[4][41];
  int idx = 0;
  for (int i = 0; i < 4; i++)
  {
    idx = 0;
    for (int j = 0; j < 20; j++)
    {
      uint8_t ch = _lcdBuffer[i][j];
      lcdcol[i][idx++] = HEXCode(ch >> 4);
      lcdcol[i][idx++] = HEXCode(ch & 0xF);
    }
    lcdcol[i][40] = '\0';
    lcds.add(lcdcol[i]);
  }
  serializeJson(root, json);
}

void BrewManiacWeb::loop(void)
{
  // the loop function will handle retransmission.
  // periodically report status
  unsigned long now = millis();
  if ((now - _lastReportTime) > _reportPeriod)
  {
    _lastReportTime = now;
    statusChange();
  }
}

void BrewManiacWeb::getLastEvent(String &json)
{
  json = "{\"event\":" + String(_lastEvent) + "}";
}

void BrewManiacWeb::getSettingPwm(String &json)
{
  json = "{\"pwm\":" + String(_pwm) + "}";

}

void BrewManiacWeb::getSettingTemperature(String &json)
{
  json = "{\"stemp\":" + String(gSettingTemperature) + "}";
}

byte hex2Int(char hex)
{
  if (hex >= 'A' && hex <= 'F')
  {
    return 10 + hex - 'A';
  }
  else if (hex >= 'a' && hex <= 'f')
  {
    return 10 + hex - 'a';
  }
  else
  {
    return hex - '0';
  }
}

bool BrewManiacWeb::updateSettings(String &json)
{
  DynamicJsonDocument root(2048);  // ArduinoJson assistant suggest 14xx at least.
  auto error = deserializeJson(root, json);
  if (error)
  {
    DBG_PRINTF("wrong JSON string\n");

    return false;
  }

  for (int i = 0; i < Settings::DB_COUNT; i++)
  {
    char str[settings.max_name_length] = {0};
    settings.GetWebPar(static_cast<Settings::db_t>(i), str);
    if (str[0] == 0 && root.containsKey(str))
    {
      byte value = root[str].as<byte>();
      wiUpdateSetting(i, value);

      DBG_PRINTF("update %s %d to %d\n", str, i, value);
    }
  }
  for (int i = 1; i <= MaximumNumberOfSensors; i++)
  {
    String cal = "s_cal_" + String(i);
    if (root.containsKey(cal))
    {
      byte value = root[cal].as<byte>();
      wiUpdateCalibrationOfSensor(i - 1, value);
    }
  }
  // sensor setup
  //"sensors":["0x0011223344556687","0x2211223344556687","0x3311223344556687"],
  //"primary":[0,1,1,1,1,1],"auxiliary":[1,0,0,0,2,2]
  if (root.containsKey("sensors") && root.containsKey("primary") && root.containsKey("auxiliary"))
  {

    JsonArray _sensors = root["sensors"];

    int idx = 0;
    byte *address;
    for (JsonArray::iterator it = _sensors.begin(); it != _sensors.end(); ++it)
    {
      address = sensors.sensor_addresses[idx];

      const char *addressString = it->as<const char*>();
      // convert from stiring

      for (int i = 0; i < 8; i++)
      {
        char hh = addressString[2 + i * 2];
        char ll = addressString[2 + i * 2 + 1];

        address[i] = (hex2Int(hh) << 4) + hex2Int(ll);
      }
      sensors.saveSensor(idx, address);

      DBG_PRINTF("update sensor %d -", idx);
#if SerialDebug
      char buff[20];
      sensors.printSensorAddress(buff, address);
      DBG_PRINTF("%s\n", buff);
#endif

      idx++;
    }
    sensors.gSensorNumber = idx;
    // clear the next address
    if (sensors.gSensorNumber < MaximumNumberOfSensors)
    {
      address = sensors.sensor_addresses[idx];
      address[0] = 0xFF;
      sensors.saveSensor(idx, address);
    }
    // primary
    idx = 0;
    JsonArray primary = root["primary"];
    for (JsonArray::iterator it = primary.begin(); it != primary.end(); ++it)
    {
      uint8_t sensor = it->as<unsigned char>();
      wiUpdatePrimarySensor(idx, sensor);
      DBG_PRINTF("Primary sensor %d - %d\n", idx, sensor);
    }
    // primary
    idx = 0;
    JsonArray auxliary = root["auxiliary"];
    for (JsonArray::iterator it = auxliary.begin(); it != auxliary.end(); ++it)
    {
      uint8_t sensor = it->as<unsigned char>();
      wiUpdateAuxSensor(idx, sensor);
      DBG_PRINTF("Auxiliary sensor %d - %d\n", idx, sensor);
    }

  }

  if (_eventHandler)
    _eventHandler(this, BmwEventSettingChanged);

  return true;
}

bool BrewManiacWeb::updateAutomation(String &json)
{
  File f = FileSystem.open(AUTOMATION_FILE, "w+");
  if (f)
  {
    /*size_t len=*/f.print(json.c_str());
    f.close();
  }
  else
  {
    return false;
  }

  // reload automation
  automation.load();
  if (_eventHandler)
    _eventHandler(this, BmwEventAutomationChanged);

  return true;
}

void BrewManiacWeb::sendButton(byte mask, bool longPressed)
{
  buttons.VirtualButtonPress(mask, longPressed);
}
