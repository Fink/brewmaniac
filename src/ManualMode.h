/*
 * ManualMode.h
 *
 *  Created on: Aug 6, 2020
 *      Author: fink
 */

#ifndef SRC_MANUALMODE_H_
#define SRC_MANUALMODE_H_

#include <cstdint>
#include "ui_globals.h"

class ManualMode
{
  public:
    enum e_state
    {
      MS_AskWater,
      MS_ManualMode,
      MS_AskAutoTune,
      MS_RunningAutoTune,
      MS_AskExitingAutoTune,
    };

    ManualMode();
    virtual ~ManualMode();

    void Init();
    bool EventHandler(std::uint8_t event, std::uint8_t mask);

  private:
    e_state _manual_state;
    std::uint32_t _changeCountDownTime;


    bool Handle_Ask_Water(std::uint8_t event, std::uint8_t mask);
    bool Handle_ManualMode(std::uint8_t event, std::uint8_t mask);
    bool Handle_Ask_AutoTune(std::uint8_t event, std::uint8_t mask);
    bool Handle_RunningAutoTune(std::uint8_t event, std::uint8_t mask);
    bool Handle_Ask_ExitAutoTune(std::uint8_t event, std::uint8_t mask);

    void Enter_ManualMode(void);
    void finishAutoTuneBackToManual(void);
    void ChangeCountdownTime(e_button_mask mask);
};

extern ManualMode manualMode;

#endif /* SRC_MANUALMODE_H_ */
