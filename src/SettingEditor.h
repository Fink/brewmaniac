/*
 * SettingEditor.h
 *
 *  Created on: Aug 4, 2020
 *      Author: fink
 */

#ifndef SETTINGEDITOR_H_
#define SETTINGEDITOR_H_

#include <cstdint>
#include "ui_globals.h"
#include "Settings.h"

class SettingEditor
{
  public:
    typedef struct _SettingItem
    {
        void (*display)(int);
        Settings::db_t address;
    } SettingItem;

    SettingEditor(void);
    void setup(const SettingItem *items, int (*getter)(int GetCurrentIndex), void (*setter)(int GetCurrentIndex, int value));
    void setup(const SettingItem *items);
    void getItemDetail(void);
    void nextItem(void);
    void nextItem(std::uint8_t max, std::uint8_t min);
    void displayItem(void);
    void displayItem(std::uint8_t max, std::uint8_t min);
    std::uint8_t GetCurrentIndex(void);
    void setIndex(std::uint8_t index);
    bool buttonHandler(e_button_mask mask);
    void editItemChange(int change);
    int editItemValue(void);
    void editItem(const char *label, int value, int max, int min, void (*displayFunc)(int));
    void editItemTitleAppendNumber(std::uint8_t num);
    void changeSettingValue(int address, std::uint8_t value);

  private:
  protected:
    std::uint8_t _currentSettingAddress;
    std::uint8_t _currentSettingIndex;
    const SettingItem *_SettingItems;
    int _editingValue;
    int _maxValue;
    int _minValue;
    void (*_displayFunc)(int);

    SettingItem _item;
    char _item_title[Settings::max_name_length];
    int _itemValue;

    int (*_getValue)(int GetCurrentIndex);
    void (*_setValue)(int GetCurrentIndex, int value);


};

extern SettingEditor settingEditor;

#endif /* SETTINGEDITOR_H_ */
