/*
 * Buttons.cpp
 *
 *  Created on: Jul 28, 2020
 *      Author: fink
 */

#include "Buttons.h"
#include "config.h"
#include "ui_globals.h"
#include "Pins.h"
#include "buzz.h"
#include "Timers.h"
#include "debug.h"

Buttons buttons;

Buttons::Buttons()
{
}

Buttons::~Buttons()
{
}

void Buttons::Init()
{
	_test_button_status = ButtonMaskNone;
	_button_change_time = 0;
	_continuous_pressed_dected_time = 0;
	_continuous_pressed_detected = false;
	_long_pressed = 0;
	_button_pressed = ButtonMaskNone;
	_oneFigerUp = 0;
}

void Buttons::VirtualButtonPress(std::uint8_t mask, bool longPressed)
{
	_button_pressed = static_cast<e_button_mask>(mask);
	if (longPressed)
	{
	  _button_pressed |= ButtonMaskLongPress;
	}
	_virtual_button_pressed = true;
	DBG_PRINTF("virutal key:%d\r\n", mask);
}

e_button_mask Buttons::ReadButtons()
{
	if (_virtual_button_pressed)
	{
		_virtual_button_pressed = false;
	  DBG_PRINTF("_virtual_button_pressed\r\n");
		return static_cast<e_button_mask>(_button_pressed);
	}

//	std::uint8_t buttons = static_cast<std::uint8_t>(ButtonMaskNone);
//
//	std::uint8_t button_pins = pins.Read_Inputs();
//
//	if (button_pins == Pins::ButtonUpPin)
//	{
//		buttons |= ButtonMaskUp;
//	}
//	if (button_pins == Pins::ButtonDownPin)
//	{
//		buttons |= ButtonMaskDown;
//	}
//	if (button_pins == Pins::ButtonEnterPin)
//	{
//		buttons |= ButtonMaskEnter;
//	}
//	if (button_pins == Pins::ButtonStartPin)
//	{
//		buttons |= ButtonMaskStart;
//	}
//
//	if (buttons == ButtonMaskNone)
//	{
//		if (_test_button_status == 0)
//		{
//			return ButtonMaskNone;
//		}
//
//		buzzMute(); // mute anything after button depressed.
//					// even if button feedback is off. the alert for
//					// temperature reached for doughing-in will be muted.
//
//		std::uint32_t duration = timers.current_time_in_ms - _button_change_time;
//
//#if BUTTON_DEBUG == true
//    	Serial.print(F("pressed:"));
//    	Serial.print(_test_button_status);
//    	Serial.print(F(","));
//    	Serial.print(buttons);
//    	Serial.print(F("for:"));
//    	Serial.println(duration);
//   		#endif
//
//		if (duration > ButtonPressedDetectMinTime)
//		{
//			if (duration > ButtonLongPressedDetectMinTime)
//			{
//				_long_pressed = true;
//				buttons |= ButtonMaskLongPress;
//			}
//			else
//			{
//				_long_pressed = false;
//			}
//			_button_pressed = _test_button_status;
//
//			_test_button_status = ButtonMaskNone;
//			_continuous_pressed_detected = false;
//
//#if BUTTON_DEBUG == true
//  		Serial.print(_button_pressed);
//  		if (_long_pressed) Serial.println(F(" -Long Pressed"));
//    	else Serial.println(F(" -Pressed"));
//   		#endif
//
//			return static_cast<e_button_mask>(buttons);
//		}
//
//#if BUTTON_DEBUG == true
//    	Serial.println(F("Not Pressed"));
//   		#endif
//
//		_test_button_status = ButtonMaskNone;
//		_continuous_pressed_detected = false;
//
//		return ButtonMaskNone;
//	}
//
//	// buttons is not ZERO afterward
//	if (buttons == _test_button_status) // pressing persists
//	{
//		if (_continuous_pressed_detected)
//		{
//			//if duration exceeds a trigger point
//			if ((timers.current_time_in_ms - _continuous_pressed_dected_time)
//					> ButtonContinuousPressedTrigerTime)
//			{
//				_continuous_pressed_dected_time = timers.current_time_in_ms;
//
//#if BUTTON_DEBUG == true
//			  	Serial.print(_button_pressed);
//    			Serial.print(F(" -Continues 2 pressed:"));
//    			Serial.println(current_time_in_ms);
//   				#endif
//
//    			return static_cast<e_button_mask>(buttons);
//			}
//		}
//		else
//		{
//			std::uint32_t duration = timers.current_time_in_ms - _button_change_time;
//
//			if (readSetting(PS_ButtonFeedback))
//			{
//				if (duration > ButtonPressedDetectMinTime)
//				{
//					buzzOn();
//				}
//				if (duration > ButtonLongPressedDetectMinTime)
//				{
//					buzzMute();
//				}
//			}
//
//			if (duration > ButtonContinuousPressedDetectMinTime)
//			{
//				_continuous_pressed_detected = true;
//				_continuous_pressed_dected_time = timers.current_time_in_ms;
//				// fir the first event
//				_button_pressed = buttons << 4; // user upper 4bits for long pressed
//				buttons |= ButtonMaskContinious;
//
//#if BUTTON_DEBUG == true
//			  	Serial.print(_button_pressed);
//    			Serial.print(F(" -Continues detected pressed:"));
//    			Serial.println(current_time_in_ms);
//   				#endif
//
//				return static_cast<e_button_mask>(buttons);
//			}
//		}
//	}
//	else // if(buttons == _testButtunStatus)
//	{
//		// for TWO buttons event, it is very hard to press and depress
//		// two buttons at exactly the same time.
//		// so if new status is contains in OLD status.
//		// it might be the short period when two fingers are leaving, but one is detected
//		// first before the other
//		// the case might be like  01/10 -> 11 -> 01/10
//		//  just handle the depressing case: 11-> 01/10
//		if ((_test_button_status & buttons) && (_test_button_status > buttons))
//		{
//			if (_oneFigerUp == 0)
//			{
//				_oneFigerUp = timers.current_time_in_ms;
//				// skip this time
//				return ButtonMaskNone;
//			}
//			else
//			{
//				// one fat finger is dected
//				if ((timers.current_time_in_ms - _oneFigerUp) < ButtonFatFingerTolerance)
//				{
//					return ButtonMaskNone;
//				}
//			}
//
//#if BUTTON_DEBUG == true
//    	Serial.println(F("Failed fatfinger"));
//   		#endif
//
//		}
//		// first detect, note time to check if presist for a duration.
//		_test_button_status = static_cast<e_button_mask>(buttons);
//		_button_change_time = timers.current_time_in_ms;
//		_oneFigerUp = 0;
//
//#if BUTTON_DEBUG == true
//		Serial.print(buttons);
//    	Serial.println(F(" -Attempted"));
//   		#endif
//
//	}

	return ButtonMaskNone;
}

