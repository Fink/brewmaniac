/*
 * uiTempTime.cpp
 *
 *  Created on: Jul 29, 2020
 *      Author: fink
 */

#include "uiTempTime.h"
#include "lcd_interface.h"
#include "Temperature.h"

float _lastShownTemperature;
float _lastShownAuxTemperature;

const byte TemperatureDisplayPos[] PROGMEM=
{
  2,1,  // Idle/Main screen
  1,1,  // Auto mode & Manual mode
  3,0   // Paused mode
};

const byte AuxTemperatureDisplayPos[] PROGMEM=
{
  10,1,  // Idle/Main screen
  1,2,  // Auto mode & Manual mode
  10,0   // Paused mode
};


const byte RunningTimeDisplayPosition[] PROGMEM=
{
  11,2,
  6,2,
};

#define RunningTimeNormalPosition 0
#define RunningTimePausePosition 1
#define RunningTimeDelayInputPosition 1


byte _uiTpDisplayRow;
byte _uiTpDisplayCol;
boolean _uiDisplayTemperature;

byte _uiAuxTpDisplayRow;
byte _uiAuxTpDisplayCol;

byte _countingTimeDirection;
unsigned long _countingTimeRef;
unsigned long _countingTimeDisplay;
byte _runningTimeRow;
byte _runningTimeCol;

boolean _runningTimeHidden;

boolean _runningTimeBlinking;
boolean _runningTimeBlinkShow;

unsigned long _runningTimeBlinkTime;


int uiGetDisplayTime(void){
  return (int)_countingTimeDisplay;
}

bool uiIsTimerRunning(void)
{
  return _countingTimeDirection != COUNTING_PAUSE;
}

bool uiIsTimerRunningUp(void)
{
  return _countingTimeDirection == COUNTING_UP;
}


void uiTempDisplayHide(void)
{
  _uiDisplayTemperature=false;
}


void uiTempDisplaySetPosition(byte index)
{
    _uiDisplayTemperature=true;
  _lastShownTemperature=INVALID_TEMP_C; // for redraw

   _uiTpDisplayRow=pgm_read_byte_near(& TemperatureDisplayPos[index*2 +1]);
   _uiTpDisplayCol=pgm_read_byte_near(& TemperatureDisplayPos[index*2]);

  _lastShownAuxTemperature=INVALID_TEMP_C; // for redraw

   _uiAuxTpDisplayRow=pgm_read_byte_near(& AuxTemperatureDisplayPos[index*2 +1]);
   _uiAuxTpDisplayCol=pgm_read_byte_near(& AuxTemperatureDisplayPos[index*2]);
}

//****************************
// running time

void uiRunningTimeStop(void)
{
  _countingTimeDirection = COUNTING_PAUSE;
}

void uiRunningTimePrint(unsigned long timeInSeconds)
{
  char buffer[10]; // using 10 bytes wan't too bad.
  unsigned long hour = timeInSeconds / (60*60);
  int dec=(hour/10);

  buffer[0]= (char) (dec<10)?(dec + '0'):(dec + 'A');
  buffer[1]= (char)((hour%10) + '0');
  buffer[2]=':';

  unsigned long minute =(timeInSeconds - hour * 60*60)/60;

  buffer[3]= (char)((minute/10) + '0');
  buffer[4]= (char)((minute%10) + '0');
  buffer[5]=':';

  unsigned long seconds=timeInSeconds - hour*60*60 - minute*60;

  buffer[6]=(char)((seconds/10) + '0');
  buffer[7]=(char)((seconds%10) + '0');
  buffer[8]=0;

  uiLcdPrint(_runningTimeCol,_runningTimeRow,buffer);
}

void uiRunningTimeHide(boolean hide)
{
  _runningTimeHidden = hide;
  if(! hide)
  {
    uiRunningTimePrint(_countingTimeDisplay);
  }
}

void uiRunningTimeSetPosition(byte pos)
{
   _runningTimeRow=pgm_read_byte_near(& RunningTimeDisplayPosition[pos*2 +1]);
   _runningTimeCol=pgm_read_byte_near(& RunningTimeDisplayPosition[pos*2]);
}

void uiRunningTimeShowInitial(unsigned long initialValue)
{
  _countingTimeDirection = COUNTING_PAUSE;
  _countingTimeDisplay = initialValue;
  _runningTimeHidden= false;
  uiRunningTimePrint(initialValue);
}

void uiRunningTimeStart(void)
{
  // use reference to note time.
  _countingTimeRef=timers.current_time_in_secs;
  _countingTimeDirection = COUNTING_UP;
}

void uiRunningTimeStartFrom(unsigned long start)
{
  // use reference to note time.
  _countingTimeRef=timers.current_time_in_secs - start;
  _countingTimeDirection = COUNTING_UP;
}


void uiRunningTimeStartCountDown(unsigned long seconds)
{
  _countingTimeRef=timers.current_time_in_secs + seconds;
  _countingTimeDirection = COUNTING_DOWN;
}


void uiRunningTimeBlank(void)
{
  uiLcdClear(_runningTimeCol,_runningTimeRow,8);
}

void uiRunningTimeBlink(boolean blink)
{
  if(blink && !_runningTimeBlinking)
  {
    _runningTimeBlinkTime = timers.current_time_in_ms;
  }
  _runningTimeBlinking = blink;

  if(! _runningTimeBlinking && ! _runningTimeBlinkShow)
    uiRunningTimePrint(_countingTimeDisplay);
}

void uiPrintTemperature(byte col, byte row, float displayTemp)
{
  char buffer[8];
  byte indent;

    if(IS_TEMP_INVALID(displayTemp))
    {
        indent = 2;
        for(byte i=0;i<4;i++) buffer[i]='-';
        buffer[4]='\0';
    }
    else
    {
        byte digitNum=sprintFloat(buffer,displayTemp,2);
    buffer[digitNum]='\0';
        indent = 6 - digitNum;
  }
  uiLcdClear(col,row,6);
  uiLcdPrint(col + indent, row,buffer);
}

void uiDisplayTemperatureAndRunningTime(void)
{
    if(_uiDisplayTemperature && (
    (_lastShownTemperature != temperature.temperatures[Temperature::TEMP_BREW])
  || (_lastShownAuxTemperature != temperature.temperatures[Temperature::TEMP_SPARGE])
    ))
    {
      _lastShownTemperature = temperature.temperatures[Temperature::TEMP_BREW];
      _lastShownAuxTemperature = temperature.temperatures[Temperature::TEMP_SPARGE];

      uiPrintTemperature(_uiTpDisplayCol,_uiTpDisplayRow,temperature.temperatures[Temperature::TEMP_BREW]);
    uiLcdDrawSymbol(_uiTpDisplayCol +6,_uiTpDisplayRow,LcdCharDegree);
      uiPrintTemperature(_uiAuxTpDisplayCol,_uiAuxTpDisplayRow,temperature.temperatures[Temperature::TEMP_SPARGE]);
    uiLcdDrawSymbol(_uiAuxTpDisplayCol +6,_uiTpDisplayRow,LcdCharDegree);

    }
      if(_runningTimeBlinking)
      {
        if(_runningTimeBlinkShow)
        {
          if((timers.current_time_in_ms - _runningTimeBlinkTime) > RunningTimeShowCycle)
          {
            _runningTimeBlinkTime=timers.current_time_in_ms;
            _runningTimeBlinkShow = false;
            //hide
            uiRunningTimeBlank();
          }
        }
        else
        {
          if((timers.current_time_in_ms - _runningTimeBlinkTime) > RunningTimeHideCycle)
          {
            _runningTimeBlinkTime=timers.current_time_in_ms;
            _runningTimeBlinkShow = true;
            // shown
              uiRunningTimePrint(_countingTimeDisplay);
          }
        }

      }

    if(_countingTimeDirection != COUNTING_PAUSE)
    {
      unsigned long count;
      if (_countingTimeDirection == COUNTING_UP)
      {
        count=timers.current_time_in_secs - _countingTimeRef;
      }
      else
      {
        // counting down
        count=_countingTimeRef - timers.current_time_in_secs;
        if(count > 86400000L)
        {
          count =0;
          _countingTimeDirection = COUNTING_PAUSE;
        }
      }

    if(! _runningTimeHidden)
    {
      if(count != _countingTimeDisplay)
        {
          _countingTimeDisplay=count;
          uiRunningTimePrint(_countingTimeDisplay);
        }

      }
    }
}

