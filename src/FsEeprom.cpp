#include <FS.h>
#include "FsEeprom.h"
#include "debug.h"

#define EEPROM_FILENAME "/eeprom.bin"

extern FS &FileSystem;

FsEEPROMClass FsEEPROM;

char* FsEEPROMClass::begin(int size)
{
  _eeprom = (char*) malloc(size);

  _size = size;

  if (FileSystem.exists(EEPROM_FILENAME))
  {
    File f = FileSystem.open(EEPROM_FILENAME, "r+");
    if (f)
    {
      DBG_PRINTF("read EEPROM from file\r\n");
      f.readBytes(_eeprom, _size);
      f.close();
    }
  }
  else
  {
    DBG_PRINTF("EEPROM file not exit\r\n");
    commit();
  }

  return _eeprom;
}

void FsEEPROMClass::commit(void)
{
  File f = FileSystem.open(EEPROM_FILENAME, "w+");
  if (f)
  {
    std::uint32_t wb = f.write((const uint8_t*) _eeprom, _size);
    f.close();
    DBG_PRINTF("Wrote %d bytes\r\n", wb);
  }
}

std::uint8_t FsEEPROMClass::read(int address)
{
  if (address > _size)
    return 0;
  return (std::uint8_t) _eeprom[address];
}

bool FsEEPROMClass::write(int address, std::uint8_t value)
{
  if (address > _size)
    return false;
  if (_eeprom[address] == value)
    return false;
  _eeprom[address] = value;
  return true;
}
