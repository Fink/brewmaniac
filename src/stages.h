/*
 * stages.h
 *
 *  Created on: Aug 3, 2020
 *      Author: fink
 */

#ifndef SRC_STAGES_H_
#define SRC_STAGES_H_


//Stage
#define StageDoughIn      0
// 1 -6 rest,
// 7 mashout
#define MashStepMashOut 7
#define StageBoil         8
#define StageCooling    9
#define StageWhirlpool     10

#define StageHopStandChill 11
#define StageHopStand      12

#define StageDelayStart   99
#define StageManualMode  100
#define StageIdleScreen  101
#define StageSetting   102
#define StagePIDAutoTune 103

#define StageDistillingPreHeat 110
#define StageDistillingHead 111
#define StageDistillingHeart 112
#define StageDistillingTail 113


#endif /* SRC_STAGES_H_ */
