
#ifndef UI_TEMPTIME
#define UI_TEMPTIME

#include "Timers.h"
#include "mystrlib.h"
#include <Arduino.h>

//********************************************************
//* for update temperature and time on screen
//********************************************************
#define COUNTING_PAUSE 0
#define COUNTING_UP 1
#define COUNTING_DOWN 2

#define TemperatureMainScreenPosition 0
#define TemperatureManualModePosition 1
#define TemperaturePausePosition 2
#define TemperatureAutoModePosition TemperatureManualModePosition

#define RunningTimeShowCycle 700
#define RunningTimeHideCycle 300

extern float _lastShownTemperature;
extern float _lastShownAuxTemperature;

extern const byte TemperatureDisplayPos[] PROGMEM;
extern const byte AuxTemperatureDisplayPos[] PROGMEM;

extern const byte RunningTimeDisplayPosition[] PROGMEM;

#define RunningTimeNormalPosition 0
#define RunningTimePausePosition 1
#define RunningTimeDelayInputPosition 1


extern byte _uiTpDisplayRow;
extern byte _uiTpDisplayCol;
extern boolean _uiDisplayTemperature;

extern byte _uiAuxTpDisplayRow;
extern byte _uiAuxTpDisplayCol;

extern byte _countingTimeDirection;
extern unsigned long _countingTimeRef;
extern unsigned long _countingTimeDisplay;
extern byte _runningTimeRow;
extern byte _runningTimeCol;

extern boolean _runningTimeHidden;

extern boolean _runningTimeBlinking;
extern boolean _runningTimeBlinkShow;

extern unsigned long _runningTimeBlinkTime;


int uiGetDisplayTime(void);
bool uiIsTimerRunning(void);
bool uiIsTimerRunningUp(void);
void uiTempDisplayHide(void);
void uiTempDisplaySetPosition(byte index);
//****************************
// running time

void uiRunningTimeStop(void);
void uiRunningTimePrint(unsigned long timeInSeconds);
void uiRunningTimeHide(boolean hide);
void uiRunningTimeSetPosition(byte pos);
void uiRunningTimeShowInitial(unsigned long initialValue);
void uiRunningTimeStart(void);
void uiRunningTimeStartFrom(unsigned long start);
void uiRunningTimeStartCountDown(unsigned long seconds);
void uiRunningTimeBlank(void);
void uiRunningTimeBlink(boolean blink);
void uiPrintTemperature(byte col, byte row, float displayTemp);
void uiDisplayTemperatureAndRunningTime(void);

#endif

