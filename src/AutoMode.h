/*
 * AutoMode.h
 *
 *  Created on: Aug 5, 2020
 *      Author: fink
 */

#ifndef SRC_AUTOMODE_H_
#define SRC_AUTOMODE_H_

#include <cstdint>
#include "ui_globals.h"

class AutoMode
{
  public:

    enum _e_state
    {
      AS_AskDelayStart,
      AS_AskResume,
      AS_AskWaterAdded,
      AS_PumpPrime,
      AS_DelayTimeInput,
      AS_DelayTimeConfirm,
      AS_DelayWaiting,
      AS_DoughIn,
      AS_MashInAskContinue,
      AS_AskAddMalt,
      AS_Mashing,
      AS_AskMaltRemove,
      AS_Boiling,
      AS_Whirlpool,
      AS_IodineTest,
      AS_Pause,
      AS_Cooling,
      AS_Finished,
      AS_AskSpargeWaterAdded,
      AS_HopStandChilling,
      AS_HopStand
    };


    AutoMode();
    virtual ~AutoMode();

    void Init();
    bool EventHandler(std::uint8_t event, std::uint8_t mask);

  private:
    static const std::uint32_t _max_delay_time = {12*60/15};
    static const std::uint8_t hop_altering_time_s = {10};
    static const std::uint8_t whirlpool_time_min = {1};
    static const std::uint8_t whirlpool_time_max = {10};
    _e_state _state;
    _e_state _stateBeforePause;
    std::uint32_t _delayTime;
    std::uint32_t _remainingBoilTime;
    std::uint32_t _whirlpoolTime;
    std::uint32_t _savedTime;
    std::uint8_t _mashingStep;
    std::uint8_t _numberMashingStep;
    std::uint8_t _numHopToBeAdded;
    std::uint8_t _hopStandSessionHopIndex;
    std::uint8_t _hopStandSession;
    std::uint8_t _primePumpCount;
    bool _delayRequested;
    bool _stageConfirm;
    bool _coolingTempReached;
    bool _mashingStageExtendEnable;
    bool _mashingStageExtending;
    bool _askingSkipMashingStage;
    bool _recoveryTimer;
    bool _whirlpoolInput;
    bool _pumpRunning;
    bool _savedHeating;
    bool _savedPump;
    bool _isPaused;
    bool _manualPump;

    bool _set_no_delay_start;
    std::uint32_t _set_pump_prime_on_time;
    std::uint32_t _set_pump_prime_off_time;
    std::uint32_t _set_pump_prime_count;
    bool _set_sparge_enabled;
    bool _set_pid_during_doughin;
    bool _set_skip_malt;
    bool _set_pump_premash;
    bool _set_skip_iodine_test;
    bool _set_skip_remove_malt;
    bool _set_pump_on_mash;
    bool _set_pump_on_mashout;
    std::uint32_t _set_iodinetime;
    bool _set_pid_pipe;
    std::uint32_t _set_boil_temp;
    bool _set_pump_during_boil;
    std::uint32_t _set_whirlpool;

    bool Handle_Ask_Resume(std::uint8_t event, std::uint8_t mask);
    bool Handle_Ask_DelayStart(std::uint8_t event, std::uint8_t mask);
    bool Handle_Ask_SpargeWaterAdded(std::uint8_t event, std::uint8_t mask);
    bool Handle_PumpPriming(std::uint8_t event, std::uint8_t mask);
    bool Handle_Ask_WaterAdded(std::uint8_t event, std::uint8_t mask);
    bool Handle_DelayTimerInput(std::uint8_t event, std::uint8_t mask);
    bool Handle_DelayTimerConfirm(std::uint8_t event, std::uint8_t mask);
    bool Handle_DelayWaiting(std::uint8_t event, std::uint8_t mask);
    bool Handle_DoughIn(std::uint8_t event, std::uint8_t mask);
    bool Handle_Pause(std::uint8_t event, std::uint8_t mask);
    bool Handle_Ask_MashInContinue(std::uint8_t event, std::uint8_t mask);
    bool Handle_Ask_AddMalt(std::uint8_t event, std::uint8_t mask);
    bool Handle_Mashing(std::uint8_t event, std::uint8_t mask);
    bool Handle_IodineTest(std::uint8_t event, std::uint8_t mask);
    bool Handle_Ask_MaltRemove(std::uint8_t event, std::uint8_t mask);
    bool Handle_Boiling(std::uint8_t event, std::uint8_t mask);
    bool Handle_HopStandChilling(std::uint8_t event, std::uint8_t mask);
    bool Handle_HopStand(std::uint8_t event, std::uint8_t mask);
    bool Handle_Cooling(std::uint8_t event, std::uint8_t mask);
    bool Handle_Whirlpool(std::uint8_t event, std::uint8_t mask);
    bool Handle_Finished(std::uint8_t event, std::uint8_t mask);

    void Enter_PumpPriming(void);
    void Enter_DelayTimeInput(void);
    void Enter_DoughIn(void);
    void Enter_Cooling(std::uint32_t elapsed);
    void Enter_MashingExtension(void);
    void Enter_Mashing(void);
    void Enter_IodineTest(void);
    void Enter_AskRemoveMalt(void);
    void Enter_Boiling(void);
    void Enter_HopStandChilling(void);
    void Enter_HopStand(std::uint32_t elapsed = 0);

    void ResumeProcess(void);
    void Pause(std::uint32_t time);
    void ExitPause(void);
    void MashingStageFinished(void);
    void CoolingAsk(const char *msg, std::uint8_t stage);
    void BrewEnd(void);
    void ToggleMashExtension(void);
    void ResetMashExtension(void);
    void NextMashingStep(bool resume);
    void Update_numberMashingStep(void);
    void IodineTestToMashExtension(void);
    void IodineTestToMashout(void);
    void ShowHopAdding(void);
    void AddHopNotice(void);
    void ReStartBoilingTimer(void);
    void StartBoilingTimer(void);
    void StartNextHopTimer(void);
    void BoilingPauseHandler(void);
    void CoolingOrWhirlpool(void);
    void StartHopStand(void);
    void ShowPostBoilHop(void);
    void StartHopStandHopTimer(void);
    void EndHopStandSession(void);
    void HopStandTimeout(void);
    void AuxTimeout(void);
    void WhirlpoolInputTime(void);
    void Whirlpool(std::uint32_t elapsed);
    void WhirlpoolFinish(void);
    void CoolingFinish(void);
    std::uint8_t HopStandSessionByTime(std::uint32_t elapsed);
    void StartWithoutPumpPrimming(void);
};

extern AutoMode autoMode;

#endif /* SRC_AUTOMODE_H_ */
