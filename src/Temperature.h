/*
 * Temperature.h
 *
 *  Created on: Jul 29, 2020
 *      Author: fink
 */

#ifndef SRC_TEMPERATURE_H_
#define SRC_TEMPERATURE_H_

#include <cstdint>

class Temperature
{
  public:
    enum
    {
      TEMP_BREW,
      TEMP_SPARGE,
      TEMP_COUNT
    };

    Temperature();
    virtual ~Temperature();

    void Init(bool fake_heat);
    void Service();

    std::uint8_t SensorForBrew;
    std::uint8_t SensorForSparge;
    float temperatures[TEMP_COUNT];
    static constexpr float temperature_adjust_threshold = {2.0};

  private:
    float _filtered_temp[TEMP_COUNT];
    float _fake_heat;
    void SetInitialFilterValue(float value);
    float AddFilterValue(std::uint8_t idx, float value);
};

extern Temperature temperature;

#endif /* SRC_TEMPERATURE_H_ */
