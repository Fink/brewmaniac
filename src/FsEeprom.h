#ifndef FSEEPROM_H
#define FSEEPROM_H

#include <cstdint>

class FsEEPROMClass
{
    char *_eeprom;
    int _size;
  public:
    FsEEPROMClass(void) :
        _size(0)
    {
    }

    char* begin(int size);

    void commit();

    std::uint8_t read(int address);

    bool write(int address, std::uint8_t value);

    char* data(void)
    {
      return _eeprom;
    }
    int size(void)
    {
      return _size;
    }
};

extern FsEEPROMClass FsEEPROM;

#endif
