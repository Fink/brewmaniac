/*
 * ui_globals.h
 *
 *  Created on: Jul 28, 2020
 *      Author: fink
 */

#ifndef SRC_UI_GLOBALS_H_
#define SRC_UI_GLOBALS_H_

#include "resources.h"
#include <cstdint>

#define ButtonPressedEventMask  0x1
#define TemperatureEventMask (0x1 <<1)
#define TimeoutEventMask  (0x1 <<2)
#define Timeout100msEventMask  (0x1 <<2)
#define PumpRestEventMask  (0x1 <<3)

#define WhirlpoolHot 2
#define WhirlpoolCold 1
#define WhirlpoolOff 0

#define RemoteEventTemperatureReached 1
#define RemoteEventAddMalt        2
#define RemoteEventRemoveMalt      3
#define RemoteEventIodineTest     4
#define RemoteEventPause      5
#define RemoteEventResume       6
#define RemoteEventAddHop       7
#define RemoteEventPwmOn      8
#define RemoteEventPwmOff       9
#define RemoteEventBoilFinished   10
#define RemoteEventPumpRest         11
#define RemoteEventPumpRestEnd      12
#define RemoteEventBrewFinished   99

#define ADVANCE_BEEP_TIME 5

typedef enum _HeatingMode
{
  HeatingModePID,
  HeatingModeBoil,
  HeatingModePWM
} HeatingMode;

typedef enum
{
  ButtonMaskNone       = 0x00,
  ButtonMaskUp         = 0x01,
  ButtonMaskDown       = 0x02,
  ButtonMaskStart      = 0x04,
  ButtonMaskEnter      = 0x08,
  ButtonMaskLongPress  = 0x10,
  ButtonMaskContinious = 0x20,
  ButtonMaskLongUp     = ButtonMaskLongPress | ButtonMaskUp,
  ButtonMaskLongDown   = ButtonMaskLongPress | ButtonMaskDown,
  ButtonMaskLongStart  = ButtonMaskLongPress | ButtonMaskStart,
  ButtonMaskLongEnter  = ButtonMaskLongPress | ButtonMaskEnter,
  ButtonMaskUpDown     = ButtonMaskUp | ButtonMaskDown,
  ButtonMaskEnterStart = ButtonMaskStart | ButtonMaskEnter,
  ButtonMaskDownStart  = ButtonMaskStart | ButtonMaskDown,
  ButtonMaskContUp     = ButtonMaskContinious | ButtonMaskUp,
  ButtonMaskContDown   = ButtonMaskContinious | ButtonMaskDown,
  ButtonMaskUpDownStart= ButtonMaskUp | ButtonMaskDown | ButtonMaskStart,
} e_button_mask;



extern byte gBoilHeatOutput;
extern bool gIsEnterPwm;
extern bool isCountDownTimeBlinking;
extern bool gIsTemperatureReached;
extern float gSettingTemperature;
extern bool gIsHeatOn;
extern bool isManualModeCountDownMode;
extern bool gEnableSpargeWaterHeatingControl;
extern float gBoilStageTemperature;
extern bool _isRunningAutoTune;

#define DEFAULT_MANUL_MODE_TEMPERATURE 35

void heatLoadParameters(void);
void setAdjustTemperature(float max, float min);
void heatOff(void);
void heatOn(bool pidmode = true);
void adjustSp(float adjust);
void setSettingTemperature(float temp);
void backToMain(void);
void adjustPwm(int adjust);
void heatProgramOff(void);
void loadBrewParameters(void);
bool processAdjustButtons(e_button_mask mask);
void togglePumpRest(void);
bool togglePwmInput(void);
void stopHeatingSpargeWater(void);
void startHeatingSpargeWater(void);
void startAutoTune(void);
void cancelAutoTune(void);
void Display_SimpleTemperature(int value);
void Display_Percentage(int data);
void SetEventMask(std::uint8_t mask);

#endif /* SRC_UI_GLOBALS_H_ */
