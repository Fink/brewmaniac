#include <time.h>
#include <Arduino.h>
#include <FS.h>
#include "config.h"
#include <ESPPerfectTime.h>
#include "debug.h"

#include "TimeKeeper.h"

#define TIME_SAVE_FILENAME "/time.saved"
#define TIME_SAVING_PERIOD 300

#define RESYNC_TIME 43200000UL
// time gap in seconds from 01.01.1900 (NTP time) to 01.01.1970 (UNIX time)
#define DIFF1900TO1970 2208988800UL

TimeKeeperClass TimeKeeper;

void TimeKeeperClass::setCurrentTime(time_t now)
{
  if (_referenceSeconds == 0)
  {
    _referenceSeconds = now;
    _referenceSystemTime = millis();
    _lastSaved = _referenceSeconds;
  }
}

void TimeKeeperClass::begin(bool useSaved)
{
  pftime::configTzTime(PSTR("GMT-1"), "time.nist.gov");
  if (!useSaved)
  {
    _referenceSystemTime = millis();
    _referenceSeconds = 0;
  }
}

void TimeKeeperClass::begin(const char *server1, const char *server2, const char *server3)
{
  pftime::configTzTime(PSTR("GMT-1"), server1, server2, server3);

  unsigned long secs = 0;

  _referenceSystemTime = millis();
  _referenceSeconds = secs;
  _lastSaved = _referenceSeconds;
}

time_t TimeKeeperClass::getTimeSeconds(void)  // get Epoch time
{
  struct tm *tm = pftime::localtime(nullptr);
  return tm->tm_sec;
}

static char _dateTimeStrBuff[24];

const char* TimeKeeperClass::getDateTimeStr(void)
{
  time_t current = getTimeSeconds();
  tm *t = pftime::localtime(&current);

  //2016-07-01T05:22:33Z
  sprintf(_dateTimeStrBuff, "%d-%02d-%02dT%02d:%02d:%02dZ", t->tm_year + 1900, t->tm_mon + 1, t->tm_mday, t->tm_hour, t->tm_min, t->tm_sec);
  return _dateTimeStrBuff;
}
