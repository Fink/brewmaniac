/*
 * Sensors.cpp
 *
 *  Created on: Aug 9, 2020
 *      Author: fink
 */

#include "Sensors.h"
#include "config.h"
#include "debug.h"
#include "Pins.h"
#include "lcd_interface.h"
#include "Settings.h"
#include <OneWire.h>

Sensors sensors;

OneWire ds(Pins::Data_Sensor);

Sensors::Sensors()
{
}

Sensors::~Sensors()
{
}

void Sensors::Init()
{
  _calibration[0] = (static_cast<float>(settings.Get(Settings::set_SensorCalibration_Brew) - 50) / 10.0);  //TODO
  _calibration[1] = (static_cast<float>(settings.Get(Settings::set_SensorCalibration_Sparge) - 50) / 10.0);  //TODO
}

void Sensors::Service()
{
  for (std::uint8_t si = 0; si < gSensorNumber; si++)
  {
    if (_isConverting[si] == false)
    {
      // start conversion and return
      SensorRequestConvert(sensor_addresses[si]);
      _isConverting[si] = true;
    }
    else
    {
      if (SensorDataReady(sensor_addresses[si]))
      {
        float rawreading = SensorRead(sensor_addresses[si]);
        _isConverting[si] = false;

        if (IS_TEMP_INVALID(rawreading))
        {
          // invalid sensor data.
          DBG_PRINTF("Sensor disconnected (%s)!", sensor_addresses[si]);
          gSensorDisconnected[si] = true;
        }
        else
        {
          gSensorDisconnected[si] = false;
          result[si] = rawreading + _calibration[si];
        }
      }
    }
  }  // for every sensor
}

void Sensors::SensorRequestConvert(std::uint8_t address[ ])
{
  ds.reset();
  ds.select(address);
  ds.write(DSCMD_CONVERT_T, 0);
}

bool Sensors::SensorDataReady(std::uint8_t address[ ])
{
  ds.reset();
  ds.select(address);
  std::uint8_t busy = ds.read_bit();
  return busy != 0;
}

float Sensors::SensorRead(std::uint8_t *address)
{
  std::uint8_t sensorData[9];
  ds.reset();
  ds.select(address);
  // request data
  ds.write(DSCMD_READ_SCRATCHPAD);
  for (std::uint8_t i = 0; i < 9; i++)
  {           // with crc we need 9 bytes
    sensorData[i] = ds.read();
  }
  /* add this routine for crc version */
  if (OneWire::crc8(sensorData, 8) != sensorData[8])
  {  //if checksum fails start a new conversion right away
    return INVALID_TEMP_C;
  }
  // data got!
  unsigned int raw = (sensorData[1] << 8) + sensorData[0];

  sensor_resolution = sensorData[4] & 0x60;

  // at lower res, the low bits are undefined, so let's zero them
  if (sensor_resolution == 0x00)
    raw = raw & ~7;  // 0.5C 9 bit resolution, 93.75 ms
  else if (sensor_resolution == 0x20)
    raw = raw & ~3;  // 0.25C 10 bit res, 187.5 ms
  else if (sensor_resolution == 0x40)
    raw = raw & ~1;  // 0.125C 11 bit res, 375 ms
  // 0x60  0.0625C 12bits, 750ms

  float temperature = (float) raw * 0.0625;
  return temperature;
}

std::uint8_t Sensors::scanSensors(std::uint8_t max, std::uint8_t addresses[ ][8])
{
//  byte i;
  std::uint8_t m = 0;
#if SerialDebug == true
  Serial.print("Looking for 1-Wire devices...\n\r");  // "\n\r" is NewLine
#endif

  while (m < max && ds.search(addresses[m]))
  {
    if (OneWire::crc8(addresses[m], 7) != addresses[m][7])
    {
#if SerialDebug == true
      Serial.print("CRC is not valid!\n\r");
#endif
      return m;
    }
    if (addresses[m][0] != 0x28)
    {
      break;
    }
#if SerialDebug == true
    char pbuf[20];
    printSensorAddress(pbuf, addresses[m]);
    Serial.print("Found 0x");
    Serial.println(pbuf);
#endif
    m++;
  }
  //  Serial.println();
#if SerialDebug == true
  Serial.println("Done");
#endif
  ds.reset_search();

  gSensorNumber = m;
  return m;
}

#define HEXChar(a) ((a) > 9)? ('A' + (a) -10):('0' + (a))
void Sensors::printSensorAddress(char *buf, std::uint8_t *addr)
{
  for (std::uint8_t i = 0; i < 8; i++)
  {
    buf[i * 2] = HEXChar(addr[i] >> 4);
    buf[i * 2 + 1] = HEXChar(addr[i] & 0xF);
  }
  buf[16] = 0;
}

void Sensors::loadSensorSetting(void)
{
  gPrimarySensorIndex = 0;

#if SerialDebug == true
  Serial.print("loadSensorSetting\n\r");
#endif

  // read sensor address from storage.
  std::uint32_t msb = 0;
  std::uint32_t lsb = 0;

  msb = settings.Get_u(Settings::set_SensorAddress_Brew_MSB);
  lsb = settings.Get_u(Settings::set_SensorAddress_Brew_LSB);
  sensor_addresses[Sensor_Bew][7] = (msb >> 24) & 0x000000FF;
  sensor_addresses[Sensor_Bew][6] = (msb >> 16) & 0x000000FF;
  sensor_addresses[Sensor_Bew][5] = (msb >>  8) & 0x000000FF;
  sensor_addresses[Sensor_Bew][4] = (msb >>  0) & 0x000000FF;
  sensor_addresses[Sensor_Bew][3] = (lsb >> 24) & 0x000000FF;
  sensor_addresses[Sensor_Bew][2] = (lsb >> 16) & 0x000000FF;
  sensor_addresses[Sensor_Bew][1] = (lsb >>  8) & 0x000000FF;
  sensor_addresses[Sensor_Bew][0] = (lsb >>  0) & 0x000000FF;

  msb = settings.Get_u(Settings::set_SensorAddress_Sparge_MSB);
  lsb = settings.Get_u(Settings::set_SensorAddress_Sparge_LSB);
  sensor_addresses[Sensor_Sparge][7] = (msb >> 24) & 0x000000FF;
  sensor_addresses[Sensor_Sparge][6] = (msb >> 16) & 0x000000FF;
  sensor_addresses[Sensor_Sparge][5] = (msb >>  8) & 0x000000FF;
  sensor_addresses[Sensor_Sparge][4] = (msb >>  0) & 0x000000FF;
  sensor_addresses[Sensor_Sparge][3] = (lsb >> 24) & 0x000000FF;
  sensor_addresses[Sensor_Sparge][2] = (lsb >> 16) & 0x000000FF;
  sensor_addresses[Sensor_Sparge][1] = (lsb >>  8) & 0x000000FF;
  sensor_addresses[Sensor_Sparge][0] = (lsb >>  0) & 0x000000FF;

  std::uint8_t i;
  for (i = 0; i < MaximumNumberOfSensors; i++)
  {
    if (sensor_addresses[i][0] != 0x28 || OneWire::crc8(sensor_addresses[i], 7) != sensor_addresses[i][7])
    {
      break;
    }
  }
  gSensorNumber = i;

  calibration[Sensor_Bew] = settings.Get(Settings::set_SensorCalibration_Brew);
  calibration[Sensor_Sparge] = settings.Get(Settings::set_SensorCalibration_Sparge);

#if SerialDebug == true
  Serial.printf("Number of sensors:%d", gSensorNumber);
#endif

}

void Sensors::saveSensor(std::uint8_t idx, std::uint8_t address[ ])
{
  std::uint32_t msb = (address[7] << 24) | (address[6] << 16) | (address[5] << 8) | (address[4] << 0);
  std::uint32_t lsb = (address[3] << 24) | (address[2] << 16) | (address[1] << 8) | (address[0] << 0);

  if (idx == Sensor_Bew)
  {
    settings.Set(Settings::set_SensorAddress_Brew_MSB, msb);
    settings.Set(Settings::set_SensorAddress_Brew_LSB, lsb);
  }
  if (idx == Sensor_Sparge)
  {
    settings.Set(Settings::set_SensorAddress_Sparge_MSB, msb);
    settings.Set(Settings::set_SensorAddress_Sparge_LSB, lsb);
  }
}

