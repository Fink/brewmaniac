#ifndef WI_H
#define WI_H

#include "Sensors.h"

#define	wiIndicationStart(a)
#define	wiIndicationPut(a)
#define	wiIndicationEnd()

#include "BrewManiacWeb.h"
extern BrewManiacWeb bmWeb;

void wiSendButtonLabel(const byte labelId);
void wiReportCurrentStage(byte stage);
void wiReportAuxHeater(byte value);
void wiUpdateHeaterStatus(void);

void wiReportHeater(byte value);


void wiReportPump(byte value);
void wiReportEvent(byte event);
void wiReportPwm(std::uint8_t pwm);
void wiReportSettingTemperature(void);
void wiTogglePwm(void);
void wiRecipeChange(void);
void wiSettingChanged(int address,byte value);
void wiSetDeviceAddress(byte ip[], bool apmode);

byte wiReadCalibrationOfSensor(byte i);
void wiUpdateCalibrationOfSensor(byte i,byte value);
byte wiReadPrimarySensor(byte i);
byte wiReadAuxSensor(byte i);
void wiUpdatePrimarySensor(byte i,byte v);
void wiUpdateAuxSensor(byte i,byte v);

void wiStartSensorScan(void);

void wiUpdateSetting(int address,byte value);
void wiLcdBufferBegin(void);
void wiLcdBufferEnd(bool update=true);
void wiInitialize();
void wiThread();

#endif
