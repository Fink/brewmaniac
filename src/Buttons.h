/*
 * Buttons.h
 *
 *  Created on: Jul 28, 2020
 *      Author: fink
 */

#ifndef SRC_BUTTONS_H_
#define SRC_BUTTONS_H_

#include <cstdint>
#include "ui_globals.h"

class Buttons
{
public:

	Buttons();
	virtual ~Buttons();

	void Init();
	void VirtualButtonPress(std::uint8_t mask, bool longPressed);
	e_button_mask ReadButtons();

private:
	bool _virtual_button_pressed;
	bool _continuous_pressed_detected;
	bool _long_pressed;
	std::uint8_t  _button_pressed;
	std::uint8_t  _test_button_status;
	std::uint32_t _button_change_time;
	std::uint32_t _continuous_pressed_dected_time;
	std::uint32_t _oneFigerUp;

};

extern Buttons buttons;

#endif /* SRC_BUTTONS_H_ */
