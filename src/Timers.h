/*
 * Timers.h
 *
 *  Created on: Jul 29, 2020
 *      Author: fink
 */

#ifndef SRC_TIMERS_H_
#define SRC_TIMERS_H_

#include <cstdint>

#define BREW_END_STAY_DURATION 5

class Timers
{
  public:
    typedef enum
    {
      TIMER_NONE            = 0b00000000,
      TIMER_NORMAL          = 0b00000001,
      TIMER_AUX             = 0b00000010,
      TIMER_RECURRING_100ms = 0b00000100,
      TIMER_RECURRING_1s    = 0b00001000,
    } e_timer_type;

    Timers();
    virtual ~Timers();

    void Init();
    std::uint32_t GetRemainingTime();
    std::uint32_t PauseTimer();
    void SetAuxTimeoutAfter(std::uint32_t duration);
    void SetTimeoutAfter(std::uint32_t duration);
    e_timer_type Any_Timeout_Occured();
    bool IsAuxTimeout();

    std::uint32_t current_time_in_ms;
    std::uint32_t current_time_in_secs;

  private:
    std::uint32_t _system_start_time_in_ms;
    std::uint32_t _timeout;
    std::uint32_t _aux_timeout;
    std::uint32_t _recurring_timeout_100ms;
    std::uint32_t _recurring_timeout_1s;
    bool  _is_aux_timed_out;

    static const std::uint32_t _recurring_duration_100ms = 100;
    static const std::uint32_t _recurring_duration_1s = 1000;
};

extern Timers timers;

#endif /* SRC_TIMERS_H_ */
