#ifndef ESPUpdateServer_H
#define ESPUpdateServer_H

void ESPUpdateServer_setup(const char* host, const char* user, const char* pass);
void ESPUpdateServer_loop(void);

#endif
