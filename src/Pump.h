/*
 * Pump.h
 *
 *  Created on: Aug 3, 2020
 *      Author: fink
 */

#ifndef SRC_PUMP_H_
#define SRC_PUMP_H_

#include "RestableDevice.h"

class Pump: public RestableDevice
{
  public:
    Pump();
    virtual ~Pump();

    void virtual deviceOn(void);
    void virtual deviceOff(bool programOff);
    void loadParameters(void);
    void updateUI();
};

extern Pump pump;

#endif /* SRC_PUMP_H_ */
