/*
 * SettingEditor.cpp
 *
 *  Created on: Aug 4, 2020
 *      Author: fink
 */

#include <pgmspace.h>
#include "SettingEditor.h"
#include "lcd_interface.h"
#include "wi.h"
#include "debug.h"

SettingEditor settingEditor;

SettingEditor::SettingEditor()
{
}

void SettingEditor::setup(const SettingItem *items, int (*getter)(int GetCurrentIndex), void (*setter)(int GetCurrentIndex, int value))
{
  //uiButtonLabel(ButtonLabel(Up_Down_x_Ok));
  _currentSettingIndex = 0;
  _SettingItems = items;
  _getValue = getter;
  _setValue = setter;
}
void SettingEditor::setup(const SettingItem *items)
{
  setup(items, NULL, NULL);
}

void SettingEditor::getItemDetail(void)
{
  const char *addr = (const char*) &_SettingItems[_currentSettingIndex];

  char *dst = (char*) &_item;
  for (int i = 0; i < (int) sizeof(SettingItem); i++)
  {
    dst[i] = pgm_read_byte_near(addr + i);
  }
  if (_item.address == 0 && _getValue != NULL)
  {
    _itemValue = _getValue(_currentSettingIndex);
  }
  else
  {
    _itemValue = (int) settings.Get(_item.address);
  }

  _minValue = static_cast<std::uint8_t>(settings.GetMin(_item.address));
  _maxValue = static_cast<std::uint8_t>(settings.GetMax(_item.address));
  _currentSettingAddress = _item.address;

  settings.GetText(_item.address, _item_title);
}

void SettingEditor::nextItem(void)
{
  _currentSettingIndex++;
  displayItem();
}

void SettingEditor::nextItem(std::uint8_t max, std::uint8_t min)
{
  _currentSettingIndex++;
  getItemDetail();
  editItem(_item_title, _itemValue, max, min, _item.display);
}

void SettingEditor::displayItem(void)
{
  getItemDetail();
  //editItem(str_t label, std::uint8_t value, std::uint8_t max, std::uint8_t min,CDisplayFunc displayFunc)
  editItem(_item_title, _itemValue, _maxValue, _minValue, _item.display);
}

void SettingEditor::displayItem(std::uint8_t max, std::uint8_t min)
{
  getItemDetail();
  //editItem(str_t label, std::uint8_t value, std::uint8_t max, std::uint8_t min,CDisplayFunc displayFunc)
  editItem(_item_title, _itemValue, max, min, _item.display);
}

std::uint8_t SettingEditor::GetCurrentIndex(void)
{
  return _currentSettingIndex;
}
void SettingEditor::setIndex(std::uint8_t index)
{
  _currentSettingIndex = index;
}

bool SettingEditor::buttonHandler(e_button_mask mask)
{
  switch (mask)
  {
  case ButtonMaskEnter:
  {
    std::uint8_t value = (std::uint8_t) editItemValue();

    if (_currentSettingAddress == 0 && _setValue != NULL)
    {
      _setValue(_currentSettingIndex, value);
    }
    changeSettingValue(_currentSettingAddress, value);
    return true;
  }

  case ButtonMaskUp:
    editItemChange(+1);
    break;

  case ButtonMaskDown:
    editItemChange(-1);
    break;

  case ButtonMaskContUp:
    editItemChange(+5);
    break;

  case ButtonMaskContDown:
    editItemChange(-5);
    break;

  default:
    break;
  }
  return false;
}

void SettingEditor::editItemChange(int change)
{
  int potential = _editingValue + change;

  if (potential > _maxValue)
    potential = _maxValue;
  if (potential < _minValue)
    potential = _minValue;
  DBG_PRINTF("_editingValue=%i,potential%i\r\n", _editingValue, potential);
  if (potential != _editingValue)
  {
    _editingValue = potential;
    uiSettingFieldClear();
    (*_displayFunc)(_editingValue);
  }
}

int SettingEditor::editItemValue(void)
{
  return _editingValue;
}

void SettingEditor::editItem(const char *label, int value, int max, int min, void (*displayFunc)(int))
{
  _editingValue = value;
  _maxValue = max;
  _minValue = min;

  if (_editingValue > _maxValue)
    _editingValue = _maxValue;
  if (_editingValue < _minValue)
    _editingValue = _minValue;
  _displayFunc = displayFunc;

  // uiClearSettingRow();
  // uiSettingTitle will know the length of
  // label, and fill spaces to clear the line
  uiSettingTitle(label);
  (*_displayFunc)(_editingValue);
}

void SettingEditor::editItemTitleAppendNumber(std::uint8_t num)
{
  uiSettingTitleAppendNumber(num);
}

void SettingEditor::changeSettingValue(int address, std::uint8_t value)
{
  if (settings.Set(static_cast<Settings::db_t>(address), static_cast<float>(value)))
    wiSettingChanged(address, value);
}

