/*
 * Timers.cpp
 *
 *  Created on: Jul 29, 2020
 *      Author: fink
 */

#include "Timers.h"
#include "debug.h"
#include <Arduino.h>

Timers timers;

Timers::Timers()
{
}

Timers::~Timers()
{
}

void Timers::Init()
{
  _system_start_time_in_ms = millis();
}

std::uint32_t Timers::GetRemainingTime()
{
  if (_timeout == 0)
  {
    return 0;
  }

  std::uint32_t ret = (_timeout - current_time_in_ms);
  return ret;
}

std::uint32_t Timers::PauseTimer(void)
{
  if (_timeout == 0)
  {
    return 0;
  }

  std::uint32_t ret = (_timeout - current_time_in_ms);
  _timeout = 0;
  _aux_timeout = 0;

  return ret;
}

void Timers::SetAuxTimeoutAfter(std::uint32_t duration)
{
  DBG_PRINTF("aux_timeout %u\r\n", duration);
  _aux_timeout = current_time_in_ms + duration;
}

void Timers::SetTimeoutAfter(std::uint32_t duration)
{
  DBG_PRINTF("timeout %u\r\n", duration);
  _timeout = current_time_in_ms + duration;
}

Timers::e_timer_type Timers::Any_Timeout_Occured()
{
  std::uint8_t ret = TIMER_NONE;

  current_time_in_ms = millis();

  current_time_in_secs = (current_time_in_ms - _system_start_time_in_ms) / 1000;

  if (_recurring_duration_100ms && (_recurring_timeout_100ms <= current_time_in_ms))
  {
//    DBG_PRINTF("TIMER_RECURRING_100ms");
    _recurring_timeout_100ms = current_time_in_ms + _recurring_duration_100ms;
    ret |= TIMER_RECURRING_100ms;
  }

  if (_recurring_duration_1s && (_recurring_timeout_1s <= current_time_in_ms))
  {
//    DBG_PRINTF("TIMER_RECURRING_1s");
    _recurring_timeout_1s = current_time_in_ms + _recurring_duration_1s;
    ret |= TIMER_RECURRING_1s;
  }

  if (_timeout && (_timeout <= current_time_in_ms))
  {
    DBG_PRINTF("TIMER_NORMAL");
    _timeout = 0;
    ret |= TIMER_NORMAL;
  }

  if (_aux_timeout && (_aux_timeout <= current_time_in_ms))
  {
    DBG_PRINTF("TIMER_AUX");
    _aux_timeout = 0;
    _is_aux_timed_out = true;
    ret |= TIMER_AUX;
  }

  return static_cast<e_timer_type>(ret);
}

bool Timers::IsAuxTimeout()
{
  return _is_aux_timed_out;
}

