/*
 * Sensors.h
 *
 *  Created on: Aug 9, 2020
 *      Author: fink
 */

#ifndef SRC_SENSORS_H_
#define SRC_SENSORS_H_

#include <cstdint>

class Sensors
{
  public:

    enum
    {
      Sensor_Bew,
      Sensor_Sparge,
      Sensor_Count
    };

    Sensors();
    virtual ~Sensors();

    void Init();
    void Service();
    void loadSensorSetting(void);
    std::uint8_t scanSensors(std::uint8_t max, std::uint8_t addresses[][8]);
    void saveSensor(std::uint8_t idx, std::uint8_t address[ ]);
    void printSensorAddress(char *buf, std::uint8_t *addr);


    std::uint8_t gSensorNumber;
    std::uint8_t sensor_addresses[Sensor_Count][8];
    std::uint8_t sensor_resolution;
    float result[Sensor_Count];
    float calibration[Sensor_Count];
    bool  gSensorDisconnected[Sensor_Count];

  private:
    bool _isConverting[Sensor_Count];
    std::uint8_t gPrimarySensorIndex;
    std::uint32_t _last_valid_reading;
    float _calibration[Sensor_Count];

    void Set_Sensor_Resolution(std::uint8_t *addr, std::uint8_t res);
    void SensorRequestConvert(std::uint8_t address[]);
    bool SensorDataReady(std::uint8_t address[]);
    float SensorRead(std::uint8_t * address);

    static const std::uint8_t DSCMD_CONVERT_T = 0x44;
    static const std::uint8_t DSCMD_READ_SCRATCHPAD = 0xBE;
};

extern Sensors sensors;

#endif /* SRC_SENSORS_H_ */
