/*
 * Heaters.h
 *
 *  Created on: Aug 6, 2020
 *      Author: fink
 */

#ifndef SRC_HEATERS_H_
#define SRC_HEATERS_H_

#include <cstdint>

class Heaters
{
  public:

    typedef struct
    {
      bool brew;
      bool sparge;
    } t_outputs;

    Heaters();
    virtual ~Heaters();

    void Init();
    void Service_100ms(); //10 second duty means 1pct after each Service call
    void Enable_Sparge_Heater(bool enable);
    void Set_Brew_Power(std::uint8_t pct);
    void Set_Sparge_Power(std::uint8_t pct);
    void Get_Outputs(t_outputs * outputs);

  private:
    bool _sparge_heater_enabled;
    std::uint8_t _primary_duty;
    std::uint8_t _sparge_duty;
    std::uint8_t _requested_brew_power;
    std::uint8_t _requested_sparge_power;
    std::uint8_t _duty_counter;
    t_outputs _outputs;
};

extern Heaters heaters;

#endif /* SRC_HEATERS_H_ */
