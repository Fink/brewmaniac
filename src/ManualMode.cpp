/*
 * ManualMode.cpp
 *
 *  Created on: Aug 6, 2020
 *      Author: fink
 */

#include "ManualMode.h"
#include "lcd_interface.h"
#include "wi.h"
#include "BrewLogger.h"
#include "stages.h"
#include "Temperature.h"
#include "Pump.h"
#include "buzz.h"
#include "Menu.h"

ManualMode manualMode;

ManualMode::ManualMode()
{
}

ManualMode::~ManualMode()
{
}

void ManualMode::Init()
{
  uiTitle(STR(Manual_Mode));
  uiSubTitle(STR(Water_Added));
  uiButtonLabel(ButtonLabel(Continue_Yes_No));
  _manual_state = MS_AskWater;
  gIsEnterPwm = false;

  gSettingTemperature = DEFAULT_MANUL_MODE_TEMPERATURE;
  wiReportSettingTemperature();

}

bool ManualMode::EventHandler(std::uint8_t event, std::uint8_t mask)
{
  if (_manual_state == MS_AskWater)
  {
    return Handle_Ask_Water(event, mask);
  }
  else if (_manual_state == MS_AskAutoTune)
  {
    return Handle_Ask_AutoTune(event, mask);
  }
  else if (_manual_state == MS_RunningAutoTune)
  {
    return Handle_RunningAutoTune(event, mask);
  }
  else if (_manual_state == MS_AskExitingAutoTune)
  {
    return Handle_Ask_ExitAutoTune(event, mask);
  }
  else if (_manual_state == MS_ManualMode)
  {
    return Handle_ManualMode(event, mask);
  }
  return false;
}

bool ManualMode::Handle_Ask_Water(std::uint8_t event, std::uint8_t mask)
{
  switch (mask)
  {
  case ButtonMaskEnter:
    menu.switchApplication(Menu::MAIN_SCREEN);
    return true;

  case ButtonMaskStart:
    //load heating parameters
    loadBrewParameters();
    Enter_ManualMode();
    _manual_state = MS_ManualMode;
    // setup to be called for Timer & temperature event
    SetEventMask(ButtonPressedEventMask | TemperatureEventMask | TimeoutEventMask);

    brewLogger.startSession(Temperature::TEMP_COUNT, TemperatureChartPeriod, false, false);

    brewLogger.stage(StageManualMode);
    brewLogger.setPoint(gSettingTemperature);

    return true;

  default:
     return false;
  }
}

bool ManualMode::Handle_ManualMode(std::uint8_t event, std::uint8_t mask)
{
  // states other than MSAskWater, handle Heat & Pump button
  if (event == ButtonPressedEventMask)
  {
    switch (mask)
    {
    case ButtonMaskEnter:
      pump.toggle();
      break;

    case ButtonMaskLongEnter:
      pump.setRestEnabled(!pump.isRestEnabled());
      break;

    case ButtonMaskStart:
      if (gIsHeatOn)
        heatOff();
      else heatOn();
      break;

    case ButtonMaskLongStart:
      uiButtonLabel(ButtonLabel(Tune_PID_No_Yes));
      _manual_state = MS_AskAutoTune;
      if (gIsHeatOn)
      {
        heatOff();
      }
      pump.off();
      break;

    case ButtonMaskDownStart:
      if (gEnableSpargeWaterHeatingControl)
      {
        stopHeatingSpargeWater();
      }
      else
      {
        startHeatingSpargeWater();
      }
      break;

    case ButtonMaskEnterStart:
      isCountDownTimeBlinking = !isCountDownTimeBlinking;
      if (isCountDownTimeBlinking)
      {
        uiRunningTimeShowInitial(0);
        _changeCountDownTime = 0;
        timers.PauseTimer();
        gIsTemperatureReached = true;  // force to ignore temperature
      }
      else
      {
        // end of input. if zero, make it normal mode
        // else make it count down mode
        isManualModeCountDownMode = (_changeCountDownTime > 0);
        gIsTemperatureReached = false;
      }
      uiRunningTimeBlink(isCountDownTimeBlinking);
      break;

    case ButtonMaskUp:
    case ButtonMaskDown:
    case ButtonMaskContUp:
    case ButtonMaskContDown:
      if (isCountDownTimeBlinking)
      {
        ChangeCountdownTime(static_cast<e_button_mask>(mask));
      }
      else
      {
        if (processAdjustButtons(static_cast<e_button_mask>(mask)))
        {
          if ( gIsTemperatureReached &&
             ( gSettingTemperature >= (temperature.temperature_adjust_threshold + temperature.temperatures[Temperature::TEMP_BREW])))
          {
            // pause counting, reset it
            if (isManualModeCountDownMode)
            {
              uiRunningTimeShowInitial(_changeCountDownTime * 60);
            }
            else
            {
              uiRunningTimeShowInitial(0);
            }
            gIsTemperatureReached = false;
          }
        }
      }
      return true;

    default:
      return true;
    }

  }  // end of button presss
  else if (event == TemperatureEventMask)
  {
    bool ret = false;
    // Handle temperature change or other states
    //
    if (!gIsTemperatureReached)
    {
      if (temperature.temperatures[Temperature::TEMP_BREW] >= gSettingTemperature)
      {
        // beep & start counting time

        buzzPlaySound(SoundIdTemperatureReached);

        gIsTemperatureReached = true;

        if (isManualModeCountDownMode)
        {
          uiRunningTimeStartCountDown(_changeCountDownTime * 60);
          timers.SetTimeoutAfter(_changeCountDownTime * 60 * 1000);
        }
        else uiRunningTimeStart();

        brewLogger.event(RemoteEventTemperatureReached);
        wiReportEvent(RemoteEventTemperatureReached);
        ret = true;
      }
    }
    // Temperate Reached state
    bool toggled = togglePwmInput();
    return ret || toggled;
  }  // end of temperature handling
  else if (event == PumpRestEventMask)
  {
    togglePumpRest();
    return true;
  }
  else if (event == TimeoutEventMask)
  {
    buzzPlaySound(SoundIdCountDown);
    isManualModeCountDownMode = false;
    uiRunningTimeStart();
    return true;
  }
  return false;
}

bool ManualMode::Handle_Ask_AutoTune(std::uint8_t event, std::uint8_t mask)
{
  if (event != ButtonPressedEventMask)
    return false;

  switch (mask)
  {
  case ButtonMaskEnter:
    _manual_state = MS_RunningAutoTune;

    uiTitle(STR(PID_AUTOTUNE));
    uiButtonLabel(ButtonLabel(x_x_Exit_Pmp));
    setSettingTemperature(temperature.temperatures[Temperature::TEMP_BREW]);

    uiRunningTimeShowInitial(0);
    uiRunningTimeStart();

    startAutoTune();
    heatOn();

    brewLogger.stage(StagePIDAutoTune);

    wiReportCurrentStage(StagePIDAutoTune);
    return true;

  case ButtonMaskStart:
    _manual_state = MS_ManualMode;
    uiButtonLabel(ButtonLabel(Up_Down_Heat_Pmp));
    return true;

  default:
    return false;
  }
}

bool ManualMode::Handle_RunningAutoTune(std::uint8_t event, std::uint8_t mask)
{
  if (event == ButtonPressedEventMask)
  {
    switch (mask)
    {
    case ButtonMaskEnter:
      pump.toggle();
      return true;

    case ButtonMaskStart:
      _manual_state = MS_AskExitingAutoTune;
      uiButtonLabel(ButtonLabel(Stop_No_Yes));
      return true;

    default:
      break;
    }
  }

  if (!_isRunningAutoTune)
  {
    // auto tuning finished
    buzzPlaySound(SoundIdAutoTuneFinished);
    finishAutoTuneBackToManual();
    return true;
  }
  return false;
}

bool ManualMode::Handle_Ask_ExitAutoTune(std::uint8_t event, std::uint8_t mask)
{
  if (event != ButtonPressedEventMask)
    return false;

  switch (mask)
  {
  case ButtonMaskEnter:
    cancelAutoTune();
    finishAutoTuneBackToManual();
    return true;

  case ButtonMaskStart:
    _manual_state = MS_RunningAutoTune;
    uiButtonLabel(ButtonLabel(x_x_Exit_Pmp));
    return true;

  default:
    return false;
  }
}

void ManualMode::Enter_ManualMode(void)
{
  uiClearSubTitleRow();
  uiButtonLabel(ButtonLabel(Up_Down_Heat_Pmp));

  // displace current temperature
  uiTempDisplaySetPosition(TemperatureManualModePosition);
  // Setpoint temperature
  uiSetSettingTemperature(gSettingTemperature);

  // display counting time
  uiRunningTimeSetPosition(RunningTimeNormalPosition);
  uiRunningTimeShowInitial(0);

  if (temperature.temperatures[Temperature::TEMP_BREW] >= gSettingTemperature)
  {
    //temp reached
    gIsTemperatureReached = true;
    uiRunningTimeStart();
  }
  else
  {
    gIsTemperatureReached = false;
    // wait to reach temperature setting
  }

  SetEventMask(TemperatureEventMask | ButtonPressedEventMask | TimeoutEventMask);
  setAdjustTemperature(110.0, 20.0);

  wiReportCurrentStage(StageManualMode);
  wiReportSettingTemperature();

}

void ManualMode::finishAutoTuneBackToManual(void)
{
  _manual_state = MS_ManualMode;

  float temp = 35;
  setSettingTemperature(temp);

  uiRunningTimeShowInitial(0);

  uiTitle(STR(Manual_Mode));
  uiButtonLabel(ButtonLabel(Up_Down_Heat_Pmp));

  if (gIsHeatOn)
    heatOff();
  pump.off();

  brewLogger.stage(StageManualMode);

  wiReportCurrentStage(StageManualMode);
}

void ManualMode::ChangeCountdownTime(e_button_mask mask)
{
  std::int16_t val = 0;

  switch (mask)
  {
  case ButtonMaskUp:
    val = +1;
    break;

  case ButtonMaskDown:
    val = -1;
    break;

  case ButtonMaskContUp:
    val = +5;
    break;

  case ButtonMaskContDown:
    val = -5;
    break;

  default:
     return;
  }

  _changeCountDownTime += val;
  if (_changeCountDownTime < 0)
  {
    _changeCountDownTime = 0;
  }
  else if (_changeCountDownTime > 600)
  {
    _changeCountDownTime = 600;
  }
  uiRunningTimeShowInitial(_changeCountDownTime * 60);
}

