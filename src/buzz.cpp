/*
 * buzz.cpp
 *
 *  Created on: Jul 28, 2020
 *      Author: fink
 */

#include "buzz.h"
#include "Pins.h"
#include "Timers.h"
#include "ui_globals.h"

const byte _soundIdTemperatureReached[] PROGMEM ={5,10,3,10,3,10};
const byte _soundIdCountDown[] PROGMEM ={9,20,20,20,20,20,20,20,20,40}; // 5 seconds
//HOP_ALTERTING_TIME
const byte _soundIdAddHop[] PROGMEM ={19,30,10,30,10,30,10,30,10,30,10,30,10,30,10,30,10,30,10,40}; // 10 seconds
const byte _soundIdWaitUserInteraction[] PROGMEM ={2,10,30};
const byte _soundIdBrewEnd[] PROGMEM ={2,37,7};
const byte * const _sounds [] PROGMEM ={
_soundIdTemperatureReached,
_soundIdCountDown,
_soundIdAddHop,
_soundIdWaitUserInteraction,
_soundIdBrewEnd};

// a hit & run function
// buzzer thread will handle the pattern & time
byte _numberofNtesToPlay;
boolean _repeat;
boolean _playing;
boolean _buzzing;
unsigned long _buzzingTime;
word _currentPeriod;
byte* _ptrCurrentNote;

byte* _currentSound;

void buzzStartPlay(SoundId id, boolean repeat)
{
	_currentSound=(byte *)pgm_read_dword(&(_sounds[id]));

	_numberofNtesToPlay = pgm_read_byte(_currentSound);

	_ptrCurrentNote =_currentSound;

	_ptrCurrentNote ++;
	_currentPeriod = (word)pgm_read_byte(_ptrCurrentNote) *25;

	_repeat = repeat;
	_playing=true;

	_buzzingTime = millis();
  pins.Set_Output(Pins::Out_BuzzControl, Pins::PIN_ON);
	_buzzing=true;
}

void buzzPlaySound(SoundId id)
{
	buzzStartPlay(id,false);
}

// play the sound until mute() is called
void buzzPlaySoundRepeat(SoundId id)
{
	buzzStartPlay(id,true);
}

void buzzMute(void)
{
  pins.Set_Output(Pins::Out_BuzzControl, Pins::PIN_OFF);
	_playing=false;

}

void buzzOn(void)
{
  pins.Set_Output(Pins::Out_BuzzControl, Pins::PIN_ON);
}

void buzzThread(void)
{
	if(!_playing) return;

	if((timers.current_time_in_ms-_buzzingTime) >= ((uint32_t)_currentPeriod + BUZZER_TIME_TOLERANCE))
	{
		_numberofNtesToPlay --;
		if(_numberofNtesToPlay ==0)
		{
			if(_repeat)
			{
				_ptrCurrentNote =_currentSound;
				_numberofNtesToPlay = pgm_read_byte(_currentSound);
			}
			else
			{
				// finished, stop
				buzzMute();
				return;
			}
		}
		_buzzingTime = millis();
		//else
		_ptrCurrentNote ++;
		_currentPeriod = (word)pgm_read_byte(_ptrCurrentNote) *25;

		if(_buzzing)
		{
		  pins.Set_Output(Pins::Out_BuzzControl, Pins::PIN_OFF);
		}
		else
		{
		  pins.Set_Output(Pins::Out_BuzzControl, Pins::PIN_ON);
		}
		_buzzing = ! _buzzing;
	}
}


