/*
 * wi.cpp
 *
 *  Created on: Aug 3, 2020
 *      Author: fink
 */

#include "wi.h"
#include "lcd_interface.h"
#include "buzz.h"
#include "stages.h"
#include "Settings.h"
#include "Temperature.h"
#include "debug.h"

extern byte gBoilHeatOutput;

void wiSendButtonLabel(const byte labelId)
{
  bmWeb.setButtonLabel(labelId);
}
byte _currentStage=101;
void wiReportCurrentStage(byte stage)
{
    _currentStage = stage;
    bmWeb.setBrewStage(stage);
}

void wiReportAuxHeater(byte value)
{
  bmWeb.setAuxHeaterStatus(value);
}
void wiUpdateHeaterStatus(void)
{
  bmWeb.statusChange();
}

void wiReportHeater(byte value)
{
  bmWeb.setHeaterStatus(value);
}


void wiReportPump(byte value)
{
  bmWeb.setPumpStatus(value);
}

void wiReportEvent(byte event)
{
  bmWeb.brewEvent(event);
}

void wiReportPwm(std::uint8_t pwm)
{
  bmWeb.updatePwm(pwm);
}

void wiReportSettingTemperature(void)
{
  bmWeb.updateSettingTemperature();
}

void wiTogglePwm(void)
{
  bmWeb.statusChange();
}

void wiRecipeChange(void)
{
  bmWeb.automationChanged();
}

void wiSettingChanged(int address,byte value)
{
  bmWeb.settingChanged(address,value);
}


void wiSetDeviceAddress(byte ip[], bool apmode)
{
  // support only IPv4 for now
  if( ip[0] == 0 && ip[1] == 0 && ip[2] == 0 && ip[3] == 0){
    // clear
    uiSetWirelessStatus(WiStateNotConnected);
    buzzPlaySound(SoundIdWarnning);
    uiSetIp(ip);
    if(_currentStage == StageIdleScreen)
      uiClearIpAddress();
  }else{
    uiSetWirelessStatus(apmode? WiStateAccessPoint:WiStateConnected);
    uiSetIp(ip);
    if(_currentStage == StageIdleScreen)
      uiPrintIpAddress();
  }
}


byte wiReadCalibrationOfSensor(byte i)
{
  if (i == 0)
  {
    return static_cast<std::uint8_t>(settings.Get(Settings::set_SensorCalibration_Brew));
  }
  else
  {
    return static_cast<std::uint8_t>(settings.Get(Settings::set_SensorCalibration_Sparge));
  }
}

void wiUpdateCalibrationOfSensor(byte i,byte value)
{
  if (i == 0)
  {
    settings.Set(Settings::set_SensorCalibration_Brew, static_cast<float>(value));
  }
  else
  {
    settings.Set(Settings::set_SensorCalibration_Sparge, static_cast<float>(value));
  }
  sensors.calibration[i]=((float)value -50.0)/10.0;
  Serial.printf("cal:%d, value:%d\n",i,value);
}

byte wiReadPrimarySensor(byte i)
{
  return 0;//readSetting(PS_SensorUseAddressOf(i));
}

byte wiReadAuxSensor(byte i)
{
  return 0;//readSetting(PS_AuxSensorAddressOf(i));
}

void wiUpdatePrimarySensor(byte i,byte v)
{
  //updateSetting(PS_SensorUseAddressOf(i),v);
}

void wiUpdateAuxSensor(byte i,byte v)
{
  //updateSetting(PS_AuxSensorAddressOf(i),v);
}

static byte _wiSensorScanRequest;

void wiStartSensorScan(void)
{
  _wiSensorScanRequest =1;
}

void wiUpdateSetting(int address,byte value)
{
  settings.Set(static_cast<Settings::db_t>(address), static_cast<float>(value));
  settings.Service();
}
void wiLcdBufferBegin(void)
{
  bmWeb.holdStatusUpdate();
}

void wiLcdBufferEnd(bool update)
{
  bmWeb.unHoldStatusUpdate(update);
}

void wiInitialize(){
  _wiSensorScanRequest =0;
}

void wiThread()
{
  if(_wiSensorScanRequest)
  {
    sensors.scanSensors(MaximumNumberOfSensors,sensors.sensor_addresses);
    _wiSensorScanRequest =0;
    bmWeb.scanSensorDone();
  }
}

