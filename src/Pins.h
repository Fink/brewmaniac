/*
 * Pins.h
 *
 *  Created on: Aug 13, 2020
 *      Author: fink
 */

#ifndef SRC_PINS_H_
#define SRC_PINS_H_

#include <cstdint>

class Pins
{
  public:
     enum
     {
       ButtonUpPin,
       ButtonDownPin,
       ButtonStartPin,
       ButtonEnterPin,
       INPUT_PIN_COUNT
     } e_inputs;

     enum
     {
       Data_Sensor,
       Data_SDA,
       Data_SCL,
       DATA_PIN_COUNT
     } e_datapins;

     enum
     {
       Out_SpargeHeater,
       Out_PumpControl,
       Out_BrewHeater,
       Out_BuzzControl,
       OUTPUT_PIN_COUNT
     } e_outputs;

     enum
     {
       PIN_OFF,
       PIN_ON
     } e_on_off;

     Pins();
     virtual ~Pins();

     void Init();
     void Set_Output(std::uint8_t output, bool on);
     std::uint8_t Read_Inputs(void);
     std::uint8_t Get_Mapped_Data_Pin(std::uint8_t pin);

  private:
    // ESP32-DevKitC V4 with ESP32-WROOM-32 module soldered
    enum
    {
      ESP_PIN_SD0      = 99, // Reserved internally
      ESP_PIN_SD1      = 99, // Reserved internally
      ESP_PIN_SD2      = 99, // Reserved internally
      ESP_PIN_SD3      = 99, // Reserved internally
      ESP_PIN_CMD      = 99, // Reserved internally
      ESP_PIN_CLK      = 99, // Reserved internally
      ESP_PIN_IO_0     = 99, // Reserved internally
      ESP_PIN_IO_2     =  2, // Stapping pin at boot
      ESP_PIN_IO_4     =  4, // Stapping pin at boot
      ESP_PIN_IO_5     =  5, // Stapping pin at boot
      ESP_PIN_IO_12    = 12, // Stapping pin at boot
      ESP_PIN_IO_13    = 13,
      ESP_PIN_IO_14    = 14,
      ESP_PIN_IO_15    = 15, // Stapping pin at boot
      ESP_PIN_IO_16    = 16,
      ESP_PIN_IO_17    = 17,
      ESP_PIN_IO_18    = 18,
      ESP_PIN_IO_19    = 19,
      ESP_PIN_IO_21    = 21,
      ESP_PIN_IO_22    = 22,
      ESP_PIN_IO_23    = 23,
      ESP_PIN_IO_25    = 24,
      ESP_PIN_IO_26    = 26,
      ESP_PIN_IO_27    = 27,
      ESP_PIN_IO_32    = 32,
      ESP_PIN_IO_33    = 33,
      ESP_PIN_IO_34    = 34, // Input only
      ESP_PIN_IO_35    = 35, // Input only
      ESP_PIN_IO_36    = 99, // Input only - Sensor VP
      ESP_PIN_IO_39    = 99, // Input only - Sensor VN
      ESP_PIN_IO_RXD_0 = 99,
      ESP_PIN_IO_TXD_0 = 99,
      ESP_PIN_IO_E     = 99, // Reserved internally
    };

    static constexpr std::uint8_t map_input[INPUT_PIN_COUNT] = {
        ESP_PIN_IO_34,
        ESP_PIN_IO_35,
        ESP_PIN_IO_36,
        ESP_PIN_IO_39,
   };

    static constexpr std::uint8_t map_data[DATA_PIN_COUNT] = {
        ESP_PIN_IO_26,
        ESP_PIN_IO_19,
        ESP_PIN_IO_21,
   };

    static constexpr std::uint8_t map_output[OUTPUT_PIN_COUNT] = {
        ESP_PIN_IO_12,
        ESP_PIN_IO_27,
        ESP_PIN_IO_14,
        ESP_PIN_IO_25
    };
};

extern Pins pins;

#endif /* SRC_PINS_H_ */
